<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class GymwijzerPage extends Model
{

    protected $table = 'leerlijnen';
    protected $primaryKey = 'name';
    public $incrementing = false;
    public $keyType = 'string';

    public function parent()
    {
        return $this->belongsTo(GymwijzerPage::class, 'parent_path', 'path');
    }

//get all the children entry's from the database and only return if the entry is not the same as original GymwijzerPage.
    public function children()
    {
        return $this->hasMany(GymwijzerPage::class, 'parent_path', 'path')->where('id', '!=', $this->id);
    }

    public function videos($niveau)
    {
        if ($niveau === 'starter') {
            $niveau = 0;
        } else if ($niveau === 'gevorderd') {
            $niveau = 5;
        } else if ($niveau === '1 en 2') {
            $niveau = 6;
        } else if ($niveau === '3 en 4') {
            $niveau = 7;
        }

//        join the videos to the page by the bewegingsactivitijd string
        $name = DB::table('leerlijnen')->select('name')->where('id', '=', $this->id)->first();
        $name = json_decode(json_encode($name), true);//make the name a normal string
        $field = 'bewegingsthema';
        $videos = video::where($field, $name)->where('niveau', '=', $niveau)->orderBy('volgorde')->get();
        return $videos;
    }

    public function levels()
    {
//        join on the bewegingsthema string and return the numbers of the niveau
        $name = DB::table('leerlijnen')->select('name')->where('id', '=', $this->id)->first();
        $name = json_decode(json_encode($name), true); //make the name a normal string
        $field = 'bewegingsthema';
        $levels = DB::table('videos')->where($field, $name)->select('niveau')->distinct()->orderBy('niveau')->get();
        $index = 0;
        $out[] = '';

//        turn numbers into text
        foreach ($levels as $level) {
            if ($level->niveau === 0) {
                $out[$index] = 'starter';
                $index++;
            } elseif ($level->niveau === 5) {
                $out[$index] = 'gevorderd';
                $index++;
            } elseif ($level->niveau === 6) {
                $temp = $out[$index - 1];
                $out[$index - 1] = '1 en 2';//strange levels call for different order
                $out[$index] = $temp;
                $index++;
            } elseif ($level->niveau === 7) {
                $temp = $out[$index - 1];
                $out[$index - 1] = '3 en 4';
                $out[$index] = $temp;
                $index++;
            } elseif ($level->niveau === 8) {
                $out[$index] = "voor iedereen";
                $index++;
            } else {
                $out[$index] = $level->niveau;
                $index++;
            }
        }
        return $out;
    }

    public function defaultLevel()
    {
        $levels = $this->levels();
        return $levels[0];
    }

}
