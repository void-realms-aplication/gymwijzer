<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $guarded = [];
    public function results()
    {
        return $this->hasMany(Result::class);
    }
    public function kijkwijzers()
    {
        return $this->hasMany(Kijkwijzer::class);
    }
    public function videos()
    {
        return $this->hasMany(Video::class);
    }
}
