<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Schoolteacher extends Model
{
    protected $guarded = [];
    public function schoolclasses()
    {
        return $this->belongsTo(Schoolclass::class);
    }
    public function getStudents()
    {
        return DB::table('children')
               ->join('teacher_schoolclass', 'children.schoolclass_id', '=', 'teacher_schoolclass.schoolclass_id')
               ->join('teachers', 'teachers.id', '=', 'teacher_schoolclass.teacher_id')
               ->where('teacher_schoolclass.teacher_id' ,'=', $this->id)
               ->select('children.firstname', 'children.schoolclass_id')
               ->orderBy('children.schoolclass_id')
               ->get();
    }
}
