<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $guarded = [];
    public function activities()
    {
        return $this->belongsTo(Activity::class);
    }
    public function Children()
    {
        return $this->belongsTo(Child::class);
    }
}
