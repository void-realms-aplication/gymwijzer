<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactUsMail extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $email;
    public $subjectMail;
    public $messageMail;
    public $title;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($form, $title)
    {
        $this->name = $form['name'];
        $this->email = $form['email'];
        $this->subjectMail = $form['subject'];
        $this->messageMail = (string) $form['message'];
        $this->title = $title;


    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view('emails.contact')
            ->subject('GymWijzer, ' . $this->title);
    }
}
