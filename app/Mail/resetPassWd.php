<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\URL; //needed to generate the correct set password link

class resetPassWd extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $passwordToken;
    public $setPasswordUrl;
    public $title;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $passwordToken)
    {
        $this->user = $user;
        $this->passwordToken = $passwordToken;
        $this->setPasswordUrl = route('password.set', $passwordToken);
        $this->title = 'Stel een wachtwoord in.';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.setpw')->subject('GymWijzer, ' . $this->title);
    }
}
