<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserInvitation extends Mailable
{
    public $registerAccountUrl;
    public $title;
    public $invitedBy;

    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($inviteToken, $invitedUserEmail, $user)
    {
        $this->title = 'U bent uitgenodig om een account aan te maken.';
        $this->invitedBy = $user;
        $this->$inviteToken = $inviteToken;
          $this->registerAccountUrl = route('user.registerWithInviteToken', [$inviteToken, 'email' => $invitedUserEmail]);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.user-invite')->subject('GymWijzer, '. $this->title);
    }
}
