<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use function GuzzleHttp\uri_template;

class AlmostExpiredMail extends Mailable
{
    use Queueable, SerializesModels;

    private $user;
    public $days;
    public $title;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $days)
    {
        $this->user = $user;
        $this->title = "Bijna verlopen";
        $this->days = $days;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.almostExpired',[
            'name' => $this->user->firstname,
        ])
            ->subject("Uw GymWijzer account is bijna verlopen.");
    }
}
