<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schoolclass extends Model
{
    protected $guarded = [];
    public function schools()
    {
        return $this->belongsTo(School::class,'school_id');
    }
    public function teachers()
    {
        return $this->hasMany(Schoolteacher::class);
    }

    public function gymTeachers() {
        return $this->belongsToMany(User::class, 'schoolclass_gymteacher', 'schoolclass_id', 'user_id');
    }

    public function assignTeachers($teachers)
    {
        return $this->gymTeachers()->sync($teachers);
    }

    public function children()
    {
        return $this->hasMany(Child::class);
    }
}
