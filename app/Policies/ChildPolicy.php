<?php

namespace App\Policies;

use App\Child;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ChildPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @return mixed
     */
    public function viewAny()
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @return mixed
     */
    public function view(User $user, Child $child)
    {
        if ($user->abilities()->contains('manage_own_school') && $user->school_id == $child->school_id)
        {
            return true;
        }
        foreach ($user->schoolclasses as $oneClass)
        {
            foreach ($oneClass->children as $oneChild)
            {
                if ($oneChild->id == $child->id)
                {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Determine whether the user can create models.
     *
     * @return mixed
     */
    public function create(User $user, Child $child)
    {
        if ($user->abilities()->contains('manage_own_school') && $user->school_id == $child->school_id)
        {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @return mixed
     */
    public function update(User $user, Child $child)
    {
        if ($user->abilities()->contains('manage_own_school') && $user->school_id == $child->school_id)
        {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @return mixed
     */
    public function delete()
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @return mixed
     */
    public function restore()
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @return mixed
     */
    public function forceDelete()
    {
        //
    }
}
