<?php

namespace App\Policies;

use App\Leerlijn;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class LeerlijnPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the GymWijzer admin portal
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function index(User $user)
    {
        if ($user->abilities()->contains('view_gymwijzer_admin_page')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Leerlijn  $leerlijn
     * @return mixed
     */
    public function view(?User $user, Leerlijn $leerlijn)
    {
        if(!$user && !$leerlijn->demo) {
            return false;
        }
        return true;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if ($user->abilities()->contains('view_gymwijzer_admin_page')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Leerlijn  $leerlijn
     * @return mixed
     */
    public function update(User $user, Leerlijn $leerlijn)
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Leerlijn  $leerlijn
     * @return mixed
     */
    public function delete(User $user, Leerlijn $leerlijn)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\Leerlijn  $leerlijn
     * @return mixed
     */
    public function restore(User $user, Leerlijn $leerlijn)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Leerlijn  $leerlijn
     * @return mixed
     */
    public function forceDelete(User $user, Leerlijn $leerlijn)
    {
        //
    }
}
