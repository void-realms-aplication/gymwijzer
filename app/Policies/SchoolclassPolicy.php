<?php

namespace App\Policies;

use App\School;
use App\Schoolclass;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SchoolclassPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Schoolclass  $schoolclass
     * @return mixed
     */
    public function view(User $user, Schoolclass $schoolclass)
    {
        foreach ($user->schoolclasses as $oneSchoolclass)
        {
            if ($oneSchoolclass->id == $schoolclass->id)
            {
                return true;
            }
        }
        if ($user->abilities()->contains('manage_own_school') && $user->school_id == $schoolclass->school_id) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Schoolclass  $schoolclass
     * @return mixed
     */
    public function view_NoGroup(User $user)
    {
        if ($user->abilities()->contains('manage_own_school')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user, School $school)
    {
        //Green light if user can manage all schools
        if($user->abilities()->contains('manage_all_schools')) {
            return true;
        }

        //Green light if User's got the right ability and is connected to the right school
        if ($user->abilities()->contains('manage_own_school') && $user->school_id == $school->id) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Schoolclass  $schoolclass
     * @return mixed
     */
    public function update(User $user, Schoolclass $schoolclass)
    {
        //Green light if user can manage all schools
        if($user->abilities()->contains('manage_all_schools')) {
            return true;
        }

        //Green light if User's got the right ability and is connected to the right school
        if ($user->abilities()->contains('manage_own_school') && $user->school_id == $schoolclass->school_id) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Schoolclass  $schoolclass
     * @return mixed
     */
    public function delete(User $user, Schoolclass $schoolclass)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\Schoolclass  $schoolclass
     * @return mixed
     */
    public function restore(User $user, Schoolclass $schoolclass)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Schoolclass  $schoolclass
     * @return mixed
     */
    public function forceDelete(User $user, Schoolclass $schoolclass)
    {
        //
    }
}
