<?php

namespace App\Policies;

use App\Period\PeriodConfiguration;
use App\School;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PeriodPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function manage(User $user, School $school) {
        if($user->abilities()->contains('manage_school_periods') && $user->school_id == $school->id) {
            return true;
        }
        return false;
    }

    public function edit(User $user, PeriodConfiguration $config) {
        if($user->abilities()->contains('manage_school_periods') && $user->school_id == $config->school_id) {
            return true;
        }
        return false;
    }
}
