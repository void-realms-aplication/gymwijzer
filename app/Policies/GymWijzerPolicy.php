<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Leerlijn;
use App\Bewegingsthema;
use App\Bewegingsactiviteit;

class GymWijzerPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //Check if the user (or guest) is allowed to see a certain GymWijzer instance
    //This works the same for Leerlijnen, Bewegingsthemas & Bewegingsactiviteit
    public function view(?User $user, $page)
    {
        //Red light if the user is logged out & the page instance is not set to demo
        if(!$user && !$page->demo) {
            return false;
        }
        return true;
    }

    //For now we have one global ability 'manage gymwijzer'
    //It automatically grants the rights to view the GW Admin portal and manage its content
    public function manage(User $user) {
        if ($user->abilities()->contains('manage_gymwijzer_content')) {
            return true;
        }
        return false;
    }
}
