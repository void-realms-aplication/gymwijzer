<?php

namespace App\Period;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PeriodConfigurationTemplate extends Model
{
    public function periods() {
        return $this->hasMany(PeriodTemplate::class, 'config_id');
    }

    public function startYear() {
        return (new Carbon($this->year_start))->year;
    }

    public function endYear() {
        return (new Carbon($this->year_end))->year;
    }

    public function schoolyear() {
        return $this->startYear() . '-' . $this->endYear();
    }
}
