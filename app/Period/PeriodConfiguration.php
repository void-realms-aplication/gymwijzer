<?php

namespace App\Period;

use App\Bewegingsactiviteit;
use App\Period\Period;
use App\Period\PeriodConfigurationTemplate;
use App\School;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PeriodConfiguration extends Model
{
    protected $guarded = [];

    public function setStartEndDates($template) {
        $template = PeriodConfigurationTemplate::find($template);
        if($template == null) { return false; }
        $this->year_start = $template->year_start;
        $this->year_end = $template->year_end;
    }

    public function initPeriods($template) {
        $template = PeriodConfigurationTemplate::find($template);
        if($template == null) { return false; }
        foreach ($template->periods as $period) {
            Period::create([
                'start' => $period->start,
                'end' => $period->end,
                'config_id' => $this->id
            ]);
        }
    }

    public function periods() {
        return $this->hasMany(Period::class, 'config_id');
    }

    public function activiteitenUnique() {
        $activiteiten = [];
        foreach ($this->periods as $period) {
            foreach($period->activiteiten as $activiteit) {
                if(!array_key_exists($activiteit->id, $activiteiten)) {
                    $activiteiten[$activiteit->id] = $activiteit;
                }
            }
        }
        return empty($activiteiten) ? null : array_values($activiteiten);
    }

    public function activiteiten() {
        $activiteiten = [];
        foreach ($this->periods as $period) {
            foreach($period->activiteiten as $activiteit) {
                array_push($activiteiten,$activiteit);
            }
        }
        return empty($activiteiten) ? null : $activiteiten;
    }


    public function school() {
        return $this->belongsTo(School::class, 'school_id');
    }

    public function startYear() {
        return (new Carbon($this->year_start))->year;
    }

    public function endYear() {
        return (new Carbon($this->year_end))->year;
    }

    public function schoolyear() {
        return $this->startYear() . '-' . $this->endYear();
    }
}
