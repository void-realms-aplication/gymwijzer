<?php

namespace App\Period;

use Illuminate\Database\Eloquent\Model;

class PeriodTemplate extends Model
{
    public function PeriodConfiguration() {
        return $this->belongsTo(PeriodConfigurationtemplate::class, 'config_id');
    }
}
