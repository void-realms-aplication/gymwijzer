<?php

namespace App\Period;

use App\Bewegingsactiviteit;
use Illuminate\Database\Eloquent\Model;

class Period extends Model
{
    protected $guarded = [];

    public function activiteiten() {
        return $this->belongsToMany(Bewegingsactiviteit::class, 'activity_period', 'period_id', 'activity_id');
    }

    public function configuration() {
        return $this->belongsTo(PeriodConfiguration::class, 'config_id');
    }

    public function assignActivity($bewegingsactiviteiten) {
        return $this->activiteiten()->sync($bewegingsactiviteiten);
    }
}
