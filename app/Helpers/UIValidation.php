<?php

function acceptedParameters() {
    return[
        'required' => 'empty',
        'email' => 'email',
        'accepted' => 'checked',
        'url' => 'url',
        'integer' => 'integer',
        'numeric' => 'number',
    ];
}

/**
 * @param $validation
 * @return array|false
 * Helper file by Slava. Accepts the validation in Laravel style format
 * Returns an array of the same validation in Semantic UI format)
 */

function UIValidation($request) {
    //grab the data first
    $validation = $request->rules();
    $messages = $request->messages();
    $fields = []; //the fields that are validated will be stored here
    $rules = []; //the (combined) validation rules will be stored here
    //those two arrays will later be merged into one neat output

    //loop through the validation rules
    foreach ($validation as $field=>$rule) {
        array_push($fields, $field); //store the field as is

        //skip the rule if there is anything with 'exclude_if' specified
        if(strpos(serialize($rule),'exclude_if') !== false) {
            array_push($rules,[]);
            continue;
        }

        //process the rule - unpack something like 'required|string|max:30' into an array
        $unpackedRule = is_array($rule) ? $rule : explode('|', $rule); //unless it's already an array

        $frontEndRule = array(); //semantic style validation rule will be put here
        $frontEndRules = array();

        //loop through the separate validation parameters
        foreach ($unpackedRule as $innerRule) {
            try{
                if(array_key_exists($innerRule, acceptedParameters())) {
                    $frontEndRule = getFrontEndRule($frontEndRule, $field, $innerRule, $messages);
                    array_push($frontEndRules, $frontEndRule);
                }
            } catch (Exception $e) {

            }
        }

        array_push($rules, $frontEndRules);

    }
    return array_combine($fields, $rules);
}

/**
 * Getting the right locale error message for a certain field&rule combination
 * @param array $frontEndRule
 * @param $field - The field we're checking (eg. 'email')
 * @param $rule - The rule this field is checked against (eg. 'required')
 * @param $messages - The custom defined messages in our specific validation
 * @return array Array of semantic friendly type and the right error message
 */
function getFrontEndRule(array $frontEndRule, $field, $rule, $messages): array
{
    $parameters = acceptedParameters();
    $frontEndRule['type'] = $parameters[$rule];

    //Priority one: check if custom validation message is defined in the Request
    if(array_key_exists($field . '.' . $rule, $messages)) {
        $validationMessage = $messages[$field . '.' . $rule];
    }
    //Priority two: check if a global custom validation messaged is defined in the locale
    elseif (Lang::has('validation.custom.' . $field . '.required')) {
        $validationMessage = Lang::get('validation.custom.' . $field . '.required');
    }
    //Priority three: check if fancy attributes are defined for this field
    elseif (Lang::has('validation.attributes.' . $field)) {
        $attribute = Lang::get('validation.attributes.' . $field);
        $validationMessage = Lang::get('validation.' . $rule, ['attribute' => $attribute]);
    }
    //All fails? Just echo the standard line with a field name!
    else {
        $validationMessage = Lang::get('validation.' . $rule, ['attribute' => $field]);
    }
    $frontEndRule['prompt'] = $validationMessage;
    return $frontEndRule;
}
