<?php

use App\GymwijzerPage;

function GWBreadcrumbs($leerlijn, $thema = null, $activiteit = null) {
    $parentsArray = [['name' => 'GymWijzer!', 'url' => route('start')], ['name'=>'Leerlijnen', 'url' => route('gymwijzer.index')]];
    $urlLinks = [];
    array_push($parentsArray, [
        'name' => $leerlijn->name,
        'url' => route('gymwijzer.leerlijn', $leerlijn->path)
    ]);

    if($thema) {
        array_push($parentsArray, [
            'name' => $thema->name,
            'url' => route('gymwijzer.bewegingsactiviteit', [$leerlijn->path, $thema->path])
        ]);
    }

    if($activiteit) {
        array_push($parentsArray, [
            'name' => $activiteit->name,
            'url' => route('gymwijzer.bewegingsthema', [$leerlijn->path, $thema->path, $activiteit->path])
        ]);
    }
    return $parentsArray;
}

//This is no longer necessary with the new GymWijzer data structure. shame to delete it tho :p
//#slava. Builds an associative array of 'name' => 'url' for all the parents of a GymWijzer page
//kids don't try this at home
function oldBuildParentsUrlArray(GymwijzerPage $page) {
    $parentsArray = [['name' => 'GymWijzer!', 'url' => route('start')], ['name'=>'Leerlijnen', 'url' => route('gymwijzer.index')]];
    $urlLinks = [];
    $parentItems = explode('/', $page->path); //break the parent path into pieces

    //build the url links array with the previous parents before
    foreach ($parentItems as $piece) {
        if ($urlLinks != []) { //first item?
            array_push($urlLinks, end($urlLinks) . '/' . $piece);
        } else {
            array_push($urlLinks, $piece);
        }
    }

    //now merge them together adding 'gymwijzer' before each url
    foreach ($parentItems as $key => $item) {
        array_push($parentsArray, ['name' => $parentItems[$key], 'url' => route('gymwijzer.page', $urlLinks[$key])]);

    }

    return $parentsArray;
}
