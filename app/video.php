<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $guarded = [];

    public function parent()
    {
        return $this->belongsTo(GymwijzerPage::class, 'bewegingsthema', 'name');
    }

    public function source(){
        return $this->link;
    }
}
