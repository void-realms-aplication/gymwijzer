<?php

namespace App\Console;


use App\Mail\AlmostExpiredMail;
use App\School;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Mail;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        $schedule->call(function (){

        })->dailyat('03:00');

        $schedule->call(function (){
            //checks if subscriptions are still valid.
            $schools = School::where('expired', '=', 'false')->get();


            foreach ($schools as $school) {
                //expires schoool
                if (($school->expires_at < Carbon::now()) && ($school->expires_at != null)) {
                    $school->expired = true;
                    $school->save();
                }
//                    send almost expired email
                $days = [14, 7, 3, 1];
                foreach ($days as $day) {
                    if (($school->expires_at == Carbon::today()->addDays($day)) && ($school->expires_at != null)) {
                        Mail::to($school->getAdmin->email)
                            ->send(new AlmostExpiredMail($school->getadmin, $day));
                    }
                }
            }
        })->dailyAt('01:00');

         }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
