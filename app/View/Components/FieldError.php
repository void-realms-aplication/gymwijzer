<?php

namespace App\View\Components;

use Illuminate\View\Component;

class fieldError extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return <<<'blade'
<div class="ui basic red pointing prompt label transition visible"
                                  style="display: inline-block !important;">{{ $slot }}</div>
blade;
    }
}
