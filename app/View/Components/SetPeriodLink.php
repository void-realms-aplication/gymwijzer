<?php

namespace App\View\Components;

use App\Period\PeriodConfigurationTemplate;
use Carbon\Carbon;
use Illuminate\View\Component;

class SetPeriodLink extends Component
{
    public $schoolyear;
    public $test = '123';

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $periodConfig = PeriodConfigurationTemplate::first();
        if(!$periodConfig) { return false; }
        $startYear = (new Carbon($periodConfig->year_start))->year;
        $endYear = (new Carbon($periodConfig->year_end))->year;
        $this->schoolyear = $startYear . '-' . $endYear;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return <<<'blade'
            {{ route('setPeriods', $schoolyear) }}
blade;
    }
}
