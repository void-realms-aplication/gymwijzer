<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kijkwijzer extends Model
{
    protected $guarded = [];
    public function activities()
    {
        return $this->belongsTo(Bewegingsactiviteit::class);
    }
}
