<?php

namespace App;

use App\Period\Period;
use App\Period\PeriodConfiguration;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Expr\Cast\Array_;

class Bewegingsactiviteit extends Model
{
    protected $guarded = [];

    protected $table = 'bewegingsactiviteiten';

    public function getRouteKeyName()
    {
        return 'path'; //automatically grab the activiteit from the query by it's path
    }

    public function parent() {
        return $this->belongsTo(Bewegingsthema::class, 'bewegingsthema_id');
    }

    //accepts $level as string e.g 'starter' or '1' or 'gevorderd'
    public function videos($level) {
        if ($level == null) {
            return []; //Bug fix for bewegingsactiviteit with no videos crashing the page!
        }
        return Video::where('bewegingsactiviteit', $this->name)
            ->where('niveau', array_flip($this->possibleLevels)[$level])->orderBy('volgorde')->get();
    }

    public function allVideos() {
        return Video::where('bewegingsactiviteit', $this->name)->get();
    }

    //returns levels in string format
    public function levels() {
        $levels = Video::where('bewegingsactiviteit', $this->name)->distinct('niveau')->orderBy('niveau')
            ->pluck('niveau')->toArray();

        if(!empty($levels)) {
            $output= array();
            foreach ($levels as $level) {
                array_push($output, $this->possibleLevels[$level]);
            }
            return $output;
        } else {
            return []; //returning null into @forelse in a blade template crashes it for some reason
        }
    }

    //For getting the correct string from level key and vice-versa (using array_flip ;)
    protected $possibleLevels = array(
        'starter',
        '1',
        '2',
        '3',
        '4',
        'gevorderd',
        '1 en 2',
        '3 en 4',
        'voor iedereen'
    );

    public function defaultLevel() {
        if (!empty($this->levels())) {
            return $this->levels()[0];
        } else {
            return null;
        }
    }

    /*
     *  Quickly check if the activity is connected to a given period
     *  #slava: if someone has a more clever way to do this hit me up!
     */
    public function isSetupForPeriod(Period $period) {
        foreach ($period->activiteiten as $activiteit) {
            if ($activiteit->id == $this->id) { return true; }
        }
        return false;
    }

    public function isPresentInPeriodConfiguration(PeriodConfiguration $config) {
        foreach ($config->periods as $period) {
            if($this->isSetupForPeriod($period)) {
                return true;
            }
        }
        return false;
    }

    public function countPresentInPeriodConfiguration(PeriodConfiguration $config) {
        $index = 0;
        foreach ($config->periods as $period) {
            if($this->isSetupForPeriod($period)) {
                $index++;
            }
        }
        return $index;
    }

    public function kijkwijzer(){
        return $this->hasOne(Kijkwijzer::class);
    }
}
