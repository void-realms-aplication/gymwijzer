<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

//#slava dependencies for the SetPassword feature
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\SetPw;

class User extends Authenticatable
{
    use Notifiable;

    //slava #sprint5. allow for quick User::create([~])..
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    public function assignRole($role)
    {
        $this->role()->associate($role);
        $this->save();
    }

    public function abilities()
    {
        return $this->role->abilities->pluck('name')->flatten()->unique();
    }


    public function schoolclasses()
    {
        return $this->belongsToMany(Schoolclass::class, 'schoolclass_gymteacher', 'user_id', 'schoolclass_id');
    }

    #Slava methods for initial password setup
    public function sendSetPasswordMail()
    {
        //Generate a token and hash it
        $pwSetToken = Str::random(32);
        DB::table('password_sets')->updateOrInsert(['email' => $this->email], //updateOrInsert - no need for duplicates
            [
                'email' => $this->email,
                'token' => \Hash::make($pwSetToken)
            ]);

        Mail::to($this->email)->send(new SetPw($this, $pwSetToken)); //SetPw is the mail template
    }

    //actually setting the password
    public function setPassword($newPw)
    {
        $this->password = Hash::make($newPw);
        $this->save();
    }

    public function getSchool()
    {
        return $this->belongsTo(School::class, 'school_id', 'id');
    }

    public function expired()
    {

        $school = $this->getSchool()->get();
        if ($school->isEmpty()) {
            return false;
        }
        return $school[0]->expired;
    }


    public function isStudent()
    {
        return $this->getSchool->isStudent();
    }

    public function isAdmin()
    {
        return $this->role_id == 3;
    }

    public function getLogo(){
        return $this->getSchool->getLogo();
    }

    public function enableLessonMode() {
        $this->lesson_mode = true;
        $this->save();
    }


}
