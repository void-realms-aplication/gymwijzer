<?php

namespace App;

use App\Period\PeriodConfiguration;
use App\Period\PeriodConfigurationTemplate;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $guarded = [];

    public function schoolclasses()
    {
        return $this->hasMany(Schoolclass::class);
    }

    public function gymTeachers()
    {
        return $this->hasMany(User::class);
    }

    public function subscription()
    {
        return $this->belongsTo(Subscription::class);
    }

    public function children()
    {
        return $this->hasMany(Child::class);
    }

    public function renew()
    {
        $expiresAt = $this->expires_at;
        if ($expiresAt < Carbon::today()) {
            $newDate = Carbon::today()->addYear();
        } else {
            $newDate = Carbon::create($expiresAt)->addYear();
        }
        $this->expires_at = $newDate;
        $this->expired = false;
        $this->save();

    }

    public function getAdmin()
    {
//        return user::where('school_id', $this->id)->where('role_id', 3);
        return $this->hasOne(User::class, 'school_id', 'id')->where('role_id', '=', 3);
    }

//    public function validTill(){
//        dd($this->expires_at);
//        return $this->expires_at;
//    }

    public function periodConfigurations()
    {
        return $this->hasMany(PeriodConfiguration::class);
    }

    public function nextYearSetupAvailable()
    {
        /*
         *  So far checks for the following things:
         *  - is there is a period configuration template IN THE FUTURE but less than 90 days away from now
         *  - if the school doesn't yet have a period configuration with that start day
         *  Conditions above pass? Then the school should be able to set a new period config (and true is returned)
        */

        foreach (PeriodConfigurationTemplate::all() as $configurationTemplate) {
            $yearStartFromNow = Carbon::parse($configurationTemplate->year_start)->diffInDays(Carbon::now());
            if ($yearStartFromNow < 90 && Carbon::now()->lessThan(Carbon::parse($configurationTemplate->year_start))) {

                foreach ($this->periodConfigurations as $configuration) {
                    if ($configuration->year_start == $configurationTemplate->year_start) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }

    public function isStudent()
    {
        return $this->subscription_id == 1;
    }

    public function getLogo()
    {
        return $this->logo;
    }

    public function amountOfChildren(){
        return $this->children->count();
    }
}
