<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leerlijn extends Model
{
    protected $guarded = [];

    protected $table = 'leerlijnen';

    public function getRouteKeyName()
    {
        return 'path'; //automatically grab the leerlijn from the query by it's path
    }

    public function themas() {
        return $this->hasMany(Bewegingsthema::class);
    }

    public function activiteiten() {
        return $this->hasManyThrough(Bewegingsactiviteit::class, Bewegingsthema::class);
    }
}
