<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bewegingsthema extends Model
{
    protected $guarded = [];

    public function parent() {
        return $this->belongsTo(Leerlijn::class, 'leerlijn_id');
    }

    public function activiteiten() {
        return $this->hasMany(Bewegingsactiviteit::class);
    }

    public function getRouteKeyName()
    {
        return 'path'; //automatically grab the thema from the query by it's path
    }


}
