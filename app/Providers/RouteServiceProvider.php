<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * The path to the "home" route for your application.
     *
     * @var string
     */

    //#slava: this is the path you'll get redirected to after logging in
    //(or if you're logged in and try to open a route that has 'guest' middleware
    public const HOME = '/'; //the standard value was '/home', we dont want that

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //slava: let's split our routes into different groups
        $this->mapUserRoutes();

        $this->mapPurchaseRoutes();

        $this->mapTestRoutes();

        $this->mapAdminRoutes();

        $this->mapGymWijzerRoutes();

        $this->mapTeacherRoutes();

        $this->mapPeriodRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));
    }

    // Define the custom route group for the User Invite via mail Feature
    protected function mapUserRoutes() {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/user.php'));
    }

    // Define the custom route group for purchasing subscriptions
    protected function mapPurchaseRoutes() {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/purchase.php'));
    }

    // Define the custom route group for some test routes
    protected function mapTestRoutes() {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/test.php'));
    }

    protected function mapAdminRoutes() {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/admin.php'));
    }

    protected function mapGymWijzerRoutes() {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/gymwijzer.php'));
    }

    // Define the custom route group for the theachers routes
    protected function mapTeacherRoutes() {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/teacher.php'));
    }

    protected function mapPeriodRoutes() {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/period.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }




}
