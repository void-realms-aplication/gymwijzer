<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\User;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
         'App\Schoolclass' => 'App\Policies\SchoolclassPolicy',
        'App\Leerlijn' => 'App\Policies\GymWijzerPolicy',
        'App\Bewegingsthema' => 'App\Policies\GymWijzerPolicy',
        'App\Bewegingsactiviteit' => 'App\Policies\GymWijzerPolicy',
        'App\Period\PeriodConfiguration' => 'App\Policies\PeriodPolicy',
        'App\Period\Period' => 'App\Policies\PeriodPolicy',
        'App\Child' => 'App\Policies\ChildPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::before(function ($user, $ability)
        {
            if ($user->abilities()->contains($ability))
            {
                return true;
            }

            //Devs may do anything
            if($user->role->name == 'dev') {
                return true;
            }

        });
        //

    }
}
