<?php

namespace App\Providers;

use App\View\Components\fieldError;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //slava
        $this->loadHelpers();

        \Carbon\Carbon::setLocale('nl_NL');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    //#slava load custom helpers
    //src: https://devdojo.com/devdojo/custom-global-helpers-in-laravel
    public function loadHelpers() {
        foreach (glob(__DIR__.'/../Helpers/*.php') as $filename) {
            require_once $filename;
        }
    }
}
