<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Support\Facades\Auth;

class CheckSubscription
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth::user();


        if (($user == null) || !($user->expired())) { //|| ($user->school_id == null)
            return $next($request);
        }
        return response(view('auth.verlopen', [
            'user' => $user,
            'lastNameAdmin' => $user->getSchool->getAdmin->lastname,
        ]));
    }
}
