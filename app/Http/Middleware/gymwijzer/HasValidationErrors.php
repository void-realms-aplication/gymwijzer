<?php

namespace App\Http\Middleware\gymwijzer;

use Closure;
use Session;
use Auth;

class HasValidationErrors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // We want to forbid the users simply opening /checkout when no POST form is submit
        // The following check makes sure that only GET request with session errors are let through
        #slava: this really took a while to figure out

        if($request->session()->get('error-origin') != 'subscription-to-payment') {
            if(!Auth::check()) {
                return redirect()->route('purchase.start');
            }
            return redirect()->route('start');
        }
        return $next($request);
    }
}
