<?php

namespace App\Http\Middleware\gymwijzer;

use Closure;
use App\Transaction;
use Carbon\Carbon;
use http\Env\Request;

class CheckActiveTransaction
{
    //TODO: maybe flash a notification to users when their transactions are expired?
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $transactionToken = $request->session()->get('transaction');
        if (!$transactionToken) {
            return $this->exitAccordingly($request, $next);
        }
        $transaction = Transaction::find($transactionToken);

        if (!$transaction || $transaction->status == 'Expired' || $transaction->status == 'Canceled') {
            //if no transaction with presented token is found in the DB (or is no longer active)-> flush the session
            return $this->exitAccordingly($request, $next);
        }

        //if we have a transacation which is succesfully paid: only access the sucess page!
        if ($transaction->status == 'Paid') {
            if ($request->route()->getName() == 'purchase.success') {
                return $next($request);
            } else {
                return redirect(route('purchase.success', $transaction->serverToken));
            }
        }

        if ($transaction->status == 'Open' && Carbon::now()->gt(Carbon::parse($transaction->expires))) {
            //if it was still in payment and now expired-> flush the session
            $transaction->status = 'Expired';
            $transaction->save();
            return $this->exitAccordingly($request, $next);
        }

        //All if's above fail? Then we must have an active payment transaction in process!
        if ($request->route()->getName() == 'purchase.view') {
            return $next($request);
        }
        return redirect(route('purchase.view'));

    }

    protected function exitAccordingly($request, Closure $next)
    {
        if ($request->route()->getName() == 'purchase.view' || $request->route()->getName() == 'purchase.success') {
            return redirect(route('purchase.start'));
        } else {
            $request->session()->forget('transaction');
            return $next($request);
        }
    }
}
