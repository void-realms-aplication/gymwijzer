<?php

namespace App\Http\Middleware\gymwijzer;

use Closure;
use http\Env\Request;
use Illuminate\Support\Facades\Auth;

class CheckLessonMode
{
    protected $safeRoutes = [
        'static-css',
        'exitLessonMode',
        'exitLessonModePOST',
        'start',
        'gymwijzer.index',
        'gymwijzer.leerlijn',
        'gymwijzer.bewegingsactiviteit',
        'gymwijzer.bewegingsthema'
    ];

    protected $fileExtensions = [
        '.jpg',
        '.png',
        '.mp4',
        '.css',
        '.js'
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::user() || in_array($request->route()->getName(), $this->safeRoutes)) {
            return $next($request);
        }

        if(in_array(substr($request->url(), -3), $this->fileExtensions) || in_array(substr($request->url(), -2), $this->fileExtensions)) {
            return $next($request);
        }

        if(Auth::user()->lesson_mode == true) {
            return redirect(route('exitLessonMode'));
        }

        return $next($request);
    }
}
