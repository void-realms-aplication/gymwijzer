<?php

namespace App\Http\Controllers;

//mail stuff, to be removed
use App\Mail\AlmostExpiredMail;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

use App\Subscription;

use App\School;
use Carbon\Carbon;
use Illuminate\Http\Request;


class HomepageController extends Controller
{
    public function index()
    {
        $subscriptions = Subscription::all();
        return view('homepage', ['subs' => $subscriptions]);
    }

    public function demo()
    {
        return view('gymwijzer.demo');
    }

    public function test()
    {

//        $user = auth::user();
//        $user = User::where('email', $user->email)->first();
//        $school = $user->getSchool;
//        $school->renew();
//        dd($school);

        $schools = School::where('expired', '=', 'false')->get();
        foreach ($schools as $school) {
            if (($school->expires_at < Carbon::now()) && ($school->expires_at != null)) {
                $school->expired = true;
                $school->save();
            }
//            } elseif (($school->expires_at == Carbon::today()->addWeeks(2)) && ($school->expires_at != null)) {
//                Mail::to($school->getAdmin->email)
//                    ->send(new AlmostExpiredMail($school->getadmin));
//            }
            $days = [14, 7, 3, 1];
            foreach ($days as $day) {
                if (($school->expires_at == Carbon::today()->addDays($day)) && ($school->expires_at != null)) {
                    Mail::to($school->getAdmin->email)
                        ->send(new AlmostExpiredMail($school->getadmin, $day));
                }
            }
        }
        return 201;
    }
}
