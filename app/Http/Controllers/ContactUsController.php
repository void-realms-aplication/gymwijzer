<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContactUs;
use App\mail\ContactUsMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Collection;


class ContactUsController extends Controller
{
    public function index()
    {
        return view('contact-us');
    }

    public function store(Request $request)
    {

        $validated = $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'subject' => 'required',
            'message' => 'required'
        ]);

        $title = 'Contact formulier';

        ContactUs::create($validated);

        Mail::to($validated['email'])
            ->bcc('info@gymwijzer.nl')
            ->send(new ContactUsMail($validated, $title));
        return view('contact-success');
    }

}
