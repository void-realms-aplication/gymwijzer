<?php

namespace App\Http\Controllers;

use App\Bewegingsactiviteit;
use App\Http\Requests\CreatePeriods;
use App\Http\Requests\StorePeriods;
use App\Http\Requests\UpdatePeriods;
use App\Period\PeriodConfigurationTemplate;
use App\School;
use Illuminate\Http\Request;
use App\Period\PeriodConfiguration;
use App\Period\Period;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;

class PeriodController extends Controller
{
    /*
     * TODO:
     *  - meldingen a la 'je hebt te weinig activiteiten ingesteld op de dashboard'
     *  - eventuele bug fixes
     */
    public function __construct()
    {
        $this->middleware('auth'); //Step 0: no auth - no setting periods
    }

    //URL input, user rights and a lot of other stuff extensively checked in --> CreatePeriods.php <--
    public function create(CreatePeriods $request) {
        return view('volgwijzer.periods.create' ,[
            'schoolyear' => $request->scooolyear[1] . '-' . $request->scooolyear[2],
            'school' => $request->school,
            'configurations' => $request->availableConfigurations
        ]);
    }

    //URL input, user rights and a lot of other stuff extensively checked in --> StorePeriods.php <--
    public function store(StorePeriods $request) {
        $template = PeriodConfigurationTemplate::find($request->config_id);

        //Step 1 create the period configuration
        $newconfig = PeriodConfiguration::create([
            'year_start' => $template->year_start,
            'year_end' => $template->year_end,
            'school_id' => $request->school->id,
            'max_activities' => $template->max_activities,
            'recommended_min_activities' => $template->recommended_min_activities,
            'recommended_max_activities' => $template->recommended_max_activities
        ]);

        //Step 2 create the according amount of periods connected to our new config
        foreach ($template->periods as $periodTemplate) {
            Period::create([
                'start' => $periodTemplate->start,
                'end' => $periodTemplate->end,
                'config_id' => $newconfig->id
            ]);
        }

        Session::flash('success-message',  'De periode configuratie is bepaald, stel nu de bewegingsactiviteiten in per periode!');
        return redirect(route('editPeriods', $newconfig->id));

    }

    public function edit(PeriodConfiguration $config) {
        $this->authorize('edit', $config); //This time the checks are far less complex!
        return view('volgwijzer.periods.edit', [
            'config' => $config,
            'periods' => $config->periods,
            'school' => \auth()->user()->getSchool,
            'bewegingsactiviteiten' => Bewegingsactiviteit::all()

        ]);
    }

    public function update(PeriodConfiguration $config, UpdatePeriods $request) {
        foreach ($request->parsedPeriods as $period => $activities) {
            $activities = $activities == null ? null : explode(",", $activities);
            $period = Period::find($period);
            $period->assignActivity($activities);
        }
        Session::flash('success-message',  'Bewegingsactiviteiten per periode zijn succesvol aangepast!');
        return redirect(route('admin.dashboard'));
    }
}
