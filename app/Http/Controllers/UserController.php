<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdatePass;
use App\Http\Requests\UpdateUser;
use App\User;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;
use App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpKernel\Profiler\Profile;

class UserController extends Controller
{
    public function __contruct()
    {
        $this->middleware('auth');
    }

    public function show()
    {
        $user = auth::user();
        $school = $user->getSchool;

        if ($school) {
            $admin = $school->getAdmin;
            return view('volgwijzer.mySchool.show', [
                'school' => $school,
                'admin' => $user->isAdmin(),
                'student' => $user->isStudent(),
                'firstName' => $user->firstname,
                'middleName' => $user->tussenvoegsel,
                'lastName' => $user->lastname,
                'email' => $user->email,
                'adresNummer' => $school->additive ? $school->house_number . " " . $school->additive : $school->house_number,
                'nameAdmin' => $admin->tussenvoegsel ? $admin->tussenvoegsel . " " . $admin->lastname : $admin->lastname,
                'validTill' => $school->expires_at ? Carbon::createFromFormat('Y-m-d H:i:s', $school->expires_at)->locale('nl_NL')->isoFormat('LL') : 'geen datum beschikbaaar',
                'validSinds' => $school->created_at ? Carbon::createFromFormat('Y-m-d H:i:s', $school->created_at)->locale('nl_NL')->isoFormat('LL') : 'geen datum beschikbaaar',
                'logo' => $school->getLogo() ? '/storage/' . $school->logo : '/img/gymwijzer-theme/placeholder.png'
            ]);
        } else {
            return view('volgwijzer.mySchool.show', [
                'admin' => 0,
                'school' => 0,
                'firstName' => $user->firstname,
                'middleName' => $user->tussenvoegsel,
                'lastName' => $user->lastname,
                'email' => $user->email,
            ]);
        }
    }

    public function edit()
    {
        $user = auth()->user();
        return view('volgwijzer.mySchool.edit-user', [
            'id' => $user->id,
            'firstName' => $user->firstname,
            'middleName' => $user->tussenvoegsel,
            'lastName' => $user->lastname,
            'email' => $user->email,
        ]);
    }

    public function update(UpdateUser $validated)
    {

        $user = auth()->user();


            $user->firstname = $validated['firstname'];
            array_key_exists('middelname', $validated) ? $user->tussenvoegsel = $validated['middelname'] : '';
            $user->lastname = $validated['lastname'];
            $user->save();

            $validated->session()->flash('success-message', 'Uw gegevens zijn gewijzigd!');
            return redirect(route('mijnAccount'));

    }

    public function password(UpdatePass $validated)
    {

        $user = auth()->user();
        if (password_verify($validated['oldPass'], $user->password)) {
            if ($validated['newPass'] === $validated['newPassRepeat']) {
                $user->setPassword($validated['newPass']);
                $validated->session()->flash('success-message', 'Uw gwachtwoord is gewijzigd!');
                return redirect(route('mijnAccount'));
            } else {

            }
        } else {
            Validator::make()->errors()->add('oldPass', 'U heeft uw oude wachtwoord vekeerd ingevuld.');
        }

    }
}
