<?php

namespace App\Http\Controllers;

use App\Child;
use App\Http\Requests\AddChild;
use App\Http\Requests\EdditChild;
use App\Http\Requests\EdditChildGroup;
use App\Http\Requests\EdditChildPass;
use App\School;
use App\Schoolclass;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use phpDocumentor\Reflection\Types\This;

class ChildController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($school)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Child $child)
    {
        $this->authorize('view', $child);
        $school = School::find(Auth::user()->school_id);
        return view('volgwijzer.children.viewChild', [
            'child' => $child,
            'school' => $school,
            'preOpenModal' => false,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showChildWithoutGroup(Child $child)
    {
        $this->authorize('view', $child);
        $school = School::find(Auth::user()->school_id);
        return view('volgwijzer.children.viewChildWithoutGroup', [
            'child' => $child,
            'school' => $school,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add(AddChild $formChild)
    {
        $user = auth()->user();
        $old_child_class_id = $formChild['GroupID'];
        $childData = $formChild->validated();

        $childData['password'] = Hash::make($childData['nieuwWachtwoord']);
        unset($childData['nieuwWachtwoord']);
        unset($childData['herhaalWachtwoord']);

        $childData['school_id'] = $user->school_id;

        $childData['schoolclass_id'] = $childData['GroupID'];
        unset($childData['GroupID']);

        $child = new Child($childData);
        $this->authorize('create', $child);

        Child::create($childData);

        Session::flash('success-message', 'kind succesvol aangemaakt!');
        if ($old_child_class_id != null)
        {
            return redirect(route('admin.schoolclass.viewgroup', $old_child_class_id));
        }
        return redirect(route('admin.children.without.class'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(EdditChild $formChild)
    {
        $child = Child::find($formChild->id);
        $this->authorize('update', $child);

        $old_child_class_id = $child->schoolclass_id;

        $childData = $formChild->validated();

        $haseGroup = $childData['haseGroup'];
        unset($childData['haseGroup']);

        $childData['schoolclass_id'] = $childData['groep'];
        unset($childData['groep']);
        $child->update($childData);

        Session::flash('success-message', 'kind succesvol aangepast!');
        if ($haseGroup == "true")
        {
            return redirect(route('admin.schoolclass.viewgroup', $old_child_class_id));
        }
        return redirect(route('admin.children.without.class'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edditGroup(EdditChildGroup $formChild)
    {
        $children = $formChild->children == null ? null : explode(",", $formChild->children);

        $childData = $formChild->validated();
        $childData['schoolclass_id'] = null;

        $schoolclass = Schoolclass::find($formChild->GroupID);
        $allChildrenFromClass = $schoolclass->children;

        foreach ($allChildrenFromClass as $child)
        {
            $this->authorize('update', $child);

            $child->update($childData);
        }
        $childData['schoolclass_id'] = $formChild->GroupID;
        if ($children != null)
        {
            foreach ($children as $childID)
            {
                $child = Child::find($childID);
                $this->authorize('update', $child);

                $child->update($childData);
            }
        }

        Session::flash('success-message', 'kind succesvol toegevoeg!');
        return redirect(route('admin.schoolclass.viewgroup', $formChild->GroupID));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function editPass(EdditChildPass $formChildPass)
    {
        $child = Child::find($formChildPass->id);
        $this->authorize('update', $child);

        $childData = $formChildPass->validated();

        unset($childData['preOpenModal']);
        $haseGroup = $childData['haseGroup'];
        unset($childData['haseGroup']);


        $childData['password'] = Hash::make($childData['nieuwWachtwoord']);
        unset($childData['nieuwWachtwoord']);
        unset($childData['herhaalWachtwoord']);
        $child->update($childData);

        Session::flash('success-message', 'Wachtwoord van kind succesvol aangepast!');
        if ($haseGroup == "true")
        {
            return redirect(route('admin.child.viewchild', $formChildPass->id));
        }
        return redirect(route('admin.child.viewchild_Withoutgroup', $formChildPass->id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        //
    }
}
