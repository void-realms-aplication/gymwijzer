<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterByInvite;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class RegisterByInviteController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function create(Request $request, $token) {
        if (!$this->checkRequestToken($token)) {
            return redirect()->route('start');
        }

        $valdiation = UIValidation(RegisterByInvite::create());
        return view('auth.reg-invite', ['token' => $token,'expectedEmail' => $request->input('email') ,
            'validation' => $valdiation]);
    }

    public function store(RegisterByInvite $request) {
        $user = $this->createUser($request);
//        $this->guard()->login($user); //doesnt work yet. TODO figure out way to instantly login user after this

        DB::table('user_invites')->where('email', $request->email)->delete(); //Delete the unnecessary pw token

        \Auth::login($user, true);

        Session::flash('success-message', 'Welkom ' . $user->name . '! Registratie voltooid.');
        return redirect()->route('start');
    }

    //Helper functions!
    protected function checkRequestToken($token) {
        $tokenValid = false;
        $existingTokens = DB::table('user_invites')->pluck('token');
        foreach ($existingTokens as $existingToken) {
            if (\Hash::check($token,$existingToken) == true) {
                $tokenValid = true;
            }
        }

       return $tokenValid;
    }

    protected function createUser(Request $request) {
        return User::create([
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'school_id' => DB::table('user_invites')->where('email', $request->email)->first()->school_id,
            'role_id' => Role::where('name', 'teacher')->first()->id
        ]);

    }
}
