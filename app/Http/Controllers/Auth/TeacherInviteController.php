<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\InviteTeacher;
use App\Mail\UserInvitation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Facades\Session;

use App\User;
use App\School;
/*
 *  #slava Controller to allow (dean accounts) to send email invite links which will let new users register
 *  Those new users will then also have teacher permissions and will be linked to the school
 */

class TeacherInviteController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:invite_teachers'); //permission check
    }

    public function create(Request $request) {
        $school = School::find($request->user()->school_id) ?? null;
        $validation = UIValidation(InviteTeacher::create(''));

        return view('gymwijzer.admin.inviteTeacher', ['school' => $school, 'validation' => $validation]);
    }

    public function store(InviteTeacher $request) {
        $inviteToken = Str::random(32);
        $inviteTokenHash = \Hash::make($inviteToken);

        DB::table('user_invites')->insert( //just insert, validation makes sure that there are no duplicates!
            [
                'email' => $request->email,
                'token' => $inviteTokenHash,
                //TODO: implement the expires option
                'invitedBy' => $request->user()->id,
                'school_id' => $request->user()->school_id ?? $request->school_id
            ]);

        //UserInvitation is the mail template: app/Mail/UserInvitation.php
        Mail::to($request->email)->send(new UserInvitation($inviteToken, $request->email, $request->user()));

        Session::flash('success-message', 'Uitnodiging successvol verstuurd!');
        return redirect()->route('admin.dashboard');
    }
}
