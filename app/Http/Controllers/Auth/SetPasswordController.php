<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddSubscriptionToCart;
use App\Http\Requests\SetPassword;
use Illuminate\Http\Request;
//#slava custom dependencies
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Support\Facades\Auth;

//#slava Controller to initially set a password for new empty user
//v0.2
class SetPasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    //Display the form for setting the password
    public function create($token) {
        $validation = UIValidation(SetPassword::create(''));
        return view('auth.passwords.set', ['token' => $token, //token passed as a hidden field
            'validation' => $validation
        ]);
    }

    //(Attempt to) setup the password
    public function store(SetPassword $request) {
        //Grab the user
        $user = User::where('email',$request->email)->first();

        //Update his password
        $user->setPassword($request->password);

        //Delete the unnecessary pw token
        DB::table('password_sets')->where('email', $request->email)->delete();

        //Log the user in
        \Auth::login($user, true);

        //Redirect back to login
        return redirect(route('login'));

        //TODO: (maybe) log the user into the system when done - HOW?
    }

}
