<?php

namespace App\Http\Controllers;

use App\Subscription;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Mollie\Laravel\Facades\Mollie;
use Carbon\Carbon;

class RenewController extends Controller
{
    public function index()
    {
//        dd(carbon::now());
        $user = auth::user();
        $expiresAt = Carbon::createFromFormat('Y-m-d H:i:s', $user->getSchool->expires_at)->locale('nl_NL');

        if ($user->abilities()->contains('renew_subscription')) {
            $transaction = Transaction::where('email', $user->email)->where('status', 'Open')->get()->first();
//            dd($transaction);
            if ($transaction == null) {
                return view('renewSub.renew', [
                    'admin' => true,
                    'validTill' => $expiresAt->isoFormat('LL')
                ]);
            } else {
                $payment = Mollie::api()->payments->get($transaction->mollieToken);
                // redirect customer to Mollie checkout page
                return redirect($payment->getCheckoutUrl(), 303);
            }
        } else {
            return view('renewSub.renew', [
                'admin' => false,
                'validTill' => $expiresAt->isoFormat('LL')
            ]);
        }


    }

    public function success($id)
    {
        $user = Auth::user();

        $transaction = Transaction::where('serverToken', $id)->firstOrFail();
        $expiresAt = Carbon::createFromFormat('Y-m-d H:i:s', $user->getSchool->expires_at)->locale('nl_NL');

        if ($transaction->status == 'Paid') {
            return view('renewSub.success', [
                'validTill' => $expiresAt->isoFormat('LL')
            ]);
        } else if ($transaction->status == 'Canceled') {
            return view('purchaseSub.canceled');
        } else {
            return view('purchaseSub.failed');
        }


//        dd($transaction);


    }
}
