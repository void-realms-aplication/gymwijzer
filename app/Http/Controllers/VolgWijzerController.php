<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VolgWijzerController extends Controller
{
    public function index()
    {
        $user = auth::user();

        return view('volgwijzer.index', [
            'role' => $user->getSchool->subscription_id
        ]);
    }

    public function demo()
    {

    }
}
