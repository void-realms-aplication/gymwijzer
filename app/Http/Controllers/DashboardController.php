<?php

namespace App\Http\Controllers;
use App\user;
use App\School;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\Types\Null_;

class DashboardController extends Controller
{
    private $abilities = ['dev_dashboard', 'master_dashboard', 'dean_dashboard', 'teacher_dashboard'];
    private $adminAbilities = ['dev_dashboard', 'master_dashboard'];
    private $schoolAbilities = ['dean_dashboard', 'teacher_dashboard'];
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $school = School::find(Auth::user()->school_id);
        $allSchoolTeachers = ($school == null) ? null : $school->gymTeachers;
        $allSchoolClasses = ($school == null) ? null : $school->schoolclasses;
        $assignedClasses = ($school == null) ? null : Auth::user()->schoolclasses;

        return view('gymwijzer.dashboard.dashboard', [
            'school' => $school,
            'allSchools' => School::all(),
            'allTeachers' => $allSchoolTeachers,
            'allClasses' => $allSchoolClasses,
            'theacherHasClasses' => $assignedClasses,
        ]);
    }

    public function school($selectedSchool)
    {
        foreach ($this->adminAbilities as $ability) {
            if (Auth::check() && in_array($ability, Auth::user()->abilities()->toArray())) {
                return ('de pagina voor {{$selectedSchool}} moet nog komen');
            }
        }
        return redirect('/');
    }
}
