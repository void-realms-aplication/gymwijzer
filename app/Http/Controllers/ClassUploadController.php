<?php

namespace App\Http\Controllers;

use App\Child;
use App\School;
use App\Schoolclass;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Mtownsend\XmlToArray\XmlToArray;

class ClassUploadController extends Controller
{
    public function index()
    {
        return view('admin-portal.upload.index');
    }

    public function store(Request $request)
    {

        $user = auth::user();

        if ($user->abilities()->contains('manage_children')) {
            $school = $user->getSchool;
            $request->validate([
                'file' => 'required|mimes:xml'
            ]);
            $file = $request->file('file');
            $xml = file_get_contents($file->getRealPath());
            $array = XmlToArray::convert($xml);
            $leerlingen = $array['leerlingen']['leerling'];


            $groepen = $array['groepen'];
            if (Arr::has($groepen, 'samengestelde_groep')) {
                dd('fout: samengestelde groepen kunnen we (nog) niet verwerken.');
            }
            $groepen = $groepen['groep'];
//            dd($leerlingen);

            if (Arr::has($leerlingen, 'achternaam')) {// check if case there is only one chiled
//                dd($user->getSchool->children->count());
                if ($user->getSchool->amountOfChildren <= $user->getSchool->subscription->max_students) {


                    $array = ['firstname' => $leerlingen['roepnaam'],
                        'tussenvoegsel' => $leerlingen['tussenvoegsel'],
                        'lastname' => $leerlingen['achternaam'],
                        'date_of_birth' => $leerlingen['geboortedatum'],
                    ];
                }
                else{
                    dd('te veel leerlingen');
                }

            } else {
                foreach ($leerlingen as $leerling) {

                }
            }


            if (Arr::has($groepen, 'naam')) { //if only 1 class is present in the edexml file
                $class = (Schoolclass::create([
                    'name' => $groepen['naam'],
                    'grade' => $groepen['jaargroep'],
                    'school_id' => $school->id
                ]));

                if (Arr::has($leerlingen, 'achternaam')) {//if there is only 1 chiled in the file

                    Child::create([
                        'firstname' => $leerlingen['roepnaam'],
                        'lastname' => $leerlingen['achternaam'],
                        'date_of_birth' => $leerlingen['geboortedatum'],
                        'password' => Hash::make($leerlingen['geboortedatum']),
                        'tussenVoegsel' => Arr::has($leerlingen, 'voorvoegsel') ? $leerlingen['voorvoegsel'] : '',
                        'schoolclass_id' => $class->id,
                        'school_id' => $school->id
                    ]);
                } else {
                    foreach ($leerlingen as $leerling) {

                        Child::create([
                            'firstname' => $leerling['roepnaam'],
                            'lastname' => $leerling['achternaam'],
                            'date_of_birth' => $leerling['geboortedatum'],
                            'password' => Hash::make($leerling['geboortedatum']),
                            'tussenVoegsel' => Arr::has($leerling, 'voorvoegsel') ? $leerling['voorvoegsel'] : '',
                            'schoolclass_id' => $class->id,
                            'school_id' => $school->id
                        ]);
                    }
                }
            } else { //if thare are multiple classes in the file
                foreach ($groepen as $groep) {

                    $class[$groep['@attributes']['key']] = (Schoolclass::create([
                        'name' => $groep['naam'],
                        'grade' => $groep['jaargroep'],
                        'school_id' => $school->id
                    ]));

                }
                if (Arr::has($leerlingen, 'achternaam')) {//if there is only 1 chiled in the file

                    Child::create([
                        'firstname' => $leerlingen['roepnaam'],
                        'lastname' => $leerlingen['achternaam'],
                        'date_of_birth' => $leerlingen['geboortedatum'],
                        'password' => Hash::make($leerlingen['geboortedatum']),
                        'tussenVoegsel' => Arr::has($leerlingen, 'voorvoegsel') ? $leerlingen['voorvoegsel'] : '',
                        'schoolclass_id' => $class[$leerlingen['groep']['@attributes']['key']]->id,
                        'school_id' => $school->id
                    ]);
                } else {

                    foreach ($leerlingen as $leerling) {

                        Child::create([
                            'firstname' => $leerling['roepnaam'],
                            'lastname' => $leerling['achternaam'],
                            'date_of_birth' => $leerling['geboortedatum'],
                            'password' => Hash::make($leerling['geboortedatum']),
                            'tussenVoegsel' => Arr::has($leerling, 'voorvoegsel') ? $leerling['voorvoegsel'] : '',
                            'schoolclass_id' => $class[$leerling['groep']['@attributes']['key']]->id,//$class['key']->id,
                            'school_id' => $school->id
                        ]);
                    }
                }
            }
            $request->session()->flash('alert-success', 'bestand succesvol geupload');
            return redirect()->route('admin.dashboard');

        } else {
            abort(403);
        }

    }
}
