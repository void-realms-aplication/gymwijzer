<?php

namespace App\Http\Controllers\GymWijzer;

use App\Bewegingsactiviteit;
use App\Bewegingsthema;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateLeerlijn;
use App\Http\Requests\EditLeerlijn;
use App\Leerlijn;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LeerlijnController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:manage,leerlijn')->except(['index', 'show', 'overview']);
    }

    //index only used on /gymwijzer/
    public function index()
    {
        return view('gymwijzer.index', [
            'leerlijnen' => Leerlijn::all()
        ]);
    }

    public function show(Leerlijn $leerlijn, Request $request)
    {
        $this->authorize('view', $leerlijn);

        return view('gymwijzer.list', [
            'page' => $leerlijn,
            'children' => $leerlijn->themas,
            'parentlink' => route('gymwijzer.index'),
            'urlPrefix' => route('gymwijzer.leerlijn', $leerlijn->path),
            'parents' => GWBreadcrumbs($leerlijn)
        ]);
    }

    //Display the view to create a leerlijn
    public function create() {
        $validation = UIValidation(CreateLeerlijn::create(''));
        return view('admin-portal.leerlijn.create', ['validation' => $validation]);
    }

    //Process a request to store a leerlijn
    public function store(CreateLeerlijn $request) {
        $leerlijn = $request->validated();

        Leerlijn::create($leerlijn);

        Session::flash('success-message',  'Leerlijn succesvol toegevoegd!');
        return redirect(route('gymwijzer.admin'));
    }

    public function edit(Leerlijn $leerlijn) {
        $validation = UIValidation(EditLeerlijn::create(''));
        return view('admin-portal.leerlijn.edit', ['leerlijn' => $leerlijn, 'validation'  => $validation]);
    }

    public function update(Leerlijn $leerlijn, EditLeerlijn $request) {
        $leerlijn->update($request->validated());
        Session::flash('success-message',  'Leerlijn succesvol aangepast!');
        return redirect(route('gymwijzer.admin'));
    }

    public function destroy(Leerlijn $leerlijn) {
        if($leerlijn->themas()->count() >= 1) {
            Session::flash('error-message',
                'Leerlijn ' . $leerlijn->name . ' bevat bewegingsthemas, verwijder deze eerst!');
            return redirect(route('gymwijzer.admin'));
        };
        $leerlijn->delete();
        Session::flash('success-message',  'Leerlijn succesvol verwijderd!');
        return redirect(route('gymwijzer.admin'));
    }

    public function overview(){
        $leerlijnen = Leerlijn::all();
        $beweginsthema = Bewegingsthema::all();
        $bewegingsactiviteit = Bewegingsactiviteit::all();

        return view('gymwijzer.overview',[
            'leerlijnen' => $leerlijnen,
            'beweginsthema'=> $beweginsthema,
            'bewegingsactiviteit' => $bewegingsactiviteit
        ]);

    }
}
