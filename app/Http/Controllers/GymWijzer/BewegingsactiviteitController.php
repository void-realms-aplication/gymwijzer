<?php

namespace App\Http\Controllers\GymWijzer;

use App\Bewegingsactiviteit;
use App\Bewegingsthema;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateBewegingsactiviteit;
use App\Http\Requests\EditBewegingsactiviteit;
use App\Leerlijn;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class BewegingsactiviteitController extends Controller
{
    /**
     *  No index is implemented. The system doesn't need to display ALL the themes
     *  The theme lists are only generated in a context of a 'Leerlijn'
     */

    public function __construct()
    {
        $this->middleware('can:manage,bewegingsactiviteit')->except('show');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $validation = UIValidation(CreateBewegingsactiviteit::create(''));
        $bewegingsthemas = Bewegingsthema::all();
        if ($bewegingsthemas->count() == 0) {
            Session::flash('error-message', 'Je kunt pas bewegingsactiviteiten toevoegen als er bewegingsthemas zijn');
            return redirect(route('gymwijzer.admin'));
        }
        return view('admin-portal.bewegingsactiviteit.create', ['validation' => $validation, 'bewegingsthemas' => $bewegingsthemas]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateBewegingsactiviteit $request)
    {
        $bewegingsactiviteit = $request->validated();
        $bewegingsactiviteit['bewegingsthema_id'] = Bewegingsthema::where('path', $request->bewegingsthema)->first()->id;
        unset($bewegingsactiviteit['bewegingsthema']);
        Bewegingsactiviteit::create($bewegingsactiviteit);

        Session::flash('success-message', 'Bewegingsactiviteit succesvol toegevoegd!');
        return redirect(route('gymwijzer.admin'));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Bewegingsactiviteit $bewegingsactiviteit
     * @return \Illuminate\Http\Response
     */
    public function show(Leerlijn $leerlijn, $themapath, $activiteitpath, Request $request)
    {
        //with the leerlijn get the according bewegingsactiviteit
        $bewegingsthema = Bewegingsthema::where('path', $themapath)->where('leerlijn_id', $leerlijn->id)
            ->firstOrFail();

        //with the bewegingsactiviteit get the according activity
        $bewegingsactiviteit = Bewegingsactiviteit::where('path', $activiteitpath)
            ->where('bewegingsthema_id', $bewegingsthema->id)->firstOrFail();

        $this->authorize('view', $bewegingsactiviteit);

        //grab the level from request, if nothing provided use a default one
        $niveau = $request->input('niveau', $bewegingsactiviteit->defaultLevel());
//        dd($bewegingsactiviteit->kijkwijzer);
        return view('gymwijzer.video', [
            'activiteit' => $bewegingsactiviteit,
            'parentlink' => route('gymwijzer.bewegingsactiviteit', [$leerlijn->path, $bewegingsthema->path]),
            'niveau' => $niveau,
            'parents' => GWBreadcrumbs($leerlijn, $bewegingsthema, $bewegingsactiviteit),
            'kijkwijzer' => $bewegingsactiviteit->kijkwijzer != null ? $bewegingsactiviteit->kijkwijzer->path : 'https://gymwijzer.nl/wp-content/uploads/Kijkje_in_de_gymzaal_hvdm.pdf'
        ]);

    }

    public function edit(Bewegingsactiviteit $bewegingsactiviteit)
    {
        $validation = UIValidation(EditBewegingsactiviteit::create(''));
        $bewegingsthemas = Bewegingsthema::all();
        return view('admin-portal.bewegingsactiviteit.edit', ['bewegingsactiviteit' => $bewegingsactiviteit,
            'validation' => $validation, 'bewegingsthemas' => $bewegingsthemas]);
    }

    public function update(EditBewegingsactiviteit $request, Bewegingsactiviteit $bewegingsactiviteit)
    {
        $update = $request->validated();
        $update['bewegingsthema_id'] = Bewegingsthema::where('path', $request->bewegingsthema)->first()->id;
        unset($update['bewegingsthema']);
        $bewegingsactiviteit->update($update);
        Session::flash('success-message', 'Bewegingsactiviteit succesvol aangepast!');
        return redirect(route('gymwijzer.admin'));
    }

    public function destroy(Bewegingsactiviteit $bewegingsactiviteit)
    {
        $bewegingsactiviteit->delete();
        Session::flash('success-message', 'Bewegingsactiviteit succesvol verwijderd!');
        return redirect(route('gymwijzer.admin'));
    }


}
