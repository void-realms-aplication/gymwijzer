<?php

namespace App\Http\Controllers\GymWijzer;

use App\Bewegingsthema;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateBewegingsthema;
use App\Http\Requests\EditBewegingsthema;
use App\Leerlijn;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class BewegingsthemaController extends Controller
{
    /**
     *  No index is implemented. The system doesn't need to display ALL the themes
     *  The theme lists are only generated in a context of a 'Leerlijn'
     */

    public function __construct()
    {
        $this->middleware('can:manage,bewegingsthema')->except('show');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $validation = UIValidation(CreateBewegingsthema::create(''));
        $leerlijnen = Leerlijn::all();

        if($leerlijnen->count() == 0) {
            Session::flash('error-message',  'Je kunt pas bewegingsthemas toevoegen als er leerlijnen zijn');
            return redirect(route('gymwijzer.admin'));
        }

        return view('admin-portal.bewegingsthema.create', ['validation' => $validation, 'leerlijnen' => $leerlijnen]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateBewegingsthema $request)
    {
        $bewegingsthema = $request->validated();
        $bewegingsthema['leerlijn_id'] = Leerlijn::where('path', $request->leerlijn)->first()->id;
        unset($bewegingsthema['leerlijn']);
        Bewegingsthema::create($bewegingsthema);

        Session::flash('success-message',  'Bewegingsthema succesvol toegevoegd!');
        return redirect(route('gymwijzer.admin'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bewegingsthema  $bewegingsthema
     * @return \Illuminate\Http\Response
     */
    public function show(Leerlijn $leerlijn, $themapath)
    {
        $bewegingsthema = Bewegingsthema::where('path', $themapath)->where('leerlijn_id', $leerlijn->id)
            ->firstOrFail();

        $this->authorize('view', $bewegingsthema);

        return view('gymwijzer.list', [
            'page' => $bewegingsthema,
            'children' => $bewegingsthema->activiteiten,
            'parentlink' => route('gymwijzer.leerlijn', $leerlijn->path),
            'urlPrefix' => route('gymwijzer.bewegingsactiviteit', [$leerlijn->path, $bewegingsthema->path]),
            'parents' => GWBreadcrumbs($leerlijn, $bewegingsthema)
        ]);
    }


    public function edit(Bewegingsthema $bewegingsthema)
    {
        $validation = UIValidation(Editbewegingsthema::create(''));
        $leerlijnen = Leerlijn::all();
        return view('admin-portal.bewegingsthema.edit', ['bewegingsthema' => $bewegingsthema,
            'validation'  => $validation, 'leerlijnen' => $leerlijnen]);
    }

    public function update(Editbewegingsthema $request, Bewegingsthema $bewegingsthema)
    {
        $update = $request->validated();
        $update['leerlijn_id'] = Leerlijn::where('path', $request->leerlijn)->first()->id;
        unset($update['leerlijn']);
        $bewegingsthema->update($update);
        Session::flash('success-message',  'Bewegingsthema succesvol aangepast!');
        return redirect(route('gymwijzer.admin'));
    }

    public function destroy(Bewegingsthema $bewegingsthema)
    {
        if($bewegingsthema->activiteiten()->count() >= 1) {
            Session::flash('error-message',
                'Bewegingsthema ' . $bewegingsthema->name . ' bevat bewegingsactiviteiten, verwijder deze eerst!');
            return redirect(route('gymwijzer.admin'));
        };
        $bewegingsthema->delete();
        Session::flash('success-message',  'Bewegingsthema succesvol verwijderd!');
        return redirect(route('gymwijzer.admin'));
    }
}
