<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddSubscriptionToCart;
use App\Http\Requests\PaymentWebhook;
use App\Http\Requests\SubscriptionProceedToPayment;
use App\School;
use App\Subscription;
use App\Transaction;
use Illuminate\Http\Request;

//custom dependencies
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Mollie\Laravel\Facades\Mollie;
use Session;
use Carbon\Carbon;

//#slava controller responsible for purchasing subscriptions
class PurchaseController extends Controller
{
    protected $purchaseTimeout = 31; //how many minutes max before a transaction expires

    public function __construct()
    {
        $this->middleware('guest');  //Ensure you can only get any of those purchase pages if you are not logged in!
        $this->middleware('transaction')->only(['start', 'checkout', 'success']);
    }

    // Initialize the purchase
    public function start(Request $request)
    {
        $validation = UIValidation(AddSubscriptionToCart::create(''));

        // Pre-select the subscription on the next page. Will be empty if nothing (or an invalid one) selected
        return view('purchaseSub.purchaseSubStep1', ['subscriptions' => Subscription::all(),
            'selected' => $request->subscription,
            'validation' => $validation]);
    }

    // Now trying to add the payment to cart and proceed to payment
    public function checkout(AddSubscriptionToCart $request)
    {
        $UIValidation = UIValidation(SubscriptionProceedToPayment::create(''));

        $selectedSubsription = $this->parseSubscription($request);
        $email = $this->parseEmail($request);
        return view('purchaseSub.purchaseSubStep2', ['subscriptions' => Subscription::all(),
            'email' => $email,
            'selected' => $selectedSubsription,
            'validation' => $UIValidation]
            );
    }

    //the success route.
    //should only be visible when the user is NOT logged in AND has a transaction session which's set to paid!
    //the middleware file takes care of that
    public function success($id)
    {

        $transaction = Transaction::where('serverToken', $id)->firstOrFail();
//        dd($transaction);
        if ($transaction->status == 'Paid') {
            return view('purchaseSub.purchaseSubStep4');
        } else if ($transaction->status == 'Canceled') {
            return view('purchaseSub.canceled');
        } else {
            return view('purchaseSub.failed');
        }


    }

    public function test(Request $request)
    {
        return $request->session()->all();
    }

    public function clear(Request $request)
    {
        $request->session()->forget('transaction');
        return 'Done';
    }

    // Couple of helper functions
    protected function parseSubscription(Request $request)
    {
        if ($request->old('subscription_id  ')) {
            $selectedSubsription = Subscription::find($request->old('subscription_id'));
        } elseif ($request->subscription_id) {
            $selectedSubsription = Subscription::find($request->subscription_id);
        } else {
            $selectedSubsription = Subscription::first();
        }
        return $selectedSubsription;
    }

    protected function parseEmail(Request $request)
    {
        if ($request->old('email')) {
            $email = $request->old('email');
        } elseif ($request->email) {
            $email = $request->email;
        } else {
            $email = null;
        }
        return $email;
    }


    protected function initUser(Transaction $transaction, $schoolId)
    {
        $user = User::create([
            'firstname' => $transaction->firstname,
            'tussenvoegsel' => $transaction->tussenvoegsel,
            'lastname' => $transaction->lastname,
            'email' => $transaction->email,
            'role_id' => 3,
            'school_id' => $schoolId
        ]);

        $user->sendSetPasswordMail();
    }

    protected function initSchool(Transaction $transaction)
    {
        $school = School::create([
            'email' => $transaction->email,
            'phone_number' => $transaction->phone_number,
            'brin_number' => $transaction->brin_number,
            'country' => $transaction->country,
            'city' => $transaction->city,
            'street' => $transaction->street,
            'house_number' => $transaction->house_number,
            'postal_code' => $transaction->postal_code,
            'number_of_students' => Subscription::find($transaction->subscription_id)->max_students,
            'subscription_id' => $transaction->subscription_id
        ]);

        return $school;
    }
}
