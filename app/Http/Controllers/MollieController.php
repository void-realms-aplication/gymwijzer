<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Mollie\Laravel\Facades\Mollie;
use App\Http\Requests\PaymentWebhook;
use App\Http\Requests\SubscriptionProceedToPayment;
use App\School;
use App\Subscription;
use App\Transaction;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use Illuminate\Support\Facades\Log;


class MollieController extends Controller
{


    // the test site ngrok.com, don't foget to add the endpint in the .env
    public function preparePayment(SubscriptionProceedToPayment $request)
    {
        $transaction = $request->validated();
        $transaction['status'] = 'Open';
        $transaction['renew'] = false;
        do {
            $serverToken = 'gw_' . Str::random(10);
        } while ((DB::table("transactions")->where('serverToken', '=', $serverToken))->exists());

        $transaction['serverToken'] = $serverToken;
        $subscription = Subscription::where('id', $request->subscription_id)->firstOrFail();
        $price = number_format($subscription->price, 2);
        $payment = Mollie::api()->payments->create([
            "amount" => [
                "currency" => "EUR",
                "value" => $price // You must send the correct number of decimals, thus we enforce the use of strings
            ],
            "description" => "gymwijzer" . $subscription->name . $request->lastname,
            "redirectUrl" => route('purchase.success', [$serverToken]),
            "webhookUrl" => route('purchase.webhook')

        ]);

        $transaction['mollieToken'] = $payment->id;
        Transaction::create($transaction);
        $request->session()->put('transaction', $serverToken);
        $payment = Mollie::api()->payments->get($payment->id);

        // redirect customer to Mollie checkout page
        return redirect($payment->getCheckoutUrl(), 303);
    }

    public function renewPayment()
    {
        $user = auth::user();
        if ($user->abilities()->contains('renew_subscription')) {

            $transaction = Transaction::where('email', $user->email)->where('status', 'Open')->get()->first();
//            dd($transaction);

            if ($transaction == null) {

                $school = $user->getSchool;
                do {
                    $serverToken = 'gw_' . Str::random(10);
                } while ((DB::table("transactions")->where('serverToken', '=', $serverToken))->exists());
//            dd($school);

                $subscription = Subscription::where('id', $school->subscription_id)->firstOrFail();
                $price = number_format($subscription->price, 2);
                $transaction = [
                    'email' => $user->email,
                    'status' => 'Open',
                    'firstname' => $user->firstname,
                    'tussenvoegsel' => $user->tussenvoegsel,
                    'lastname' => $user->lastname,
                    'company' => $school->name,
                    'brin_number' => $school->brin_number,
                    'country' => $school->country,
                    'city' => $school->city,
                    'street' => $school->street,
                    'house_number' => $school->house_number,
                    'additive' => $school->additive,
                    'postal_code' => $school->postal_code,
                    'phone_number' => $school->phone_number,
                    'serverToken' => $serverToken,
                    'renew' => true,
                    'subscription_id' => $school->subscription_id,
                ];

                //duplicate code, sorry world.
                $payment = Mollie::api()->payments->create([
                    "amount" => [
                        "currency" => "EUR",
                        "value" => $price // You must send the correct number of decimals, thus we enforce the use of strings
                    ],
                    "description" => "gymwijzer verlengen" . $subscription->name . $user->lastname,
                    "redirectUrl" => route('renew.success', [$serverToken]),
                    "webhookUrl" => route('purchase.webhook')

                ]);

                $transaction['mollieToken'] = $payment->id;
                Transaction::create($transaction);
                $payment = Mollie::api()->payments->get($payment->id);

                // redirect customer to Mollie checkout page
                return redirect($payment->getCheckoutUrl(), 303);
            } else {

                $payment = Mollie::api()->payments->get($transaction->mollieToken);

                // redirect customer to Mollie checkout page
                return redirect($payment->getCheckoutUrl(), 303);
            }
        } else {
            return redirect(route('renew'));
        }
    }


    /**
     * After the customer has completed the transaction,
     * you can fetch, check and process the payment.
     * (See the webhook docs for more information.)
     *
     */

    public function handlePayment(PaymentWebhook $request)
    {

        http_response_code(200);
        $mollie = new \Mollie\Api\MollieApiClient();
        $mollie->setApiKey("test_SMv45dR7DxcTBzTgxpWzhNyvEGQbfM");
        $mollie->testmode = true;
        $payment = $mollie->payments->get($request->id);
        $transaction = Transaction::where('mollieToken', $request->id)->first();

        if ($transaction->renew) {
            if ($payment->isPaid()) {
                $transaction->status = 'Paid';
                $transaction->save();
                $user = User::where('email', $transaction->email)->first();
                $user->getSchool->renew();

            }
            if ($payment->isFailed()) {
                $transaction->status = 'Failed';
                $transaction->save();
            }
            if ($payment->isCanceled()) {
                $transaction->status = 'Canceled';
                $transaction->save();
            }
            if ($payment->isExpired()) {
                $transaction->status = 'Expired';
                $transaction->save();
            }
        } else {
            if ($payment->isPaid()) {
                $transaction->status = 'Paid';
                $transaction->save();
                $school = $this->initSchool($transaction);
                $user = $this->initUser($transaction, $school->id);
            }
            if ($payment->isFailed()) {
                $transaction->status = 'Failed';
                $transaction->save();
            }
            if ($payment->isCanceled()) {
                $transaction->status = 'Canceled';
                $transaction->save();
            }
            if ($payment->isExpired()) {
                $transaction->status = 'Expired';
                $transaction->save();
            }
        }
    }

    protected function initSchool(Transaction $transaction)
    {
        if($transaction->company == null){
            $name = "De studenten school van " . $transaction->lastname;
        }
        else{
            $name = $transaction->company;
        }
        $school = School::create([
            'name' => $name,
            'email' => $transaction->email,
            'phone_number' => $transaction->phone_number,
            'brin_number' => $transaction->brin_number,
            'country' => $transaction->country,
            'city' => $transaction->city,
            'street' => $transaction->street,
            'house_number' => $transaction->house_number,
            'postal_code' => $transaction->postal_code,
            'number_of_students' => Subscription::find($transaction->subscription_id)->max_students,
            'subscription_id' => $transaction->subscription_id,
            'expires_at' => Carbon::today()->addYear()->addDay(),
        ]);

        return $school;
    }

    protected function initUser(Transaction $transaction, $schoolId)
    {
        $user = User::create([
            'firstname' => $transaction->firstname,
            'tussenvoegsel' => $transaction->tussenvoegsel,
            'lastname' => $transaction->lastname,
            'email' => $transaction->email,
            'role_id' => 3,
            'school_id' => $schoolId,

        ]);

        $user->sendSetPasswordMail();
    }
}

