<?php

namespace App\Http\Controllers;

use App\Bewegingsactiviteit;
use App\Bewegingsthema;
use App\GymwijzerPage;
use App\Leerlijn;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AdminPortalController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->authorize('manage', Leerlijn::class);

        $leerlijnen = Leerlijn::all();
        $themas = Bewegingsthema::all();
        $activiteiten = Bewegingsactiviteit::all();

        return view('admin-portal.index', [
            'leerlijnen' => $leerlijnen,
            'themas' => $themas,
            'activiteiten' => $activiteiten
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createLeerlijn()
    {
        $leerlijn = Leerlijn::all()->toArray();
        return view('admin-portal.create', ['leerlijn' => $leerlijn]);
    }

    public function createBewegingsthema()
    {
        $leerlijn = Leerlijn::all()->toArray();
        return view('admin-portal.createBewegingsthema', ['leerlijn' => $leerlijn]);
    }

    public function createBewegingsactiviteit()
    {
        $leerlijn = Leerlijn::all()->toArray();
        return view('admin-portal.createBewegingsactiviteit', ['leerlijn' => $leerlijn]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Leerlijn $leerlijn
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $leerlijn = new Leerlijn();

        $leerlijn->name = request('name');
        $leerlijn->parent_path = request('parent_path');
        $leerlijn->path = request('path');
        $leerlijn->leerlijnNummer = request('leerlijnNummer');
        $leerlijn->bewegingsthemaNummer = request('bewegingsthemaNummer');
        $leerlijn->bewegingsactiviteitNummer = request('bewegingsactiviteitNummer');
        $leerlijn->has_video = request('has_video');
        $leerlijn->demo = request('demo');

        $leerlijn->save();

        Session::flash('success-message',  $this->successMessage($request) . ' succesvol toegevoegd!'); //#slava
        return redirect('/admin-portal');
    }

    /**
     * Display the specified resource.
     *
     * @param  Leerlijn $leerlijn
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Leerlijn $leerlijn)
    {
        return view('admin-portal.show', ['leerlijn' => $leerlijn]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id, Leerlijn $request)
    {
        $leerlijn = Leerlijn::find($id);
        $request = Leerlijn::all()->toArray();
        return view('admin-portal.edit', ['leerlijn' => $leerlijn, 'request' => $request]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $leerlijn = Leerlijn::find($id);

        $leerlijn->name = request('name');
        $leerlijn->parent_path = request('parent_path');
        $leerlijn->path = request('path');
        $leerlijn->leerlijnNummer = request('leerlijnNummer');
        $leerlijn->bewegingsthemaNummer = request('bewegingsthemaNummer');
        $leerlijn->bewegingsactiviteitNummer = request('bewegingsactiviteitNummer');
        $leerlijn->has_video = request('has_video');
        $leerlijn->demo = request('demo');

        $leerlijn->save();

        Session::flash('success-message',  $this->successMessage($request) . ' succesvol aangepast!'); //#slava
        return redirect('/admin-portal/' . $leerlijn->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $leerlijn = Leerlijn::find($id);

        $leerlijn->delete();

        Return redirect('admin-portal');
    }

    protected function successMessage(Request $request) {
        if ($request->leerlijnNummer != null) { return 'Leerlin'; }
        if ($request->bewegingsthemaNummer != null) { return 'Bevegingsthema'; }
        if ($request->bewegingsactiviteitNummer != null) { return 'Bevegingsactiviteit'; }
    }
}
