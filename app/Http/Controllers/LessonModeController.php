<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

class LessonModeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function update(Request $request) {
        if($request->user()->lesson_mode != true) {
            return redirect(route('admin.dashboard'));
        }
        return view('exit-lesson-mode');
    }

    public function destroy(Request $request) {
        $validator = Validator::make($request->all(), ['password' => 'required']);

        $validator->after(function($validator) use ($request) {
            if(\Hash::check($request->password,$request->user()->password) == false) {
                $validator->errors()->add('password', 'De ingevulde wachtwoord is onjuist');
            }
        });

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $request->user()->lesson_mode = false;
        $request->user()->save();

        Session::flash('success-message',  'Uw identiteit is bevestigd en de les modus is afgesloten, welkom terug!');
        return redirect(route('admin.dashboard'));

    }
}
