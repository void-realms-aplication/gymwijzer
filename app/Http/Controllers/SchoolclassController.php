<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddSchoolclass;
use App\Http\Requests\EdditChildGroup;
use App\Http\Requests\EditLinkGymteacherSchoolclass;
use App\Schoolclass;
use App\School;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SchoolclassController extends Controller
{

    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($school)
    {
        $school = School::findOrFail($school);
        $this->authorize('create', [Schoolclass::class, $school]);

        return view('volgwijzer.schoolclass.create', ['school' => $school]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddSchoolclass $request, $school)
    {
        $school = School::findOrFail($school);
        $this->authorize('create', [Schoolclass::class, $school]);

        Schoolclass::create([
            'name' => $request->classname,
            'grade' => $request->grade,
            'email' => $request->email,
            'school_id' => $school->id
        ]);

        Session::flash('success-message', 'Groep succesvol toegevoegd!');
        return redirect(route('admin.dashboard'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Schoolclass  $schoolclass
     * @return \Illuminate\Http\Response
     */
    public function show(Schoolclass $schoolclass)
    {
        $this->authorize('view', $schoolclass);
        $allChildrenFromClass = $schoolclass->children;

        $user = auth()->user();
        $school = $user->getSchool;
        $allSchoolChildren = $school->children;
        $amountOfChildren = count($allSchoolChildren);
        for ($i = 0; $i < $amountOfChildren; $i++)
        {
            if ($allSchoolChildren[$i]->schoolclass_id != null && $allSchoolChildren[$i]->schoolclass_id != $schoolclass->id)
            {
                unset($allSchoolChildren[$i]);
            }
        }

        return view('volgwijzer.schoolclass.viewGroup', [
            'allChildrenFromClass' => $allChildrenFromClass,
            'schoolclass' => $schoolclass,
            'allChildrenWithoutAndFromClass' => $allSchoolChildren,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showNoGroup()
    {
        $user = auth()->user();
        if ($user->can('manage_own_school'))
        {
            $school = $user->getSchool;
            /*$NoneExistingGroup = new Schoolclass([
                'id' => null,
                'name' => 'NoneExisting',
            ]);*/
            $allSchoolChildren = $school->children;
            $amountOfChildren = count($allSchoolChildren);
            for ($i = 0; $i < $amountOfChildren; $i++)
            {
                if ($allSchoolChildren[$i]->schoolclass_id != null)
                {
                    unset($allSchoolChildren[$i]);
                }
            }

            return view('volgwijzer.viewClidren_withoutClasses', [
                'allChildrenWithoutGroup' => $allSchoolChildren,
            ]);
        }
        else
        {
            return redirect(route('admin.dashboard'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Schoolclass  $schoolclass
     * @return \Illuminate\Http\Response
     */
    public function edit(Schoolclass $schoolclass)
    {
        $school = School::find($schoolclass->school_id);
        $this->authorize('update', $schoolclass);
        $schoolGymTeachers = $school->gymteachers;
        $assignedGymTeachers = $schoolclass->gymTeachers()->get();

        return view('volgwijzer.schoolclass.edit', compact('school', 'schoolclass', 'schoolGymTeachers', 'assignedGymTeachers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Schoolclass  $schoolclass
     * @return \Illuminate\Http\Response
     */
    public function update(AddSchoolclass $request, Schoolclass $schoolclass)
    {
        $school = School::find($schoolclass->school_id)->first();
        $this->authorize('update', $schoolclass);

        $schoolClassData = $request->validated();
        $schoolClassData['name'] = $schoolClassData['classname'];
        unset($schoolClassData['classname']);
        $schoolclass->update($schoolClassData);

        Session::flash('success-message', 'Groep succesvol aangepast!');
        return redirect(route('admin.dashboard'));
    }

    public function updateTeachers(EditLinkGymteacherSchoolclass $request, Schoolclass $schoolclass) {
        $this->authorize('update', $schoolclass);
        $teachers = $request->teachers == null ? null : explode(",", $request->teachers);
        $schoolclass->assignTeachers($teachers);

        Session::flash('success-message', 'Docentenlijst succesvol aangepast!');
        return redirect(route('admin.dashboard'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Schoolclass  $schoolclass
     * @return \Illuminate\Http\Response
     */
    public function destroy(Schoolclass $schoolclass)
    {
        //
    }

}
