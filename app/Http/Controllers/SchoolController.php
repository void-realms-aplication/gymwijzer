<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateSchool;
use App\School;
use App\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use function foo\func;


class SchoolController extends Controller
{

    public function inviteTeacher(Request $request)
    {
        $this->authorize('invite_teacher');
        return view('gymwijzer.admin.inviteTeacher');

        dd('dat mag!');
    }

    public function sendTeacherInvite(Request $request)
    {
        $this->authorize('invite_teacher');
        $this->inviteByEmailValidation($request);

        dd($request->all());

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\School $school
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(School $school)
    {
        $user = auth::user();
        $school = $user->getSchool;

        return view('volgwijzer.mySchool.show', [
            'school' => $school,
            'user' => $user,
            'validTill' => $school->expires_at ? Carbon::createFromFormat('Y-m-d H:i:s', $school->expires_at)->locale('nl_NL') : 'geen datum beschikbaaar',
            'validSinds' => $school->created_at ? Carbon::createFromFormat('Y-m-d H:i:s', $school->created_at)->locale('nl_NL') : 'geen datum beschikbaaar',
            'logo' => $school->getLogo() ? '/storage/' . $school->getLogo : '/img/gymwijzer-theme/placeholder.png'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\School $school
     * @return \Illuminate\Http\Response
     */
    public function edit(School $school)
    {
        $user = auth()->user();
        $school = $user->getSchool;
        if (!$school) {
            abort(403);
        }
        $admin = $school->getAdmin;
        return view('volgwijzer.mySchool.edit-school', [
            'school' => $school,
            'nameAdmin' => $admin->tussenvoegsel ? $admin->tussenvoegsel . " " . $admin->lastname : $admin->lastname,
            'id' => $school->id
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\School $school
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSchool $validated)
    {
        $user = auth()->user();
        $school = $user->getSchool;

            $school->name = $validated['school_name'];
            $school->phone_number = $validated['phone_number'];
            $school->city = $validated['city'];
            $school->street = $validated['street'];
            $school->house_number = $validated['house_number'];
            $school->additive = $validated['additive'];
            $school->postal_code = $validated['postal_code'];
            $school->save();

            $validated->session()->flash('success-message', 'De gegevens van uw school zijn gewijzigd!');
            return redirect(route('mijnAccount'));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\School $school
     * @return \Illuminate\Http\Response
     */
    public function destroy(School $school)
    {
        //
    }

    protected function inviteByEmailValidation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users,email'
        ]);

        //#not the neatest but it should work
        $validator->after(function ($validator) use ($request) {
            if (Transaction::where([
                ['email', '=', $request->email],
                ['status', '=', 'payment']
            ])->orWhere([
                ['email', '=', $request->email],
                ['status', '=', 'paid']
            ])->exists()) {
                $validator->errors()->add('email', 'Momenteel wordt er een transactie verricht met deze email');
            }
        });

        $validator->validate();
    }
}
