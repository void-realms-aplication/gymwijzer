<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class LogoController extends Controller
{
    public function _constuct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = auth::user();
        if ($user->abilities()->contains('manage_logo')) {
            return view('volgwijzer.mySchool.logo', [
                'admin' => $user->isAdmin(),
                'logo' => $user->getSchool->getLogo(),
            ]);
        } else {
            abort(403);
        }

    }

    public function store(Request $request)
    {
//        dd($request->files);
        $user = auth::user();
        if ($user->abilities()->contains('manage_logo')) {

            request()->validate([
                'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:min_width=100,min_height=100,max_width=1000,max_height=1000' //'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10240'
            ]);

            $school = $user->getSchool;
            $logo = $request->file('logo');
            //            $name = Str::slug($request->input('name')).'_'.time();
            $name = date('Y-m-d:H') . "." . Str::kebab($school->name) . "." . Str::random(10) . '.' . $logo->getClientOriginalExtension();
            $folder = "/uploads/logos/";
            $filepath = $folder . $name;
            $logo->storeAs($folder, $name, 'public');
            $school->logo = $filepath;
            $school->save();

            $request->session()->flash('success-message', 'Uw logo is gewijzigd!');
            return redirect(route('mijnAccount'));
        } else {
            abort(403);
        }

    }

}
