<?php

namespace App\Http\Requests;

use App\Bewegingsactiviteit;
use App\Period\Period;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Session;

class UpdatePeriods extends FormRequest
{
    public $school;
    public $parsedPeriods = [];

    public function authorize()
    {
        $this->school = $this->user()->getSchool()->firstOrFail();
        return $this->user()->can('manage', [Period::class, $this->school]);
    }

    public function after($validator) {
        foreach ($this->all() as $key => $value) {
            if (strpos($key, 'activities-') !== false) {
                $this->parsedPeriods[substr($key,11)] = $value;
            }
        }

        foreach ($this->parsedPeriods as $period => $activities) {
            if(!in_array($period,$this->config->periods->pluck('id')->toArray())) {
                $validator->errors()->add('Activities', 'Fail'); //Tricking laravel, this message isnt displayed
                Session::flash('error-message',  'Er is een ongeldige periode met uw aanvraag ingevuld');
                return;
            } else {
                $activities = $activities == null ? null : explode(",", $activities);
                if($activities != null) {
                    foreach($activities as $activity) {
                        $activity = Bewegingsactiviteit::find($activity);
                        if(!$activity) {
                            $validator->errors()->add('Activities', 'Fail'); //Tricking laravel, this message isnt displayed
                            Session::flash('error-message',  'Er is een ongeldige bewegingsactiviteit met uw aanvraag ingevuld');
                            return;
                        }
                    }

                    if(count($activities) > $this->config->max_activities) {
                        $validator->errors()->add('Activities', 'Fail'); //Tricking laravel, this message isnt displayed
                        Session::flash('error-message',  'Er mogen maximaal ' . $this->config->max_activities . ' bewegingsactiviteiten gekoppeled worden aan één periode');
                        return;
                    }
                }


            }

        }

    }

    public function rules() { return []; } //No traditional style rule checking but w/o this method it crashes

    //trick to be able to use ::after in Request files.
    //src: https://laracasts.com/discuss/channels/general-discussion/l5-how-to-use-after-method-on-form-request
    protected function getValidatorInstance()
    {
        return parent::getValidatorInstance()->after(function($validator){
            $this->after($validator);
        });
    }
}
