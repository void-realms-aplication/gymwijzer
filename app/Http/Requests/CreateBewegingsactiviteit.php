<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateBewegingsactiviteit extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|unique:bewegingsactiviteiten',
            'path' => 'required|string|unique:bewegingsactiviteiten',
            'bewegingsthema' => 'required|string|exists:bewegingsthemas,path',
            'demo' => Rule::in([1, 0])
        ];
    }

    public function messages()
    {
        return [
            'name.unique' => 'Deze bewegingsactiviteit bestaat al.',
        ];
    }

}
