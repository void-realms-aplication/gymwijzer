<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;

class EditLinkGymteacherSchoolclass extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function after($validator) {
        if($this->teachers != null) {
            $teachers = explode(",", $this->teachers);
            foreach ($teachers as $teacherId) {
                $teacher = User::find($teacherId);
                if($teacher == null || $teacher->school_id != $this->route('schoolclass')->school_id) {
                    $validator->errors()->add('teachers', "De geselecteerde leraren zijn ongeldig");
                }
            }
        }
    }

    //trick to be able to use ::after in Request files.
    //src: https://laracasts.com/discuss/channels/general-discussion/l5-how-to-use-after-method-on-form-request
    protected function getValidatorInstance()
    {
        return parent::getValidatorInstance()->after(function($validator){
            // Call the after method of the FormRequest (see below)
            $this->after($validator);
        });
    }

}
