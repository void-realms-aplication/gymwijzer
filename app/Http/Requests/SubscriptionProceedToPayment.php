<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class SubscriptionProceedToPayment extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected $redirectRoute = 'purchase.checkout';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'firstname' => 'required|string|max:30',
            'tussenvoegsel' => 'nullable|string|max20',
            'lastname' => 'required|string|max:30',
            'company' => 'exclude_if:subscription_id,1|required|string|max:100',
            'country' => ['required', Rule::in(['nl','be'])],
            'city' => 'required|string|max:50',
            'street' => 'required|string|max:50',
            'house_number' => 'required|integer|digits_between:1,5',
            'additive' => 'nullable|string|max:20',
            'postal_code' => 'required|string|max:7',
            'email' => 'required|email|unique:users,email',
            'phone_number' => 'required|string|max:10',
            'brin_number' => 'exclude_if:subscription_id,1|required|string|max:6',
            'subscription_id' => 'required|exists:subscriptions,id'
        ];
    }

    public function messages()
    {
        return [

            'brin_number.min' => 'Een brin nummer moet minimaal 4 karakters lang zijn',
            'brin_number.max' => 'Een brin nummer moet minimaal 6 karakters lang zijn',
            'additive.max' => 'Een toevoeing mag maximaal 20 karaktgers lang zijn'

        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $this->session()->flash('error-origin', 'subscription-to-payment');
        throw (new ValidationException($validator))
            ->errorBag($this->errorBag)
            ->redirectTo($this->getRedirectUrl());
    }
}
