<?php

namespace App\Http\Requests;

use App\Period\Period;
use App\Period\PeriodConfigurationTemplate;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\ValidationException;

class CreatePeriods extends FormRequest
{

    public $school; //making it public makes sure $request->school works in the Controller ;)
    public $scooolyear; //same story here etc
    public $availableConfigurations = [];
    protected $redirectRoute = 'admin.dashboard';

    public function authorize()
    {
        //Step one - no school or no right to config periods? Stop!
        $this->school = $this->user()->getSchool()->firstOrFail();
        return $this->user()->can('manage', [Period::class, $this->school]);
    }

    public function after($validator) {
        //https://stackoverflow.com/questions/29578153/how-to-validate-route-parameters-in-laravel-5/45188652
        //Step two - valid schoolyear format provided in the URL? - REGEXP KIDS
        $extractedperiod = preg_match('/\b(\d{4})\b-\b(\d{4})\b/', $this->route('schoolyear'), $year);
        if(!$extractedperiod)
        {
            $validator->errors()->add('Schoolyear', 'Fail'); //Tricking laravel, this message isn't displayed
            Session::flash('warning-message',  'Ongeldige schooljaar ingevuld in de URL');
            return;
        }
        $this->scooolyear = $year;

        //Step 3 - any available period configurations for the schoolyear provided?
        $availableTemplates = false;
        foreach (PeriodConfigurationTemplate::all() as $template) {
            if((int)$year[1] == $template->startYear() && (int)$year[2] == $template->endYear()) {
                array_push($this->availableConfigurations, $template);
            }
        }
        if(empty($this->availableConfigurations)) {
            $validator->errors()->add('Schoolyear', 'Fail'); //Tricking laravel, this message isn't displayed
            Session::flash('warning-message',  'Geen configuraties beschikbaar voor de ingevulde schooljaar');
            return;
        }

        //Step 4 - check if period configuration for this year doesn't already exist.
        foreach ($this->school->periodConfigurations as $config) {
            if((int)$year[1] == $config->startYear() && (int)$year[2] == $config->endYear()) {
                $validator->errors()->add('Schoolyear', 'Fail'); //Tricking laravel, this message isnt displayed
                Session::flash('warning-message',  'De periodes voor deze school voor de ingevulde schooljaar zijn al ingesteld!');
                return;
            }
        }
    }

    public function rules() { return []; } //No traditional style rule checking but w/o this method it crashes

    //trick to be able to use ::after in Request files.
    //src: https://laracasts.com/discuss/channels/general-discussion/l5-how-to-use-after-method-on-form-request
    protected function getValidatorInstance()
    {
        return parent::getValidatorInstance()->after(function($validator){
            $this->after($validator);
        });
    }

}
