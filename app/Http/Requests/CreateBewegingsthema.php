<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Bewegingsthema;
use Illuminate\Validation\Rule;

class CreateBewegingsthema extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|unique:bewegingsthemas',
            'path' => 'required|string|unique:bewegingsthemas',
            'leerlijn' => 'required|string|exists:leerlijnen,path',
            'demo' => Rule::in([1, 0])
        ];
    }

    public function messages()
    {
        return [
            'name.unique' => 'Deze bewegingsthema bestaat al.',
            'leerlijn.exists' => 'Ongeldige leerlijn ingevoerd.'
        ];
    }

}
