<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Leerlijn;

class EditLeerlijn extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'path' => 'required|string',
            'demo' => Rule::in([1, 0])
        ];
    }

    public function after($validatior) {
        if($this->name != $this->leerlijn->name && Leerlijn::where('name', $this->name)->first() !== null) {
            $validatior->errors()->add('name', 'Er bestaat al een andere leerlijn met deze naam.');
        }

        if($this->path != $this->leerlijn->path && Leerlijn::where('path', $this->path)->first() !== null) {
            $validatior->errors()->add('path', 'Er bestaat al een andere leerlijn met deze pad.');
        }
    }

    //trick to be able to use ::after in Request files.
    //src: https://laracasts.com/discuss/channels/general-discussion/l5-how-to-use-after-method-on-form-request
    protected function getValidatorInstance()
    {
        return parent::getValidatorInstance()->after(function($validator){
            // Call the after method of the FormRequest (see below)
            $this->after($validator);
        });
    }

}
