<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;

class SetPassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|exists:users,email',
            'password' => 'required|min:8|same:password_confirmation'
        ];
    }

    public function messages()
    {
        return [
            'email.exists' => 'Er is geen gebruiker gevonden met deze E-Mail'
        ];
    }

    public function after($validator) {
        $correctToken = DB::table('password_sets')->where('email', $this->email)->value('token');

        if(\Hash::check($this->token,$correctToken) == false) { //Check if token is correct
            $validator->errors()->add('token', 'De meegegeven token is onjuist');
        }

    }

    //trick to be able to use ::after in Request files.
    //src: https://laracasts.com/discuss/channels/general-discussion/l5-how-to-use-after-method-on-form-request
    protected function getValidatorInstance()
    {
        return parent::getValidatorInstance()->after(function($validator){
            // Call the after method of the FormRequest (see below)
            $this->after($validator);
        });
    }
}
