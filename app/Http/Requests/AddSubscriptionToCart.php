<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;
use Session;
use Response;

class AddSubscriptionToCart extends FormRequest
{

    protected $redirectRoute = 'purchase.start';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users,email',
            'subscription_id' => 'required|exists:subscriptions,id'
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        if($this->getMethod() == 'POST') {
            throw (new ValidationException($validator))
                ->errorBag($this->errorBag)
                ->redirectTo($this->getRedirectUrl());

        }
    }


}
