<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSchool extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = auth()->user();
        return $user->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'school_name' => 'required|string|max:100',
            'phone_number' => 'required|string|max:10',
            'city' => 'required|string|max:50',
            'street' => 'required|string|max:50',
            'house_number' => 'required|integer|digits_between:1,5',
            'additive' => 'nullable|string|max:20',
            'postal_code' => 'required|string|max:7',
        ];
    }
    public function messages()
    {
        return [
            'school_name.required' => 'Vul alstublieft een school naam in.',
            'phone_number.required' => 'Vul alstublieft een telefoonnummer in.'
        ];
    }
}
