<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateLeerlijn extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|unique:leerlijnen',
            'path' => 'required|string|unique:leerlijnen',
            'demo' => Rule::in([1, 0])
        ];
    }

    public function messages()
    {
        return [
            'name.unique' => 'Deze leerlijn bestaat al.',
        ];
    }

}
