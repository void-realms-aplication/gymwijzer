<?php

namespace App\Http\Requests;

use App\Period\Period;
use App\Period\PeriodConfiguration;
use App\Period\PeriodConfigurationTemplate;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Session;

class StorePeriods extends FormRequest
{
    public $school; //making it public makes sure $request->school works in the Controller ;)
    public $scooolyear; //same story here etc
    protected $redirectRoute = 'start';

    /*
     * Validation for the POST request to store a period configuration for a school
     * A lot of this code will be a duplicate of CreatePeriods.php for basic validation
     * Not sure if there's a way to avoid that.
     */

    public function authorize()
    {
        //Step one - no school or no right to config periods? Stop!
        $this->school = $this->user()->getSchool()->firstOrFail();
        return $this->user()->can('manage', [Period::class, $this->school]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function after($validator) {
        //Step two - valid schoolyear format provided in the URL? - REGEXP KIDS
        $extractedperiod = preg_match('/\b(\d{4})\b-\b(\d{4})\b/', $this->route('schoolyear'), $year);
        if(!$extractedperiod)
        {
            $this->redirectRoute = 'admin.dashboard';
            $validator->errors()->add('Schoolyear', 'Fail'); //Tricking laravel, this message isn't displayed
            Session::flash('warning-message',  'Ongeldige schooljaar ingevuld in de POST request');
            return;
        }
        $this->scooolyear = $year;

        //Step 3 - check if the given period configuration is indeed available for this school atm
        $periodConfigAvailable = false;
        foreach(PeriodConfigurationTemplate::all() as $template) {
            if((int)$year[1] == $template->startYear() && (int)$year[2] == $template->endYear()) {
                $periodConfigAvailable = true;
                break; //no need to look further
            }
        }

        if($periodConfigAvailable == false) {
            $this->redirectRoute = 'admin.dashboard';
            $validator->errors()->add('Schoolyear', 'Fail'); //Tricking laravel, this message isnt displayed
            Session::flash('error-message',  'Ongeldige schooljaar meegegeven in de aanvraag!');
            return;
        }

        //Step 4 - check if period configuration for this year doesn't already exist.
        foreach ($this->school->periodConfigurations as $config) {
            if((int)$year[1] == $config->startYear() && (int)$year[2] == $config->endYear()) {
                $this->redirectRoute = 'admin.dashboard';
                $validator->errors()->add('Schoolyear', 'Fail'); //Tricking laravel, this message isnt displayed
                Session::flash('warning-message',  'De periodes voor deze school voor de ingevulde schooljaar zijn al ingesteld!');
                return;
            }
        }

        //Step 5 - is the given period configuration available?
        $template = PeriodConfigurationTemplate::find($this->config_id);
        if(!$template) {
            $this->redirectRoute = 'setPeriods';
            $validator->errors()->add('Schoolyear', 'Fail'); //Tricking laravel, this message isnt displayed
            Session::flash('error-message',  'Ongeldige periode configuratie ingevuld!');
            return;
        }

        /*
         *  Step 6 - imagine you can somehow possibly setup period configurations for multiple years
         *  Then you wouldn't want to be able to set it for year B from the setting page of year A
         */
        if($template->startYear() != $year[1] || $template->endYear() != $year[2]) {
            $this->redirectRoute = 'setPeriods';
            $validator->errors()->add('Schoolyear', 'Fail'); //Tricking laravel, this message isnt displayed
            Session::flash('error-message',  'De ingevoerde periode configuratie is niet beschikbaar voor de invoerde schooljaar!');
            return;
        }

    }

    //trick to be able to use ::after in Request files.
    //src: https://laracasts.com/discuss/channels/general-discussion/l5-how-to-use-after-method-on-form-request
    protected function getValidatorInstance()
    {
        return parent::getValidatorInstance()->after(function($validator){
            $this->after($validator);
        });
    }

    protected function getRedirectUrl()
    {
        $url = $this->redirector->getUrlGenerator();

        if($this->redirectRoute == 'setPeriods') {
            return $url->route($this->redirectRoute, $this->scooolyear[0]);
        } else {
            return $url->route($this->redirectRoute);
        }
    }
}
