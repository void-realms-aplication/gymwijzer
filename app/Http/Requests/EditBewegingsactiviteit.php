<?php

namespace App\Http\Requests;

use App\Bewegingsactiviteit;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EditBewegingsactiviteit extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'path' => 'required|string',
            'bewegingsthema' => 'required|string|exists:bewegingsthemas,path',
            'demo' => Rule::in([1, 0])
        ];
    }

    public function after($validatior) {
        if(strcasecmp ($this->name , $this->bewegingsactiviteit->name ) && Bewegingsactiviteit::where('name', $this->name)->first() !== null) {
            $validatior->errors()->add('name', 'Er bestaat al een andere bewegingsactiviteit met deze naam.');
        }

        if($this->path != $this->bewegingsactiviteit->path && Bewegingsactiviteit::where('path', $this->path)->first() !== null) {
            $validatior->errors()->add('path', 'Er bestaat al een andere bewegingsactiviteit met deze pad.');
        }
    }

    //trick to be able to use ::after in Request files.
    //src: https://laracasts.com/discuss/channels/general-discussion/l5-how-to-use-after-method-on-form-request
    protected function getValidatorInstance()
    {
        return parent::getValidatorInstance()->after(function($validator){
            // Call the after method of the FormRequest (see below)
            $this->after($validator);
        });
    }
}
