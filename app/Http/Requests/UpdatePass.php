<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdatePass extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'oldPass' => 'required|password',
            'newPass' => 'required|string|same:newPassRepeat',
            "newPassRepeat" => 'required|string|same:newPass'
        ];
    }

    public function messages()
    {
        return [
            'oldPass.required' => 'Vul alstublieft uw oude wachtwoord in.',
            'oldPass.password' => 'U heeft niet het goede wachtwoord ingevuld.',
            'newPass.required' => 'Vul astublieft een nieuw wachtwoord in',
            'newPassRepeat.required' => 'Vul astublieft nog een keer uw nieuwe wachtwoord in',
            'newPass.same' => 'De wachtwoorden die u heeft ingevuld zijn niet het zelfde.',
            'newPassRepeat.same' => 'De wachtwoorden die u heeft ingevuld zijn niet het zelfde.'
        ];
    }
}
