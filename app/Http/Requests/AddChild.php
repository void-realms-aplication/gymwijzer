<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddChild extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = auth()->user();
        if ($user->abilities()->contains('manage_own_school')) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'Required|String|Max:30',
            'lastname' => 'Required|String|Max:30',
            'tussenVoegsel' => 'Nullable|String|Max:30',
            'date_of_birth' => 'Required|date',
            'GroupID' => 'Nullable|Integer',
            'nieuwWachtwoord' => 'Required|String',
            'herhaalWachtwoord' => 'Required|String|same:nieuwWachtwoord',
        ];
    }
}
