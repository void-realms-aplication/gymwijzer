<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;

class RegisterByInvite extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => ['required', 'string', 'max:50'],
            'lastname' => ['required', 'string', 'max:50'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ];
    }

    public function messages()
    {
        return [
            'token.required' => 'Pleases provide an invite token.',
            'token.exists' => 'The provided invite token is invalid',
        ];
    }

    public function after($validator) {
        $correctToken = DB::table('user_invites')->where('email', $this->email)->value('token');
        if(\Hash::check($this->token,$correctToken) == false) { //Check if token is correct
            $validator->errors()->add('email', "The provided email and token don't match");
        }

    }

    //trick to be able to use ::after in Request files.
    //src: https://laracasts.com/discuss/channels/general-discussion/l5-how-to-use-after-method-on-form-request
    protected function getValidatorInstance()
    {
        return parent::getValidatorInstance()->after(function($validator){
            // Call the after method of the FormRequest (see below)
            $this->after($validator);
        });
    }


}
