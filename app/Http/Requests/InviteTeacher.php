<?php

namespace App\Http\Requests;

use App\Transaction;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;

class InviteTeacher extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users,email',
            'school_id' => 'exists:schools,id'
        ];
    }

    public function after($validator) {
        //Extra check 1: is there already an active invite sent to this mail?
        $existingInvite = DB::table('user_invites')->where('email', $this->email)->first();
        if ($existingInvite != null) {
            $validator->errors()->add('email', 'Er is al een uitnodiging naar deze email verstuurd');
        }

        //Extra check 2: maybe someone is trying to buy a subscription for this email at this very moment?
        if(Transaction::where([
            ['email', '=', $this->email],
            ['status', '=', 'payment']
        ])->orWhere([
            ['email', '=', $this->email],
            ['status', '=', 'paid']
        ])->exists()) {
            $validator->errors()->add('email', 'Momenteel wordt er een transactie verricht met deze email');
        }

    }

    //trick to be able to use ::after in Request files.
    //src: https://laracasts.com/discuss/channels/general-discussion/l5-how-to-use-after-method-on-form-request
    protected function getValidatorInstance()
    {
        return parent::getValidatorInstance()->after(function($validator){
            // Call the after method of the FormRequest (see below)
            $this->after($validator);
        });
    }

}
