<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Schoolclass;

class Child extends Model
{
    protected $guarded = [];

    public function results()
    {
        return $this->hasMany(Result::class);
    }

    public function setSchoolclass($schoolclass) {
        $class = Schoolclass::find($schoolclass);
        if($class == null) return false;
        return $this->update(['schoolclass_id' => $class->id]);
    }

    public function setSchool_id($school_id) {
        $school = School::find($school_id);
        if($school == null) return false;
        return $this->update(['school_id' => $school->id]);
    }

    public function school(){
        return $this->belongsTo(School::class,'school_id');
    }

    public function schoolClass(){
        return $this->belongsTo(Schoolclass::class,'schoolclass_id');
    }
}
