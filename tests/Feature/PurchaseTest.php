<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Session;
use Mail;

class PurchaseTest extends TestCase
{
    /**
     * Testing for the purchase routes being protected from logged in users.
     */

    /** @test */
    public function test_only_guests_can_access_purchase_routes()
    {
        $routes = [
            '/purchasesubscription',
            '/purchasesubscription/checkout',
            '/purchasesubscription/payment'
        ];

        $this->actingAs(factory(User::class)->create());

        foreach ($routes as $route)
            $response = $this->get($route);
            $response->assertStatus(302);
            $response->assertLocation('/');
    }

    /**
     * Testing validation for Step 1
     */

    /** @test */
    public function test_step_1_requires_an_email() {
        $this->submitStep1(['email' => ''])->assertSessionHasErrors('email');
    }

    /** @test */
    public function test_step_1_requires_a_unique_email() {
        $this->submitStep1(['email' => 'slava@syurin.com'])->assertSessionHasErrors('email');
    }

    /** @test */
    public function test_step_1_requires_a_subscription() {
        $this->submitStep1(['subscription_id' => ''])->assertSessionHasErrors('subscription_id');
    }

    /** @test */
    public function test_step_1_requires_a_valid_subscription() {
        $this->submitStep1(['subscription_id' => 25])->assertSessionHasErrors('subscription_id');
    }

    /*
     *  Testing validation for Step 2
     */
    /** @test */
    public function test_step_2_requires_firstname() {
        $this->submitSetp2(['firstname' => ''])->assertSessionHasErrors('firstname');
    }

    /** @test */
    public function test_step_2_requires_valid_country() {
        //They should all return an error except for 'nl' and 'be'
        $this->submitSetp2(['country' => 'uk'])->assertSessionHasErrors('country');
    }

    /** @test */
    public function test_step_2_brin_number_required_for_nl() {
        $this->submitSetp2(['country' => 'nl', 'brin_number' => ''])->assertSessionHasErrors('brin_number');
    }

    /** @test */
    public function test_step2_no_brin_number_required_for_be() {
        //Belgium selected? No brin number should be necessary now!
        $this->submitSetp2(['country' => 'be', 'brin_number' => ''])->assertSessionHasNoErrors();
    }

    /** etc etc! */

    /** Refactoring helper functions and stuff */
    protected function submitStep1($attributes) {
        $this->withExceptionHandling();
        return $this->post('/purchasesubscription/checkout', $this->validStep1Input($attributes));
    }

    /**
     * @return array
     * Valid form input for the step 1 of the purchase
     */
    protected function validStep1Input($overrides = [])
    {
        return array_merge([
            'email' => 'new@user.com',
            'subscription_id' => 3,
        ], $overrides);
    }

    protected function submitSetp2($attributes) {
        $this->withExceptionHandling();
        return $this->post('/mollie', $this->validStep2Input($attributes));
    }

    protected function validStep2Input($overrides = [])
    {
        return array_merge([
            'firstname' => 'Mark',
            'lastname' => 'Rutte',
            'company' => '',
            'country' => 'nl',
            'city' => 'Den Haag',
            'street' => 'Vloedlijn',
            'house_number' => 2,
            'postal_code' => '1759JW',
            'email' => 'markrutte@planet.nl',
            'phone_number' => '0224583535',
            'brin_number' => 'MR13',
            'subscription_id' => 4
        ], $overrides);
    }
}
