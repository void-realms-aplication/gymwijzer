<?php

namespace Tests\Feature;

use App\Leerlijn;
use App\School;
use App\User;
use http\Env\Response;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use phpDocumentor\Reflection\Types\Null_;
use phpDocumentor\Reflection\Types\This;
use Tests\TestCase;

class UsersTest extends TestCase
{
    use DatabaseTransactions;

    public function Give_School_With_Id($id)
    {
        $school = School::find($id);
        return $school;
    }
    public function Login_As_Fake_Admin()
    {
        $user = new User([
            'id' => 1,
            'name' => 'Fake',
            'role_id' => 1
        ]);
        $this->actingAs($user);
        return $user;
    }
    public function Login_As_Fake_Dean()
    {
        $user = new User([
            'id' => 2,
            'name' => 'Dean',
            'role_id' => 2
        ]);
        $this->actingAs($user);
        return $user;
    }
    public function Login_As_Test_SchoolAdmin()
    {
        $user = User::find(6);
        $this->actingAs($user);
        return $user;
    }
    public function Login_As_Test_SchoolTeacher()
    {
        $user = User::find(7);
        $this->actingAs($user);
        return $user;
    }


    /** @test */
    public function learning_lines_can_be_added()
    {
        // arrange
        $expectedCount = Leerlijn::All()->count() + 1;

        // act
        $this->Login_As_Fake_Admin();
        $response = $this->post('/admin-portal', [
            'name' => 'a22222',
            'path' => 'a22222',
            'leerlijnNummer' => '13',
            'has_video' => '0',
            'demo' => '0',
        ]);

        // assert
        $this->assertCount($expectedCount, Leerlijn::all());
    }

    /** @test */
    public function themas_can_be_added()
    {
        // arrange
        $expectedCount = Leerlijn::All()->count() + 1;

        // act
        $this->Login_As_Fake_Admin();
        $response = $this->post('/admin-portal', [
            'name' => 'a22222',
            'path' => '2a22222/a22222',
            'parent_path' => 'a22222',
            'bewegingsthemanummer' => '31',
            'has_video' => '0',
            'demo' => '0',
        ]);

        // assert
        $this->assertCount($expectedCount, Leerlijn::all());
    }

    /** @test */
    public function activities_can_be_added()
    {
        // arrange
        $expectedCount = Leerlijn::All()->count() + 1;

        // act
        $this->Login_As_Fake_Admin();
        $response = $this->post('/admin-portal', [
            'name' => 'a22222',
            'path' => '2a22222/a22222/a22222',
            'parent_path' => 'a22222/a22222',
            'bewegingsactiviteitNummer' => '52',
            'has_video' => '1',
            'demo' => '0',
        ]);

        // assert
        $this->assertCount($expectedCount, Leerlijn::all());
    }
    /** @test */
    public function learning_lines_can_be_updated()
    {
        // arrange
        $this->withoutExceptionHandling();
        $leerlijnOld = Leerlijn::create([
            'name' => 'a22222',
            'path' => 'a22222',
            'leerlijnNummer' => '13',
            'has_video' => '0',
            'demo' => '0',
        ]);
        $edditString = '/admin-portal/' . $leerlijnOld->id;
        // act
        $this->Login_As_Fake_Admin();
        $this->put($edditString, [
            'name' => 'b22222',
            'path' => 'b22222',
            'leerlijnNummer' => '13',
            'has_video' => '0',
            'demo' => '0',
        ]);
        $leerlijnNew = Leerlijn::find($leerlijnOld->id);
        // assert
        $this->assertEquals('b22222', $leerlijnNew->name);
    }

    /** @test */
    public function themas_can_be_updated()
    {
        // arrange
        $this->withoutExceptionHandling();
        $leerlijnOld = Leerlijn::create([
            'name' => 'a22222',
            'path' => '2a22222/a22222',
            'parent_path' => 'a22222',
            'bewegingsthemanummer' => '31',
            'has_video' => '0',
            'demo' => '0',
        ]);
        $edditString = '/admin-portal/' . $leerlijnOld->id;
        // act
        $this->Login_As_Fake_Admin();
        $this->put($edditString, [
            'name' => 'b22222',
            'path' => '2a22222/b22222',
            'parent_path' => 'a22222',
            'bewegingsthemanummer' => '31',
            'has_video' => '0',
            'demo' => '0',
        ]);
        $leerlijnNew = Leerlijn::find($leerlijnOld->id);
        // assert
        $this->assertEquals('b22222', $leerlijnNew->name);
    }

    /** @test */
    public function activities_can_be_updated()
    {
        // arrange
        $this->withoutExceptionHandling();
        $leerlijnOld = Leerlijn::create([
            'name' => 'a22222',
            'path' => '2a22222/a22222/a22222',
            'parent_path' => 'a22222/a22222',
            'bewegingsactiviteitNummer' => '52',
            'has_video' => '1',
            'demo' => '0',
        ]);
        $edditString = '/admin-portal/' . $leerlijnOld->id;
        // act
        $this->Login_As_Fake_Admin();
        $this->put($edditString, [
            'name' => 'b22222',
            'path' => '2a22222/a22222/b22222',
            'parent_path' => 'a22222/a22222',
            'bewegingsactiviteitNummer' => '52',
            'has_video' => '1',
            'demo' => '0',
        ]);
        $leerlijnNew = Leerlijn::find($leerlijnOld->id);
        // assert
        $this->assertEquals('b22222', $leerlijnNew->name);
    }
    /** @test */
    public function logged_in_users_can_see_dashboard()
    {
        // arrange
        // act
        $this->Login_As_Fake_Admin();
        $response = $this->get('/dashboard');
        // assert
        $response->assertOk();
    }
    /** @test */
    public function only_logged_in_users_can_see_dashboard()
    {
        // arrange
        // act
        $response = $this->get('/dashboard');
        // assert
        $response->assertRedirect('/login');
    }

    /** @test */
    public function developers_and_the_productOwner_can_see_admin_dashboard()
    {
        // arrange
        $users = ([
            $this->Login_As_Fake_Admin(),
            $this->Login_As_Fake_Dean(),
        ]);
        // act
        foreach ($users as $user)
        {
            $this->actingAs($user);
            $response = $this->get('/dashboard');
            // assert
            if (($user->role_id != 1) && ($user->role_id != 2)) {
                $this->fail('User not a dean');
            }
            else {
                $response->assertOk();
            }
        }
    }
    /** @test */
    public function School_Admin_allowed_to_add_school_classes()
    {
        // arrange
        $user = $this->Login_As_Test_SchoolAdmin();
        // act
        $response = $this->get(route('schoolclass.create', ['schoolId' => $user->school_id]));
        if ($user->role_id != 3) {
            self::fail("User is not a School Admin");
        }
        // assert
        $response->assertOk();
    }
    /** @test */
    public function developers_and_the_productOwner_can_add_classes_to_all_schools()
    {
        // arrange
        $users = ([
            $this->Login_As_Fake_Admin(),
            $this->Login_As_Fake_Dean(),
        ]);
        $schools = ([
            $this->Give_School_With_Id(1),
            $this->Give_School_With_Id(2),
        ]);
        // act
        for ($i = 0; $i < sizeof($users); $i++)
        {
            $this->actingAs($users[$i]);
            $response = $this->get(route('schoolclass.create', ['schoolId' => $schools[$i]->id]));
            if ($users[$i]->role_id != 1 && $users[$i]->role_id != 2) {
                self::fail("A user is not a developer or product owner");
            }
//        // assert
            $response->assertOk();
        }
    }
    /** @test */
    public function SchoolAdmin_Can_edit_klasses()
    {
        // arrange
        $user = $this->Login_As_Test_SchoolAdmin();
        $school = School::find($user->school_id);
        $AllSchoolClasse = $school->schoolclasses;
        $SchoolClasse = $AllSchoolClasse->first();
        // act
        $question = '/group/' . $SchoolClasse->id . '/edit';
        $response = $this->call('GET', $question);
        // assert
        $response->assertOk();
    }
    /** @test */
    public function SchoolTeacher_can_hase_klasses()
    {
        // arrange
        $user = $this->Login_As_Test_SchoolTeacher();
        // act
        $AllSchoolClasse = $user->schoolclasses;
        // assert
        $this->assertTrue($AllSchoolClasse != Null);
    }
}
