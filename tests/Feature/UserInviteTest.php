<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class UserInviteTest extends TestCase {
    /**
     * Testing if the functionality for inviting users is protected unless you have certain role
     */
    /** @test */
    public function test_teacher_cannot_invite_new_users() {
        $this->actingAs(factory(User::class)->create(['role_id' => 4]));
        $this->get('user/invite')->assertStatus(403);
        $this->post('user/invite')->assertStatus(403);
    }

    /** @test */
    public function test_deans_can_invite_new_users() {
        $this->actingAs(factory(User::class)->create(['role_id' => 3]));
        $this->get('user/invite')->assertOk();
    }

    /**
     * Testing if the register by Invite only apperars when a valid token is provided into the URL
     */
    /** @test */
    public function test_register_page_wont_display_with_wrong_url_token() {
        $response = $this->get('user/register/somewrongroken333');
        $response->assertStatus(302);
        $response->assertLocation('/');
    }

    /** @test */
    public function test_register_page_will_display_with_correct_token() {
        //First let's create a user invite
        $this->actingAs(factory(User::class)->create(['role_id' => 3, 'school_id' => 3])); //with dean rights & a school
        $this->post('user/invite', [
            'email' => 'somenewuser@gmail.com',
            'school_id' => 3
        ]);

        //Same story with tokens - modify it by hand for this test to avoid parsing sent emails or changing code
        $correctToken = 'correctToken999';
        $correctTokenHash = \Hash::make($correctToken);

        DB::table('user_invites')->where('email', 'somenewuser@gmail.com')->update(['token' => $correctTokenHash]);

        //We were logged in as a user, our register by invite token page will not let us through unless we're a guest
        $this->refreshApplication();

        $this->get('user/register/' . $correctToken)->assertOk();
    }

}
