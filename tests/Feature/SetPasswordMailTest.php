<?php

namespace Tests\Feature;

use App\Mail\SetPw;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Support\Facades\Mail;

class SetPasswordMailTest extends TestCase {
    /** @test */
    function test_mail_sent_to_new_user() {
        $newUser = factory(User::class)->create();

        Mail::fake();
        $newUser->sendSetPasswordMail();
        Mail::assertSent(SetPw::class);
    }

    /** @test */
    function test_new_user_cannot_set_pw_with_false_token() {
        $newUser = factory(User::class)->create();
        $newUser->sendSetPasswordMail();

        $this->submitRegAttempt(['email' => $newUser->email, 'token' => 'wrongtoken'])->assertSessionHasErrors('email');
    }

    /** @test */
    function test_new_user_can_set_pw_with_correct_token() {
        $newUser = factory(User::class)->create();
        $newUser->sendSetPasswordMail();

        //In the real application the actual raw token is not saved in the database, it is only mailed once
        //Because getting that token from an email would be quite a pain for now - we modify the token
        $correctToken = 'correctToken666';
        $correctTokenHash = \Hash::make($correctToken);
        //Save the hash in the database
        DB::table('password_sets')->where('email', $newUser->email)->update(['token' => $correctTokenHash]);

        //Submit the request with the unhashed token
        $this->submitRegAttempt(['email' => $newUser->email, 'token' => $correctToken])->assertSessionHasNoErrors();
    }

    protected function submitRegAttempt($attributes) {
        $this->withExceptionHandling();
        return $this->post('/password/set/',$this->validRegFormInput($attributes));
    }

    protected function validRegFormInput($overrides = []) {
        return array_merge([
            'password' => 'DSkl2lsdkfo3',
            'password_confirmation' =>'DSkl2lsdkfo3'
        ],$overrides);
    }
}
