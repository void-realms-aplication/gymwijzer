@extends('layouts.htmlStart', ['title' => 'GymWijzer! - Welkom', 'pageid' => 'frontpage'])

@section('spotlight')
    <section id="spotlight">
        <div id="spotlight-overlay" class="section-overlay"></div>
        @include('gymwijzer.partials.curve', ['position' => 'bottom', 'direction' => 'right-left'])
        <div id="spotlight-content" class="wrapper">
            <div id="gymwijzer-welcome">
                <h1 class="fancy-heading">GymWijzer!</h1>
                <h3>Een digitaal leermiddel voor het bewegingsonderwijs</h3>
            </div>
            <div id="spotlight-call-to-action">
                <a class="action-button active" href="#"><span class="btn-text">Kom meer te weten </span><i class="fa fa-chevron-right"></i> </a>
                <a class="action-button" href="{{ route('gymwijzer.index') }}"><span class="btn-text">Bekijk de demo</span> <i class="fa fa-chevron-right"></i></a>
                <a class="action-button" href="{{ route('purchase.start') }}"><span class="btn-text">Word nu lid</span> <i class="fa fa-chevron-right"></i></a>
            </div>
        </div>
    </section>
@endsection

@section('introduction')
    <section id="introduction">
        <div class="wrapper">
            <p id="gymwijzer-hello-text">GymWijzer! 4.0 is een digitaal leermiddel dat ondersteuning biedt bij het aanleren van bewegingsactiviteiten gebaseerd op de leerlijnen uit het Basisdocument bewegingsonderwijs voor het basisonderwijs en de activiteiten uit
                <a href="http://delesbrieven.nl/" target="_blank">‘De Lesbrieven’</a>. <br/><br/>
                Met behulp van video’s vanuit verschillende aanzichten en Kijkwijzers met foto’s en aanwijzingen, zien leerlingen wat ze kunnen uitvoeren en oefenen. Het doel hiervan is dat leerlingen met behulp van een laptop, tablet, smartphone of digibord, zelfstandig of gezamenlijk bewegingsactiviteiten eigen kunnen maken.</p>



            <div id="landing-features">
                <div class="landing-feature" id="feature-1">
                    <span class="feature-icon"><i class="fa fa-film" aria-hidden="true"></i></span>
                    <h3 class="feature-title">Duidelijke filmpjes</h3>
                    <p class="feature-description">1500 korte instructiefilmpjes zonder ruis, gemaakt met leerlingen van eigen leeftijd.</p>
                </div>
                <div class="landing-feature" id="feature-2">
                    <span class="feature-icon"><i class="fa fa-child" aria-hidden="true"></i></span>
                    <h3 class="feature-title">Voor iedereen</h3>
                    <p class="feature-description">Zowel individuele- als groepen leerlingen kunnen video's van verschillende niveaus bekijken.</p>
                </div>
                <div  class="landing-feature" id="feature-3">
                    <span class="feature-icon"><i class="fa fa-handshake" aria-hidden="true"></i></span>
                    <h3 class="feature-title">Betrouwbaar</h3>
                    <p class="feature-description">De 40 jaar ervaring in het bewegingsonderwijs in combinatie met de ICT vormt de sterke basis.</p>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('try-me')
    <section id="try-me" class="try-spotlight">
        <div id="try-me-overlay" class="section-overlay"></div>

        @include('gymwijzer.partials.curve', ['position' => 'top', 'direction' => 'right-left'])

        @include('gymwijzer.partials.curve', ['position' => 'bottom', 'direction' => 'left-right'])

        <div class="wrapper">
            <h2 class="fancy-heading">Probeer GymWijzer!</h2>
            <a href="{{ route('gymwijzer.index') }}" id="try-me-action-button" class="action-button"><span class="button-text">Start de gratis demo</span> <i class="fa fa-chevron-right"></i></a>
        </div>
    </section>
@endsection

@section('try-me-2')
    <section id="try-me2" class="try-spotlight">
        <div id="try-me2-overlay" class="section-overlay"></div>

        <div class="wrapper">
            <h2 class="fancy-heading">Probeer GymWijzer!</h2>
            <a href="{{ route('gymwijzer.index') }}" id="try-me-action-button" class="action-button"><span class="button-text">Start de gratis demo</span> <i class="fa fa-chevron-right"></i></a>
        </div>
    </section>
@endsection

@section('subscriptions')
    <section id="subscriptions">
        <div id="subscriptions-inner" class="wrapper">
            <div id="subscriptions-table">
                {{--verschillende subscriptions uit de database halen en op de pagina plaatsen
                run "php artisan db:seed --class=SubscriptionsSeeder" to fill the database--}}
                @foreach($subs as $subscription)
                    <div class="subscription">
                        <h3>{{$subscription->name}}</h3>
                        <span class="sub-title-border"></span>

                        <ul class="subscription-features">
                            <li>50 bewegingsactiviteiten</li>
                            <li>1500 instructievideo’s</li>
                            <li>400 kijkwijzers</li>
                            <li>LVS VolgWijzer!</li>
                            <li class="highlighted">{{$subscription->description}}</li>
                            <li>alleen beschikbaar met e-mailadres van opleiding</li>
                        </ul>

                        <div class="kopen-sub">
                            <span class="subscription-price">€{{$subscription->price}},- per jaar</span>

                            <a href="{{ route('purchase.start') }}" class="action-button" onclick="event.preventDefault(); document.getElementById('buy-subscriptipn-{{ $subscription->id}}').submit();">
                                Kies dit abonnement!
                            </a>

                            <form id="buy-subscriptipn-{{ $subscription->id}}" action="{{ route('purchase.start') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                                <input type="hidden" name="subscription" value="{{ $subscription->id}}">
                            </form>

                        </div>
                    </div>
                @endforeach
            </div>


            <p class="subscriptions-extra-info">Scholen binnen één bestuur of gemeente of meerdere studenten van een opleiding kunnen abonnementen combineren. Vraag naar de mogelijkheden via
                <br/><a href="/{{Route('contactUs')}}">het contactformulier</a>.
            </p>

        </div>

        @include('gymwijzer.partials.curve', ['position' => 'bottom', 'direction' => 'left-right'])

    </section>
@endsection

@section('content')
    @yield('spotlight')

    @yield('introduction')

    @yield('try-me')

    @yield('subscriptions')

    @yield('try-me-2')
@endsection
