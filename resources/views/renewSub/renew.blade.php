@extends('layouts.htmlStart', [
    'title' => "Verleng abonement | GymWijzer",
    'pageid' => 'renew'
    ])

@section('content')
    @include('gymwijzer.partials.breadcrumbs', ['heading' => 'GymWijzer! - Verleng abonement',
        'backurl' => route('start'),
        'crumbs' => [['name' => 'Home', 'url' => route('start')], ['name' => 'GymWijzer! - Verleng abonement', 'url' => url()->current()]]
        ])

    <div class="wrapper">
        @if($admin)
            <h1>Verleng</h1>
        <p>Uw abonement op de GymWijzer is verlengt en is geldig tot: {{$validTill}}</p>
            <p>U kunt uw abonement op de GymWijzer via deze pagina verlengen, zodra wij de betaling hebben ontvangen kun
                u uw abonement weer een jaar gebruiken. Als u uw abonement verlengt voor de eind datum wordt en een
                extra jaar bij opgeteld, als uw abonement al is verlopen is het nieuwe abonement een jaar vanaf vandaag
                geldig.</p>
            <a href="{{Route('mollie.renew')}}" class="action-button" style="max-width: 400px; width: 90%;">Verleng uw abonement.</a>
        @else
            <p>Uw abonement op de GymWijzer is geldig tot: {{$validTill}}</p>
            <p>Het abonement kan alleen verlengd worden door het contact persoon van uw organizatie.</p>
        @endif
    </div>

@endsection

