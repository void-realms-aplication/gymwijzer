@extends('layouts.htmlStart', [
    'title' => "Betaling voltooid | GymWijzer - Word lid",
    'pageid' => 'purchase-payment-success',
    'pageclasses' => 'purchase-page'
    ])

@section('content')
    @include('gymwijzer.partials.breadcrumbs', ['heading' => 'GymWijzer! - verlengen',
        'backurl' => route('admin.dashboard'),
        'crumbs' => [['name' => 'GymWijzer!', 'url' => route('start')], ['name' => 'Dashboard', 'url' => route('admin.dashboard')]]
    ])

    <div class="wrapper">

        <h1>Betaling voltooid!</h1>
        <p style="text-align: center">
            Uw abonement op de GymWijzer is verlengt en is geldig tot: {{$validTill}}.
        </p>

    </div>

@endsection
