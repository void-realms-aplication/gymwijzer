@if(Session::has('success-message'))
    <script>
        $('body')
            .toast({
                class: 'success',
                message: `{{ Session::get('success-message') }}`,
                displayTime: 'auto',
                showIcon: 'check'
            })
        ;
    </script>
@endif

@if(Session::has('info-message'))
    <script>
        $('body')
            .toast({
                message: `{{ Session::get('info-message') }}`,
                displayTime: 'auto',
            })
        ;
    </script>
@endif

@if(Session::has('error-message'))
    <script>
        $('body')
            .toast({
                message: `{{ Session::get('error-message') }}`,
                class: 'error',
                displayTime: 'auto',
                showIcon: 'exclamation'
            })
        ;
    </script>
@endif

@if(Session::has('warning-message'))
    <script>
        $('body')
            .toast({
                message: `{{ Session::get('warning-message') }}`,
                class: 'warning',
                displayTime: 'auto',
                showIcon: 'exclamation'
            })
        ;
    </script>
@endif
