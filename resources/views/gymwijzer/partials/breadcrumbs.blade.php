<div id="breadcrumbs-menu">
    <div class="wrapper">
        <div class="breadcrumbs-content">
            @isset ($heading) <h1>{{$heading}}</h1> @endisset
            @isset ($crumbs)
                <ul>
                    @foreach($crumbs as $crumb)
                        <a @if ($loop->last) class="breadcrumb-disabled" @endif href="{{ $crumb['url'] }}">{{$crumb['name']}}</a>
                    @endforeach
                </ul>
            @endisset
        </div>
        <div class="breadcrumbs-right">

                <a class="go-back-link" @if(isset($backurl)) href="{{$backurl}}" @else onclick="window.history.go(-1); return false;" @endif>
                    <span style="padding-right: 4px;" class="uk-margin-small-right uk-icon" uk-icon="icon: arrow-left"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" ratio="1"><polyline fill="none" stroke="#000" points="10 14 5 9.5 10 5"></polyline><line fill="none" stroke="#000" x1="16" y1="9.5" x2="5" y2="9.52"></line></svg></span><span class="breadcrmb-goback-text">Terug</span>
                </a>

        </div>
    </div>
</div>
