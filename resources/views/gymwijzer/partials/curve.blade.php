@if (isset($position) && isset($direction))
{{-- Do not output anything if position or direction are unset --}}
     {{-- Also, only output stuff if the position and direction have correct values --}}
    @if(($position == 'top' || $position == 'bottom') && ($direction == 'left-right' || $direction == 'right-left'))
    <div class="section-curve {{$position == 'top' ? 'top-curve' : 'bottom-curve' }}
        {{$direction == 'right-left' ? 'right-left-curve' : 'left-right-curve' }} ">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
                    <path class="elementor-shape-fill" d="M0,6V0h1000v100L0,6z"></path>
              </svg>
            </div>
     @endif
@endif
