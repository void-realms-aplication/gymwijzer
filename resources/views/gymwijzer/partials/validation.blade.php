@if(!isset($bypass)) {{$bypass = null }}
@endif

@if(count($validation))
    <script>
    $('.ui.form')
        .form({
            inline: true,
            fields : {
                @foreach($validation as $field=>$rules)
                    @if($bypass != 'all')
                        @if(!($bypass != null && in_array($field,$bypass)))
                            {{ $field }}: {
                                rules: [
                                    @foreach($rules as $rule)
                                        {
                                            type: '{{$rule['type']}}',
                                            prompt: '{{$rule['prompt']}}'
                                    } @if(!$loop->last),@endif
                                    @endforeach
                                ]

                        } @if(!$loop->last || @isset($custom)),@endif
                        @endif
                    @endif
                @endforeach
                {!! $custom ?? '' !!}
            }
        });
</script>
@endif

