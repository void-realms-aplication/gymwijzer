@extends('layouts.htmlStart', ['title' => $activiteit->name . ' | GymWijzer!'] )

@section('head')
    <link rel="stylesheet" href="/css/gymwijzerpages.css">

    <link href="https://vjs.zencdn.net/7.8.2/video-js.css" rel="stylesheet"/>

@endsection

@section('content')
    @include('gymwijzer.partials.breadcrumbs', ['heading' => $activiteit->name,
     'backurl' =>  $parentlink,
        'crumbs' => $parents
    ])

    <div class="wrapper videogrid-wrapper">

        <section class="niveau-button-section">

            @forelse($activiteit->levels() as $level)
                <a href="?niveau={{$level}}" class="gymwijzer-button
                    @if($niveau == $level) gymwijzer-button-doing
                    @elseif($niveau == "starter" or $level == "gevorderd") gymwijzer-button-todo
                    @elseif($niveau == "gevorderd" or $level == "starter" or $niveau > $level) gymwijzer-button-done
                    @else gymwijzer-button-todo
                    @endif "> Niveau {{$level}} </a>
            @empty
                <div class="ui warning icon message">
                    <i class="exclamation icon"></i>
                    <div class="content">
                        <div class="header">
                            Oops, deze pagina heeft geen inhoud
                        </div>
                        <p>Deze bewegingsactiviteit heeft nog geen niveaus en videos!</p>
                    </div>
                </div>
            @endforelse

        </section>

        @if (!empty($activiteit->levels()))
            <section class="videos-section">
                @forelse($activiteit->videos($niveau) as $video)
                    <div class="video-container">
                        <div class="video-title">{{$video->titel}}</div>
                        <div class="video-block">
                            <video oncontextmenu='return false;' controls loop
                                   class="video-js vjs-16-9 vjs-big-play-centered"
                                   data-setup='{ "playbackRates": [1, 0.5] }'>
                                <source src="{{$video->source()}}" type="video/mp4">
                                Uw browser support geen html5.
                            </video>
                        </div>
                    </div>
                @empty
                    <p>Oops geen videos gevonden!</p>
                @endforelse
            </section>

        @endif
        <section class="kijkwijzer_section">
            <div class="kijkwijzer_title">kijkwijzer</div>
            <iframe src="{{$kijkwijzer}}" width="100%" height="100%">
            </iframe>
        </section>
    </div>

    <script src="https://vjs.zencdn.net/7.8.2/video.js"></script>
@endsection
