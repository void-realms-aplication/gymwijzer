@extends('layouts.htmlStart')

@section('head')
    <link rel="stylesheet" href="/css/gymwijzerpages.css">
@endsection

@section('content')
    @include('gymwijzer.partials.breadcrumbs', ['heading' => $page->name,
       'backurl' =>  $parentlink,
        'crumbs' => $parents
    ])

    <div class="wrapper">
        <div id="gymwijzer-grid-wrap">
            @forelse($children as $child)
                <a class="gymwijzer-button @if(!Auth::check() and !$child->demo)gymwijzer-disabled-item @endif"
                   @if(Auth::check() or $child->demo)href="{{ $urlPrefix }}/{{$child->path}} @endif">
                    {{$child->name}}
                </a>
            @empty
                <div class="ui warning icon message">
                    <i class="exclamation icon"></i>
                    <div class="content">
                        <div class="header">
                            Oops, deze pagina heeft geen inhoud
                        </div>
                        <p>Deze pagina heeft nog geen beweingsthemas/activiteiten!</p>
                    </div>
                </div>
            @endforelse
        </div>

    </div>

@endsection
