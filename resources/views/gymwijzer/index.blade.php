@extends('layouts.htmlStart', ['title' => 'Leerlijnen - GymWijzer!', 'pageid' => 'gymwijzer-index'])

@section('head')
    <link rel="stylesheet" href="/css/gymwijzerpages.css">
@endsection

@section('content')
    @include('gymwijzer.partials.breadcrumbs', ['heading' => 'Leerlijnen',
        'backurl' => route('gymwijzer.index'),
        'crumbs' => [['name' => 'GymWijzer!', 'url' => route('start')], ['name' => 'Leerlijnen', 'url' => url()->current()]]
    ])

    <div class="wrapper">

            <div id="gymwijzer-grid-wrap">
                @foreach($leerlijnen as $leerlijn)
                    <a class="gymwijzer-button @if(!Auth::check() and !$leerlijn->demo)gymwijzer-disabled-item @endif"
                       @if(Auth::check() or $leerlijn->demo)href="/gymwijzer/{{ $leerlijn->path }} @endif">
                        {{ $leerlijn->name }}
                    </a>
                @endforeach
            </div>

        <a style="margin: 50px 0; max-width: 500px; display: block; margin-right: auto; margin-left: auto" class="gymwijzer-button" href="{{route('leerlijnen.overview')}}">Overzicht
            bewegings activiteiten</a>
    </div>

@endsection
