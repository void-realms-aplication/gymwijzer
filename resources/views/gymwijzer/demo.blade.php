@extends('layouts.htmlStart', ['title' => 'Demo - GymWijzer!', 'pageid' => 'gymwijzer-demo'])

@section('head')
    <link rel="stylesheet" href="/css/gymwijzerpages.css">
@endsection

@section('content')
    @include('gymwijzer.partials.breadcrumbs', ['heading' => 'Demo ',
        'backurl' => route('start'),
        'crumbs' => [['name' => 'GymWijzer!', 'url' => route('start')], ['name' => 'demo', 'url' => url()->current()]]
    ])

    <div class="wrapper">
        <h1>Demo</h1>
        <p>Als u geen abonement heeft op de GymWijzer kunt u toch de demo bekijken, via deze demo kunt u een idee
            krijgen hoe ons product werkt. De demo werkt volledig op de zelfde manier als het echte product maar er
            zijn minder bewegingsactiviteiten beschikbaar.</p>

        <p>We hebben op dit moment nog geen demo voor de VolgWijzer maar u kunt wel schreenshot’s bekijken van het
            gebruik van de volgwijzer.</p>

        <div id="gymwijzer-grid-wrap">
            <a class="action-button" style="max-width: 500px; width: 90%;" href="{{route('gymwijzer.index')}}">Klik hier
                voor de GymWijzer demo</a>
            <a class="action-button" style="max-width: 500px; width: 90%;" href="{{route('volgwijzer.demo')}}">Klik hier om schreenshot's van de
                VolgWijzer te bekijken</a>
        </div>
    </div>

@endsection
