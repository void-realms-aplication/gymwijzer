@extends('layouts.htmlStart', ['title' => 'GymWijzer! - Dashboard', 'pageid' => 'Dashboard'])

@section('school_info')
    @if($school != null)
        <div class="wrapper">
            <h3>Informatie over uw school:</h3>
            <ul>
                <li>- Uw huidige abonnement is een <b>{{$school->subscription->name}} abonnement.</b></li>
                {{--                <li>- Uw school ID in onze database is {{$school->id}}</li>--}}
            </ul>
        </div>
    @endif
@endsection

@section('schools_table')

    <div class="ui piled segment">
        @if($allSchools != null)
            <h4 class="ui header">Overzicht scholen</h4>

            <table class="ui celled table">
                <thead>
                <tr>
                    <th>Naam School</th>
                    <th>Stad</th>
                    <th>Telefoon Nummer</th>
                    <th>Aantal Kinderen</th>
                    <th>Abonnement</th>
                    <th>Bewerken</th>
                </tr>
                </thead>
                <tbody>

                @foreach($allSchools as $oneSchool)
                    <tr>
                        <td data-label="Name School">{{$oneSchool->name}}</td>
                        <td data-label="Stad">{{$oneSchool->city}}</td>
                        <td data-label="Telefoon Nummer">{{$oneSchool->phone_number}}</td>
                        <td data-label="Aantal Kinderen">{{count($oneSchool->children)}}</td>
                        <td data-label="Subscription">{{$oneSchool->subscription->name}}</td>
                        <td data-label="Bewerk">
                            <a href="{{ url('/' . $oneSchool->id . '/edit') }}" class="Edit-button">Bewerken</a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        @else
            Momenteel zijn er geen scholen aangemeld!

        @endif
    </div>

@endsection

@section('teachers_table')
    @if($allTeachers != null)
        <table class="ui celled table">
            <thead>
            <tr>
                <th>Voornaam</th>
                <th>Tussenvoegsel</th>
                <th>Achternaam</th>
                <th>Emailadres</th>
                <th>Gekoppelde Groepen</th>
                <th>Bewerken</th>
            </tr>
            </thead>
            <tbody>
            @foreach($allTeachers as $thisSchoolUsers)
                <tr>
                    <td data-label="Voornaam">{{$thisSchoolUsers->firstname}}</td>
                    <td data-label="Tussenvoegsel">{{$thisSchoolUsers->tussenvoegsel}}</td>
                    <td data-label="Achternaam">{{$thisSchoolUsers->lastname}}</td>
                    <td data-label="Emailadres">{{$thisSchoolUsers->email}}</td>
                    <td data-label="Gekoppelde Groepen">
                        @if(count($thisSchoolUsers->schoolclasses) == 1)
                            {{$thisSchoolUsers->schoolclasses[0]->name}}
                        @elseif(count($thisSchoolUsers->schoolclasses) >= 2)
                            <div class="ui compact menu">
                                <div class="ui simple dropdown item">
                                    {{count($thisSchoolUsers->schoolclasses)}} groepen
                                    <i class="dropdown icon"></i>
                                    <div class="menu">
                                        @foreach($thisSchoolUsers->schoolclasses as $class)
                                            <div class="item">{{$class->name}}</div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @else
                            Geen
                        @endif
                    </td>
                    <td data-label="Bewerken">
                        <a href="{{ url('/dashboard/editTeacher/' . $school . '/'. $thisSchoolUsers->id) }}"
                           class="Edit-button">Bewerken</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <a class="dashboard-toevoeg-button" type="button" href='{{ route('teacher.invite')}}'>Nodig een gym docent
            uit</a>
    @endif
@endsection

@section('periods')
    @if($school != null)
        @if($school->periodConfigurations->isEmpty())
            <div id="period-setup-warning" class="ui warning icon message">
                <div class="header">
                    Configureer de periodes voor uw school
                </div>
                <div class="content">Voordat u door kunt met VolgWijzer! moet u instellen hoeveel rapport periodes per
                    jaar u wilt hanteren. <br/>
                    Gebruik daar <a href="<x-set-period-link></x-set-period-link>">deze pagina</a> voor.
                </div>
            </div>
        @else
            <a href="{{ route('editPeriods', $school->periodConfigurations->first()->id ) }}"
               class="ui large yellow fluid button">Bewerk de periode configuratie</a>
        @endif
    @endif
@endsection

@section('schoolgroups_table')
    @if($allClasses != null)
        <table class="ui celled table">
            <thead>
            <tr>
                <th>Groep Naam</th>
                <th>Leerjaar</th>
                <th>Groeps Docent</th>
                <th>Aantal leerlingen</th>
                <th>Bewerken</th>
                <th>Bekijken</th>
            </tr>
            </thead>
            <tbody>
            @foreach($allClasses as $oneClass)
                <tr>
                    <td data-label="ClassName">{{$oneClass->name}}</td>
                    <td data-label="SchoolYear">Groep {{$oneClass->grade}}</td>
                    <td data-label="Klasse Docent">{{$oneClass->email}}</td>
                    <td data-label="ClassSize">{{count($oneClass->children)}}</td>
                    <td data-label="Bewerken">
                        <a href="{{ route('schoolclass.edit', $oneClass->id) }}" class="Edit-button">Gegevens
                            Bewerken</a>
                    </td>
                    <td data-label="Bekijken">
                        <a href="{{ route('admin.schoolclass.viewgroup', $oneClass->id) }}" class="Edit-button">Groep
                            Bekijken</a>
                    </td>
                </tr>
            @endforeach
            <tr>
                <td colspan="6">
                    <a class="dashboard-toevoeg-button" type="button" href='{{ route('admin.children.without.class')}}'>
                        niet aan een groep gekopelde leerlingen
                    </a>
                </td>

            </tr>
            </tbody>
        </table>
        <a class="dashboard-toevoeg-button" type="button" href='{{ route('schoolclass.create',$school->id)}}'>Voeg een
            groep toe</a>
        <a class="dashboard-toevoeg-button" type="button" href="{{route('upload.index')}}">Upload een groep vanaf een
            bestand.</a>
    @endif
@endsection

{{--@section('schoolgroups_table')--}}
{{--    @if($allClasses != null)--}}
{{--        <table class="ui celled table">--}}
{{--            <thead>--}}
{{--            <tr>--}}
{{--                <th>Groep Naam</th>--}}
{{--                <th>Leerjaar</th>--}}
{{--                <th>Groeps Docent</th>--}}
{{--                <th>Aantal leerlingen</th>--}}
{{--                <th>Bewerk</th>--}}
{{--            </tr>--}}
{{--            </thead>--}}
{{--            <tbody>--}}
{{--            @foreach($allClasses as $oneClass)--}}
{{--                <tr>--}}
{{--                    <td data-label="ClassName">{{$oneClass->name}}</td>--}}
{{--                    <td data-label="SchoolYear">Groep {{$oneClass->grade}}</td>--}}
{{--                    <td data-label="Groeps Docent">{{$oneClass->email}}</td>--}}
{{--                    <td data-label="ClassSize">{{count($oneClass->children)}}</td>--}}
{{--                    <td data-label="Bewerk">--}}
{{--                        <a href="{{ route('schoolclass.edit', $oneClass->id) }}" class="Edit-button">Bewerk</a>--}}
{{--                    </td>--}}
{{--                </tr>--}}
{{--            @endforeach--}}
{{--            </tbody>--}}
{{--        </table>--}}
{{--        <a class="dashboard-toevoeg-button" type="button" href="{{ route('schoolclass.create', $school->id)}}">Voeg een--}}
{{--            groep toe</a>--}}
{{--        <a class="dashboard-toevoeg-button" type="button" href="{{route('upload.index')}}">Upload een groep vanaf een--}}
{{--            bestand.</a>--}}
{{--    @endif--}}
{{--@endsection--}}

@section('gymwijzer_admin_portal')
    <a class="dashboard-toevoeg-button" type="button" href="{{ route('gymwijzer.admin')}}">Bewerken van
        bewegingsactiviteiten</a>
@endsection


@section('list_of_Classes_for_teachers')
    @if($theacherHasClasses != null)
        <table class="ui celled table">
            <thead>
            <tr>
                <th>Groep Naam</th>
                <th>Leerjaar</th>
                <th>Groeps Docent</th>
                <th>Aantal leerlingen</th>
                <th>Bekijken</th>
            </tr>
            </thead>
            <tbody>
            @foreach($theacherHasClasses as $theacherHasOneClass)
                <tr>
                    <td data-label="ClassName">{{$theacherHasOneClass->name}}</td>
                    <td data-label="SchoolYear">Groep {{$theacherHasOneClass->grade}}</td>
                    <td data-label="Klasse Docent">{{$theacherHasOneClass->email}}</td>
                    <td data-label="ClassSize">{{count($theacherHasOneClass->children)}}</td>
                    <td data-label="Bekijken">
                        <a href="{{ route('teacher.schoolclass.viewgroup', $theacherHasOneClass->id) }}"
                           class="Edit-button">Groep Bekijken</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
@endsection

@section('gymwijzer_admin_portal')
    <a class="dashboard-toevoeg-button" type="button" href="{{ route('gymwijzer.admin')}}">Bewerken van
        bewegingsactiviteiten</a>
@endsection

@section('content')
    @include('gymwijzer.partials.breadcrumbs', ['heading' => 'Dashboard',
        'backurl' => route('start'),
        'crumbs' => [['name' => 'GymWijzer!', 'url' => '/'], ['name' => 'VolgWijzer', 'url' => route('volgwijzer')], ['name' => 'Dashboard', 'url' => route('admin.dashboard')]
        ]
    ])

    <div class="wrapper">

        @can('master_dashboard')
            @yield('schools_table')
            @yield('gymwijzer_admin_portal')
        @endcan

        @can('dean_dashboard')
            @if($school != null)
                <h1 class="ui header">Beheren van school {{ $school->name }}</h1>
                @yield('periods')
                @yield('teachers_table')
                @yield('schoolgroups_table')
            @endif
        @endcan

        @canany('teacher_dashboard')
            @if($school != null)
                @yield('list_of_Classes_for_teachers')
                @yield('school_info')
            @endif
        @endcanany

    </div>
@endsection
