@extends('layouts.htmlStart', [
    'title' => "Leraren toevoegen | GymWijzer!",
    'pageid' => "invite-teacher-page"
    ])

@section('content')

    @include('gymwijzer.partials.breadcrumbs', ['heading' => 'Voeg een leraar toe',
         'backurl' => route('admin.dashboard'),
         'crumbs' => [['name' => 'GymWijzer!', 'url' => ''], ['name' => 'Dashboard', 'url' => route('admin.dashboard')],
            ['name' => 'Voeg een leraar toe', 'url' => url()->current()]
         ]

    ])

    <div class="wrapper">
        @isset($school)
            <div class="ui tall stacked segment" style="margin-top: 0">
                <p>
                    Hiermee kunt u nieuwe leraren-accounts toevoegen voor de school <strong>{{$school->name}}</strong>. <br />
                    De leraar ontvangt een link naar zijn mailadres waarmee hij een account kan opstellen.
                </p>
            </div>
        @else
            <p>Hier komt (ooit) een select voor welke school we de leraren uitnodigen</p>
        @endisset

        <form id="invite-teachers" method="POST" action="{{route('teacher.invite')}}" class="ui form">
            @csrf
            <div class="field @error('email')error @enderror">
                <label>Email</label>
                <input type="email" name="email" value="{{ old('email') }}" placeholder="{{ old('email') }}">
                @error('email')
                    <x-field-error>{{ $message }}</x-field-error>
                @enderror
            </div>

            <div class="field">
                <button class="ui fluid huge positive button" type="submit">Uitnodiging versturen</button>
            </div>

            @if($errors->any())
                <div class="ui error message">
                    <div class="header">Er is een fout opgetreden</div>
                    <ul class="list">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

        </form>

    </div>

@endsection

@section('post-content')
    @include('gymwijzer.partials.validation', ['validation' => $validation])
@endsection
