@extends('layouts.htmlStart', ['title' => $child->firstname . ' ' . $child->lastname .' | Kind bewerken | GymWijzer!'
   ])

@section('viewChild')
@if($child != null)
<form method="POST" action="{{route('admin.child.store')}}">
    @csrf
    <table class="ui very basic collapsing celled table">
        <tbody>
        <tr>
            <td><b>Voornaam:</b></td>
            @can('dean_dashboard')
                <td class="ui transparent input focus">
                    <input type="text" name="firstname" placeholder="Voornaam"
                           value="{{ old('firstname') ?? $child->firstname }}">
                    @error('firstname')
                    <x-fielderror>{{ $message }}</x-fielderror>
                    @enderror
                </td>
            @else
                <td>{{$child->firstname}}</td>
            @endcan
        </tr>
        <tr>
            <td><b>Tussen Voegsels:</b></td>
            @can('dean_dashboard')
                <td class="ui transparent input focus">
                    <input type="text" name="tussenVoegsel" placeholder="Optioneel"
                           value="{{ old('tussenVoegsel') ?? $child->tussenVoegsel}}">
                    @error('tussenVoegsel')
                    <x-fielderror>{{ $message }}</x-fielderror>
                    @enderror
                </td>
            @else
                <td>{{$child->tussenVoegsel}}</td>
            @endcan
        </tr>
        <tr>
            <td><b>Achternaam:</b></td>
            @can('dean_dashboard')
                <td class="ui transparent input focus">
                    <input type="text" name="lastname" placeholder="Achternaam"
                           value="{{ old('lastname') ?? $child->lastname}}">
                    @error('lastname')
                    <x-fielderror>{{ $message }}</x-fielderror>
                    @enderror
                </td>
            @else
                <td>{{$child->lastname}}</td>
            @endcan
        </tr>
        <tr>
            <td><b>Geboorte datum:</b></td>
            @can('dean_dashboard')
                <td>
                    <div class="ui transparent calendar">
                        <div class="ui input left icon">
                            <i class="calendar icon"></i>
                            <input type="text" name="date_of_birth" placeholder="Geboorte Datum"
                                   value="{{ old('date_of_birth') ?? $child->date_of_birth}}">
                            @error('date_of_birth')
                            <x-fielderror>{{ $message }}</x-fielderror>
                            @enderror
                        </div>
                    </div>
                </td>
            @else
                <td>{{$child->date_of_birth}}</td>
            @endcan
        </tr>
        <tr>
            <td><b>groep:</b></td>
            <td>
                @can('dean_dashboard')
                    <div class="ui clearable selection dropdown">
                        <input type="hidden" name="groep" value="{{ old('groep') ?? $child->schoolClass->id ?? ''}}">>
                        <i class="dropdown icon"></i>
                        <div data-value="{{$child->schoolClass->id ?? ''}}" class="default text">Leeg</div>
                        <div class="menu">
                            @foreach($school->schoolclasses as $class)
                                <div data-value="{{$class->id}}" class="item">{{$class->name}}</div>
                            @endforeach
                        </div>
                        @error('groep')
                        <x-fielderror>{{ $message }}</x-fielderror>
                        @enderror
                    </div>
                @else
                    {{$child->schoolClass->name ?? ''}}
                @endcan
            </td>
        </tr>
        @can('dean_dashboard')
            <tr>
                <td>
                    <button class="action-button" type="button" id="show_pass_reset_button">
                        wachtwoord opnieuw instellen
                    </button>
                </td>
                <td>
                    <button type="submit" class="action-button"
                            style="max-width: 280px; width: 80%; margin-top: 10px">
                        Update gegevens
                    </button>
                </td>
            </tr>
        @endcan
        </tbody>
    </table>
    <input type="hidden" name="id" value="{{$child->id}}">
    <input type="hidden" name="haseGroup" value="false">
</form>

<div class="ui modal pass_reset_modal">
    <i class="close icon"></i>
    <div class="header">
        wachtwoord opnieuw instellen
    </div>
    <form method="POST" action="{{route('admin.child.passReset')}}">
        @csrf
        <table class="ui celled table">
            <tbody>
            <tr>
                <td>nieuw wachtwoord</td>
                <td class="ui transparent input focus">
                    <input type="password" name="nieuwWachtwoord" placeholder="nieuw wachtwoord"
                           value="{{ old('nieuwWachtwoord')}}">
                    @error('nieuwWachtwoord')
                    <x-fielderror>{{ $message }}</x-fielderror>
                    @enderror
                </td>
            </tr>
            <tr>
                <td>herhaal wachteoord</td>
                <td class="ui transparent input focus">
                    <input type="password" name="herhaalWachtwoord" placeholder="herhaal wachtwoord"
                           value="{{ old('herhaalWachtwoord')}}">
                    @error('herhaalWachtwoord')
                    <x-fielderror>{{ $message }}</x-fielderror>
                    @enderror
                </td>
            </tr>
            </tbody>
            <tfoot>
            <tr>
                <td>
                    <button class="action-button" type="button" id="hide_pass_reset_modal">
                        sluiten
                    </button>
                </td>
                <td>
                    <button type="submit" class="action-button" style="max-width: 280px; width: 80%; margin-top: 10px">
                        nieuw wachtwoord instellen
                    </button>
                </td>
            </tr>
            </tfoot>
        </table>
        <input type="hidden" name="id" value="{{$child->id}}">
        <input type="hidden" name="preOpenModal" value="true">
        <input type="hidden" name="haseGroup" value="false">
    </form>
</div>

@endif
@endsection

@section('breadcrumbs')
    @include('gymwijzer.partials.breadcrumbs', ['heading' => 'bekijk '. $child->firstname . ' '. $child->lastname,
        'backurl' => route('admin.children.without.class'),
        'crumbs' => [['name' => 'GymWijzer!', 'url' => '/'], ['name' => 'VolgWijzer', 'url' => route('volgwijzer')], ['name' => 'Dashboard', 'url' => route('admin.dashboard')], ['name' => 'Kinderen zonder klassen', 'url' => route('admin.children.without.class')], ['name' => 'bekijk '. $child->firstname, 'url' => route('admin.child.viewchild_Withoutgroup', $child->id)]
        ]
    ])
@endsection

@section('content')
    @yield('breadcrumbs')
    <div class="wrapper">
        @yield('viewChild')
    </div>

@endsection

@section('post-content')
<script>
    $('.ui.dropdown').dropdown();
    $('.ui.calendar').calendar({
        type: 'date',
        formatter: {
            date: function (date, settings) {
                if (!date) return '';
                var day = date.getDate() + '';
                if (day.length < 2) {
                    day = '0' + day;
                }
                var month = (date.getMonth() + 1) + '';
                if (month.length < 2) {
                    month = '0' + month;
                }
                var year = date.getFullYear();
                return year + '/' + month + '/' + day;
            }
        }
    });

    $(function () {
        @if(old('preOpenModal') ?? '')
        {
            $(".pass_reset_modal").modal('show');
        }
        @endif
        $("#show_pass_reset_button").click(function () {
            $(".pass_reset_modal").modal('show');

        });
        $("#hide_pass_reset_modal").click(function () {
            $(".pass_reset_modal").modal('hide');

        });
        $(".pass_reset_modal").modal({
            closable: false
        });
    });

</script>
@endsection
