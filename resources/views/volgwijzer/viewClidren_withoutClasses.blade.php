@extends('layouts.htmlStart', ['title' => 'Kinderen zonder klassen | GymWijzer!'])

@section('viewChild')
    @if($allChildrenWithoutGroup != null)
        <table class="ui celled table">
            <thead>
            <tr>
                <th>Voornaam</th>
                <th>Tussenvoegsel</th>
                <th>Achternaam</th>
                <th>gegevens</th>
                <th>Bekijken</th>
            </tr>
            </thead>
            <tbody>
            @foreach($allChildrenWithoutGroup as $ChildWithoutGroup)
                <tr>
                    <td data-label="Voornaam">{{$ChildWithoutGroup->firstname}}</td>
                    <td data-label="Tussenvoegsel">{{$ChildWithoutGroup->tussenVoegsel}}</td>
                    <td data-label="Achternaam">{{$ChildWithoutGroup->lastname}}</td>
                    <td data-label="gegevens">
                        <a href="{{ route('admin.child.viewchild_Withoutgroup', $ChildWithoutGroup->id) }}"
                           class="Edit-button">Gegevens</a>
                    </td>
                    <td data-label="Bekijk">
                        <a href="#" class="Edit-button">Voortgang Bekijken</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
@endsection
@section('addChild')
    <div class="wrapper">
        <a class="dashboard-toevoeg-button" type="button" id="show_leerling_aanmaken_button">Leerling zonder groep aanmaken</a>
    </div>
    <div class="ui modal leerling_aanmaken">
        <i class="close icon"></i>
        <div class="header">
            Leerling aanmaken
        </div>
        <form method="POST" action="{{route('admin.addChild')}}">
            @csrf
            <table class="ui celled table">
                <tbody>
                <tr>
                    <td><b>Voornaam:</b></td>
                    <td class="ui transparent input focus">
                        <input autocomplete="off" type="text" name="firstname" placeholder="Voornaam"
                               value="{{ old('firstname')}}">
                        @error('firstname')
                        <x-fielderror>{{ $message }}</x-fielderror>
                        @enderror
                    </td>
                </tr>
                <tr>
                    <td><b>Tussen Voegsels:</b></td>
                    <td class="ui transparent input focus">
                        <input autocomplete="off" type="text" name="tussenVoegsel" placeholder="Optioneel"
                               value="{{ old('tussenVoegsel')}}">
                        @error('tussenVoegsel')
                        <x-fielderror>{{ $message }}</x-fielderror>
                        @enderror
                    </td>
                </tr>
                <tr>
                    <td><b>Achternaam:</b></td>
                    <td class="ui transparent input focus">
                        <input autocomplete="off" type="text" name="lastname" placeholder="Achternaam"
                               value="{{ old('lastname')}}">
                        @error('lastname')
                        <x-fielderror>{{ $message }}</x-fielderror>
                        @enderror
                    </td>
                </tr>
                <tr>
                    <td><b>Geboorte datum:</b></td>
                    <td>
                        <div class="ui transparent calendar">
                            <div class="ui input left icon">
                                <i class="calendar icon"></i>
                                <input autocomplete="off" type="text" name="date_of_birth" placeholder="Geboorte Datum"
                                       value="{{ old('date_of_birth')}}" class="form-control" data-toggle="datepicker">
                                @error('date_of_birth')
                                <x-fielderror>{{ $message }}</x-fielderror>
                                @enderror
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td><b>wachtwoord</b></td>
                    <td class="ui transparent input focus">
                        <input type="password" name="nieuwWachtwoord" placeholder="nieuw wachtwoord"
                               value="{{ old('nieuwWachtwoord')}}">
                        @error('nieuwWachtwoord')
                        <x-fielderror>{{ $message }}</x-fielderror>
                        @enderror
                    </td>
                </tr>
                <tr>
                    <td><b>herhaal wachteoord</b></td>
                    <td class="ui transparent input focus">
                        <input type="password" name="herhaalWachtwoord" placeholder="herhaal wachtwoord"
                               value="{{ old('herhaalWachtwoord')}}">
                        @error('herhaalWachtwoord')
                        <x-fielderror>{{ $message }}</x-fielderror>
                        @enderror
                    </td>
                </tr>
                </tbody>
                <tfoot>
                <tr>
                    <td>
                        <button class="action-button" type="button" id="hide_leerling_aanmaken_modal">
                            sluiten
                        </button>
                    </td>
                    <td>
                        <button type="submit" class="action-button"
                                style="max-width: 280px; width: 80%; margin-top: 10px">
                            kind aanmaken
                        </button>
                    </td>
                </tr>
                </tfoot>
            </table>
            <input type="hidden" name="preOpenModal" value="true">
            <input type="hidden" name="GroupID" value="{{null}}">
        </form>
    </div>
@endsection



@section('breadcrumbs')
    @include('gymwijzer.partials.breadcrumbs', ['heading' => 'bekijk leerlingen zonder klassen',
        'backurl' => route('admin.dashboard'),
        'crumbs' => [['name' => 'GymWijzer!', 'url' => '/'], ['name' => 'VolgWijzer', 'url' => route('volgwijzer')], ['name' => 'Dashboard', 'url' => route('admin.dashboard')], ['name' => 'Kinderen zonder klassen', 'url' => route('admin.children.without.class')]
        ]
    ])
@endsection

@section('content')
    @yield('breadcrumbs')

    <div class="wrapper">

        @yield('viewChild')
        @yield('addChild')

    </div>
@endsection

@section('post-content')
    <script>
        $('.ui.dropdown').dropdown();
        $('.ui.calendar').calendar({
            type: 'date',
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate() + '';
                    if (day.length < 2) {
                        day = '0' + day;
                    }
                    var month = (date.getMonth() + 1) + '';
                    if (month.length < 2) {
                        month = '0' + month;
                    }
                    var year = date.getFullYear();
                    return year + '/' + month + '/' + day;
                }
            },
            popupOptions: {
                observeChanges: false
            },
        });

        $(function () {
                @if(old('preOpenModal') ?? false)
            {
                $(".leerling_aanmaken").modal('show');
            }
            @endif
            $("#show_leerling_aanmaken_button").click(function () {
                $(".leerling_aanmaken").modal('show');

            });
            $("#hide_leerling_aanmaken_modal").click(function () {
                $(".leerling_aanmaken").modal('hide');

            });
            $(".leerling_aanmaken").modal({
                closable: false
            });
        });

    </script>
@endsection
