@extends('layouts.htmlStart', ['title' => $schoolclass->name . " | Groep bewerken | GymWijzer!"])

@section('content')

    @include('gymwijzer.partials.breadcrumbs', ['heading' => $schoolclass->name . ' - groep bewerken',
        'backurl' => route('admin.dashboard'),
        'crumbs' => [['name' => 'GymWijzer!', 'url' => '/'], ['name' => 'VolgWijzer', 'url' => route('volgwijzer')], ['name' => 'Dashboard', 'url' => route('admin.dashboard')],
           ['name' => 'Groep bewerken', 'url' => Request::url()]
        ]
    ])

    <div class="wrapper" style="margin-bottom: 25px;">

        <div class="add-schoolclass-info-box">
            U bent een groep aan het bewerken van de school <span>{{$school->name}}</span>
        </div>

        <form method="POST" action="{{ route('schoolclass.edit', $schoolclass->id) }}" class="ui large form">

            @csrf

            <div class="field @error('classname')error @enderror">
                <label>Groep naam</label>
                <input type="text" name="classname" placeholder="Groep naam" value="{{ old('classname') ?? $schoolclass->name }}">
            </div>

            <div class="field @error('grade')error @enderror">
                <label>Groep jaar</label>
                <div class="ui clearable selection dropdown">
                    <input type="hidden" name="grade" value="{{ old('grade') ?? $schoolclass->grade }}">
                    <i class="dropdown icon"></i>
                    <div class="default text">Kies een leerjaar</div>
                    <div class="menu">
                        <div data-value="3" class="item">Groep 3</div>
                        <div data-value="4" class="item">Groep 4</div>
                        <div data-value="5" class="item">Groep 5</div>
                        <div data-value="6" class="item">Groep 6</div>
                        <div data-value="7" class="item">Groep 7</div>
                        <div data-value="8" class="item">Groep 8</div>
                    </div>
                </div>
            </div>


            <div class="field @error('email')error @enderror">
                <label>Contact email (Email van de groeps docent)</label>
                <input type="email" name="email" value="{{ old('email') ?? $schoolclass->email }}" placeholder="{{ old('email') }}">
            </div>

            <div class="field">
                <button class="ui fluid huge positive button" type="submit">Groep bewerken</button>
            </div>

        </form>

        @if($errors->has('classname') or $errors->has('grade') or $errors->has('email'))
            <div class="ui error message">
                <div class="header">Er is een fout opgetreden</div>
                <ul class="list">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="ui divider"></div>

        <div class="ui piled segment" style="margin: 15px 0;">
            <h4 class="ui header">Gymdocenten van deze groep beheren</h4>

            <form method="POST" action="{{ route('schoolclass.editTeacher', $schoolclass->id) }}">
                    @csrf
                    <div class="field">
                        <div id="special" class="ui fluid multiple search special selection dropdown">

                            <input type="hidden" name="teachers" value="@foreach($assignedGymTeachers as $gymTeacher){{$gymTeacher->id}}@if(!$loop->last),@endif{{""}}@endforeach">

                            <i class="dropdown icon"></i>

                            @foreach($assignedGymTeachers as $gymTeacher)
                                    <a class="ui label transition visible" data-value="{{$gymTeacher->id}}"
                                       style="display: inline-block !important;">
                                        @if(!$gymTeacher->tussenvoegsel)
                                            {{$gymTeacher->firstname}} {{$gymTeacher->tussenvoegsel}} {{$gymTeacher->lastname}}
                                        @else
                                            {{$gymTeacher->firstname}} {{$gymTeacher->lastname}}
                                        @endif
                                        <i class="delete icon"></i>
                                    </a>
                             @endforeach


                            <div class="default text">Selecteer docenten</div>
                                <div class="menu">
                                     @forelse($schoolGymTeachers as $gymTeacher)
                                         <div class="item" data-value="{{$gymTeacher->id}}">
                                             @if(!$gymTeacher->tussenvoegsel)
                                                {{$gymTeacher->firstname}} {{$gymTeacher->tussenvoegsel}} {{$gymTeacher->lastname}}
                                             @else
                                                 {{$gymTeacher->firstname}} {{$gymTeacher->lastname}}
                                             @endif
                                         </div>

                                    @empty
                                        Momenteel heeft deze school nog geen gymleraren
                                    @endforelse
                                </div>
                        </div>
                    </div>

                <div class="field">
                    <button class="ui fluid large blue button" style="margin-top: 15px;" type="submit">Bewerk leraren</button>
                </div>

            </form>

            @if($errors->has('teachers'))
                <div class="ui error message">
                    <div class="header">Er is een fout opgetreden</div>
                    <ul class="list">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

        </div>

    </div>

@endsection

@section('post-content')
    <script>
        $('.ui.dropdown')
            .dropdown()
        ;

        // $('#special')
        //     .dropdown()
        // ;
    </script>
@endsection
