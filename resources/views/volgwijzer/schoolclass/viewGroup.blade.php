@extends('layouts.htmlStart', ['title' => $schoolclass->name . " | Groep bekijken | GymWijzer!"])

@section('groupChildren_table')
    @if($allChildrenFromClass != null)
        <table class="ui celled table">
            <thead>
            <tr><th>Voornaam</th>
                <th>Tussenvoegsel</th>
                <th>Achternaam</th>
                <th>gegevens</th>
                <th>Bekijken</th>
            </tr></thead>
            <tbody>
            @foreach($allChildrenFromClass as $ChildFromClass)
                <tr>
                    <td data-label="Voornaam">{{$ChildFromClass->firstname}}</td>
                    <td data-label="Tussenvoegsel">{{$ChildFromClass->tussenVoegsel}}</td>
                    <td data-label="Achternaam">{{$ChildFromClass->lastname}}</td>
                    @can('dean_dashboard')
                        <td data-label="gegevens">
                            <a href="{{ route('admin.child.viewchild', $ChildFromClass->id) }}" class="Edit-button">Gegevens</a>
                        </td>
                        <td data-label="Bekijk">
                            <a href="#" class="Edit-button">Voortgang Bekijken</a>
                        </td>
                    @else
                        <td data-label="gegevens">
                            <a href="{{ route('teacher.child.viewchild', $ChildFromClass->id) }}" class="Edit-button">Gegevens</a>
                        </td>
                        <td data-label="Bekijk">
                            <a href="#" class="Edit-button">Voortgang Bekijken</a>
                        </td>
                    @endcan
                </tr>
            @endforeach
            </tbody>
        </table>
        @can('dean_dashboard')
            <div class="wrapper">
                <a class="dashboard-toevoeg-button" type="button" id="show_leerling_aanmaken_button">Leerling aanmaken en aan deze groep koppelen</a>
                <a class="dashboard-toevoeg-button" type="button" id="show_leerling_toevoeg_button">bewerk welke kinderen gekoppeld zijn</a>
                <a class="dashboard-toevoeg-button" type="button" href='{{--{{ route('#',$schoolclass->id)}}--}}'>zet het wachtwoord voor de hele klas {geeft form om wachtwoord te wijzigen}</a>
                                                                                                                    {{--paar opties bv: gelijk aan geboorte datum of ID--}}
                                                                                                                    {{--invulbocks of invulbock gecombieneerd met opties--}}
            </div>
        @endcan
    @endif
    <div class="ui modal leerling_aanmaken">
        <i class="close icon"></i>
        <div class="header">
            Leerling aanmaken
        </div>
        <form method="POST" action="{{route('admin.addChild')}}">
            @csrf
            <table class="ui celled table">
                <tbody>
                <tr>
                    <td><b>Voornaam:</b></td>
                    <td class="ui transparent input focus">
                        <input autocomplete="off" type="text" name="firstname" placeholder="Voornaam"
                               value="{{ old('firstname')}}">
                        @error('firstname')
                        <x-fielderror>{{ $message }}</x-fielderror>
                        @enderror
                    </td>
                </tr>
                <tr>
                    <td><b>Tussen Voegsels:</b></td>
                    <td class="ui transparent input focus">
                        <input autocomplete="off" type="text" name="tussenVoegsel" placeholder="Optioneel"
                               value="{{ old('tussenVoegsel')}}">
                        @error('tussenVoegsel')
                        <x-fielderror>{{ $message }}</x-fielderror>
                        @enderror
                    </td>
                </tr>
                <tr>
                    <td><b>Achternaam:</b></td>
                    <td class="ui transparent input focus">
                        <input autocomplete="off" type="text" name="lastname" placeholder="Achternaam"
                               value="{{ old('lastname')}}">
                        @error('lastname')
                        <x-fielderror>{{ $message }}</x-fielderror>
                        @enderror
                    </td>
                </tr>
                <tr>
                    <td><b>Geboorte datum:</b></td>
                    <td>
                        <div class="ui transparent calendar">
                            <div class="ui input left icon">
                                <i class="calendar icon"></i>
                                <input autocomplete="off" type="text" name="date_of_birth" placeholder="Geboorte Datum"
                                       value="{{ old('date_of_birth')}}" class="form-control" data-toggle="datepicker">
                                @error('date_of_birth')
                                <x-fielderror>{{ $message }}</x-fielderror>
                                @enderror
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td><b>wachtwoord</b></td>
                    <td class="ui transparent input focus">
                        <input type="password" name="nieuwWachtwoord" placeholder="nieuw wachtwoord"
                               value="{{ old('nieuwWachtwoord')}}">
                        @error('nieuwWachtwoord')
                        <x-fielderror>{{ $message }}</x-fielderror>
                        @enderror
                    </td>
                </tr>
                <tr>
                    <td><b>herhaal wachteoord</b></td>
                    <td class="ui transparent input focus">
                        <input type="password" name="herhaalWachtwoord" placeholder="herhaal wachtwoord"
                               value="{{ old('herhaalWachtwoord')}}">
                        @error('herhaalWachtwoord')
                        <x-fielderror>{{ $message }}</x-fielderror>
                        @enderror
                    </td>
                </tr>
                </tbody>
                <tfoot>
                <tr>
                    <td>
                        <button class="action-button" type="button" id="hide_leerling_aanmaken_modal">
                            sluiten
                        </button>
                    </td>
                    <td>
                        <button type="submit" class="action-button" style="max-width: 280px; width: 80%; margin-top: 10px">
                            kind aanmaken
                        </button>
                    </td>
                </tr>
                </tfoot>
            </table>
            <input type="hidden" name="preOpenAanmaakModal" value="true">
            <input type="hidden" name="GroupID" value="{{$schoolclass->id}}">
            @csrf
        </form>
    </div>

    <div class="ui modal leerlingen_toevoegen">
        <i class="close icon"></i>
        <div class="header">
            Leerling toevoegen / verwijderen van groep
        </div>
        <form method="POST" action="{{route('admin.child.edditGroup')}}">
            @csrf
                <div id="special" class="ui fluid multiple search special selection dropdown">

                    <input type="hidden" name="children" value="@foreach($allChildrenFromClass as $ChildrenFromClass){{$ChildrenFromClass->id}}@if(!$loop->last),@endif{{""}}@endforeach">

                    <i class="dropdown icon"></i>

                    @foreach($allChildrenFromClass as $ChildrenFromClass)
                        <a class="ui label transition visible" data-value="{{$ChildrenFromClass->id}}"
                           style="display: inline-block !important;">
                            @if(!$ChildrenFromClass->tussenvoegsel)
                                {{$ChildrenFromClass->firstname}} {{$ChildrenFromClass->tussenvoegsel}} {{$ChildrenFromClass->lastname}}
                            @else
                                {{$ChildrenFromClass->firstname}} {{$ChildrenFromClass->lastname}}
                            @endif
                            <i class="delete icon"></i>
                        </a>
                    @endforeach

                    <div class="default text">Selecteer kinderen</div>
                    <div class="menu">
                        @forelse($allChildrenWithoutAndFromClass as $childWithoutClass)
                            <div class="item" data-value="{{$childWithoutClass->id}}">
                                @if(!$childWithoutClass->tussenvoegsel)
                                    {{$childWithoutClass->firstname}} {{$childWithoutClass->tussenvoegsel}} {{$childWithoutClass->lastname}}
                                @else
                                    {{$childWithoutClass->firstname}} {{$childWithoutClass->lastname}}
                                @endif
                            </div>

                        @empty
                            Momenteel heeft deze school nog geen gymleraren
                        @endforelse
                    </div>
                </div>

            <table class="ui celled table">
                <tbody>
                    <tr>
                        <td>
                            <button class="action-button" type="button" id="hide_leerling_toevoegen_modal">
                                sluiten
                            </button>
                        </td>
                        <td>
                            <button type="submit" class="action-button" style="max-width: 280px; width: 80%; margin-top: 10px">
                                Kinderen toevoegen
                            </button>
                        </td>
                    </tr>
                </tbody>
            </table>

            <input type="hidden" name="preOpenToevoegModal" value="true">
            <input type="hidden" name="GroupID" value="{{$schoolclass->id}}">
        </form>
    </div>
@endsection

@section('breadcrumbs')
    @can('dean_dashboard')
        @include('gymwijzer.partials.breadcrumbs', ['heading' => 'bekijk '. $schoolclass->name,
            'backurl' => route('admin.dashboard'),
            'crumbs' => [['name' => 'GymWijzer!', 'url' => '/'], ['name' => 'VolgWijzer', 'url' => route('volgwijzer')], ['name' => 'Dashboard', 'url' => route('admin.dashboard')], ['name' => 'bekijk '. $schoolclass->name, 'url' => route('admin.schoolclass.viewgroup', $schoolclass->id)]
            ]
        ])
    @else
        @include('gymwijzer.partials.breadcrumbs', ['heading' => 'bekijk '. $schoolclass->name,
            'backurl' => route('admin.dashboard'),
            'crumbs' => [['name' => 'GymWijzer!', 'url' => '/'], ['name' => 'VolgWijzer', 'url' => route('volgwijzer')], ['name' => 'Dashboard', 'url' => route('admin.dashboard')], ['name' => 'bekijk '. $schoolclass->name, 'url' => route('teacher.schoolclass.viewgroup', $schoolclass->id)]
            ]
        ])
    @endcan
@endsection

@section('content')
    @yield('breadcrumbs')

    <div class="wrapper">

        @yield('groupChildren_table')

    </div>
@endsection

@section('post-content')
    <script>
        $('.ui.dropdown').dropdown();
        $('.ui.calendar').calendar({
            type: 'date',
            formatter: {
                date: function (date, settings) {
                    if (!date) return '';
                    var day = date.getDate() + '';
                    if (day.length < 2) {
                        day = '0' + day;
                    }
                    var month = (date.getMonth() + 1) + '';
                    if (month.length < 2) {
                        month = '0' + month;
                    }
                    var year = date.getFullYear();
                    return year + '/' + month + '/' + day;
                }
            },
            popupOptions: {
                observeChanges: false
            },
        });

        $(function () {
                @if(old('preOpenAanmaakModal') ?? false)
            {
                $(".leerling_aanmaken").modal('show');
            }
            @endif
            $("#show_leerling_aanmaken_button").click(function () {
                $(".leerling_aanmaken").modal('show');

            });
            $("#hide_leerling_aanmaken_modal").click(function () {
                $(".leerling_aanmaken").modal('hide');

            });
            $(".leerling_aanmaken").modal({
                closable: false
            });
        });


        $(function () {
                @if(old('preOpenToevoegModal') ?? false)
            {
                $(".leerlingen_toevoegen").modal('show');
            }
            @endif
            $("#show_leerling_toevoeg_button").click(function () {
                $(".leerlingen_toevoegen").modal('show');

            });
            $("#hide_leerling_toevoegen_modal").click(function () {
                $(".leerlingen_toevoegen").modal('hide');

            });
            $(".leerlingen_toevoegen").modal({
                closable: false
            });
        });

    </script>
@endsection
