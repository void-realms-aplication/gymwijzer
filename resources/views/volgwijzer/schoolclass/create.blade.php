@extends('layouts.htmlStart', ['title' => "Schoolklas toevoegen | GymWijzer!"])

@section('content')

    @include('gymwijzer.partials.breadcrumbs', ['heading' => 'Een groep toevoegen aan de school',
        'backurl' => route('admin.dashboard'),
        'crumbs' => [['name' => 'GymWijzer!', 'url' => ''], ['name' => 'Dashboard', 'url' => route('admin.dashboard')],
           ['name' => 'Groep toevoegen', 'url' => Request::url()]
        ]
    ])

    <div class="wrapper" style="margin-bottom: 25px;">

        <div class="add-schoolclass-info-box">
            U bent een groep aan het toevoegen voor de school <span>{{$school->name}}</span>
        </div>

        <form method="POST" action="{{ route('schoolclass.create', $school->id) }}" class="ui large form">

            @csrf

            <div class="field @error('classname')error @enderror">
                <label>Groep naam</label>
                <input type="text" name="classname" placeholder="Groep naam" value="{{ old('classname') }}">
            </div>

            <div class="field @error('grade')error @enderror">
                <label>Groep jaar</label>
                <div class="ui clearable selection dropdown">
                    <input type="hidden" name="grade" value="{{ old('grade') }}">
                    <i class="dropdown icon"></i>
                    <div class="default text">Kies een leerjaar</div>
                    <div class="menu">
                        <div data-value="3" class="item">Groep 3</div>
                        <div data-value="4" class="item">Groep 4</div>
                        <div data-value="5" class="item">Groep 5</div>
                        <div data-value="6" class="item">Groep 6</div>
                        <div data-value="7" class="item">Groep 7</div>
                        <div data-value="8" class="item">Groep 8</div>
                    </div>
                </div>
            </div>


            <div class="field @error('email')error @enderror">
                <label>Contact email van de groeps docent.</label>
                <input type="email" name="email" value="{{ old('email') }}" placeholder="{{ old('email') }}">
            </div>

            <div class="field">
                <button class="ui fluid huge positive button" type="submit">Groep toevoegen</button>
            </div>

        </form>

        @if($errors->any())
            <div class="ui error message">
                <div class="header">Er is een fout opgetreden</div>
                <ul class="list">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

    </div>

@endsection

@section('post-content')
    <script>
        $('.ui.dropdown')
            .dropdown()
        ;
    </script>
@endsection
