@extends('layouts.htmlStart', ['title' => 'Periodes instellen voor schooljaar ' . $schoolyear . ' - ' . $school->name])

@section('head')
    <link rel="stylesheet" href="/css/period.css">
@endsection

@section('content')
    @include('gymwijzer.partials.breadcrumbs', ['heading' => 'Periodes instellen voor ' . $schoolyear,
    'backurl' => route('admin.dashboard'),
    'crumbs' => [['name' => 'GymWijzer!', 'url' => route('start')],
     ['name' => 'Dashboard', 'url' => route('admin.dashboard')],
     ['name' => 'Periodes instellen', 'url' => url()->current()]
    ]
])

    <div class="wrapper">
        <h3>Voor het schooljaar {{ $schoolyear }} kunt u de periodeafname als volgt instellen:</h3>


        <br/>

        <div id="period-config-select">
            <div class="ui huge top attached tabular @php
                $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
             echo $f->format(count($configurations));
            @endphp item icon menu">
                @foreach($configurations as $configuration)
                    <a class="item @if($loop->first) active @endif" data-config="{{$configuration->id}}">
                        <i class="checkmark icon"></i>
                        {{ $configuration->periods->count() }} Periodes
                    </a>
                @endforeach
            </div>
            <div id="configurtaions-info" class="ui bottom attached segment">
                @foreach($configurations as $configuration)
                    <div class="content @if($loop->first) active @endif" data-config-info="{{$configuration->id}}">

                        <div class="ui huge divided items">
                            @foreach($configuration->periods as $index => $period)
                               <div class="item"><strong>Periode {{ $index + 1 }}:&nbsp;</strong>

                                   {{ \Carbon\Carbon::parse($period->start)->format('D d M Y')}}
                                   tot
                                   {{ \Carbon\Carbon::parse($period->end)->format('D d M Y')}}
                               </div>
                            @endforeach
                        </div>


                    </div>
                @endforeach
            </div>
        </div>

        <form action="{{ url()->current() }}" id="periods-submit" method="POST" class="ui form">
            @csrf
            <input type="hidden" name="config_id" value="{{ $configurations[0]->id }}">
            <button class="ui large positive fluid button" onclick="event.preventDefault(); selectPeriod()">Ga verder</button>
        </form>
    </div>



@endsection

@section('post-content')
    <script src="/js/period.js"></script>

    <script>
        $('.ui.radio.checkbox')
            .checkbox()
        ;
    </script>

@endsection
