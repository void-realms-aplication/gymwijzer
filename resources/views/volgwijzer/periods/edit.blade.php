@extends('layouts.htmlStart', ['title' => 'Bewegingsactiviteiten instellen voor schooljaar ' . $config->schoolyear() . ' - ' . $school->name,
    'pageid' => 'setupperiods'
])

@section('content')

    @include('gymwijzer.partials.breadcrumbs', ['heading' => 'Bewegingsactiviteiten instellen ' . $config->schoolyear() . ' - ' . $school->name,
    'backurl' => route('admin.dashboard'),
    'crumbs' => [['name' => 'GymWijzer!', 'url' => route('start')], ['name' => 'Dashboard', 'url' => route('admin.dashboard')],
       ['name' => 'Bewegingsactiviteiten instellen', 'url' => Request::url()]
    ]
])

  <div class="wrapper" >
      <form action="{{route('editPeriods', $config->id)}}" method="POST">
        @csrf

          <div class="ui items">

              @foreach($config->periods as $index=>$period)

                  <div class="item period" data-period="{{$period->id}}">
                      <div class="period-number" data-period="{{$period->id}}">{{$index + 1}}</div>
                      <div class="content">
                          <span class="header">
                              <i class="blue calendar icon"></i>
                              <span class="font-size: 21px;">{{ \Carbon\Carbon::parse($period->start)->translatedFormat('D d M Y')}}</span>
                              tot
                              <i class="blue calendar icon"></i>
                              <span class="font-size: 21px;">{{ \Carbon\Carbon::parse($period->end)->translatedFormat('D d M Y')}}</span>
                          </span>
                          <div class="extra">
                              @if($period->activiteiten->count() == 0)
                                  <i class="red warning icon"></i>
                                  Voor deze periode zijn nog geen activiteiten ingesteld
                              @elseif($period->activiteiten->count() > 0 && $period->activiteiten->count() < $config->recommended_min_activities)
                                  <i class="yellow exclamation icon"></i>
                                  Voor deze periode {{ $period->activiteiten->count() == 1 ? 'is' : 'zijn' }}
                                  er maar {{ $period->activiteiten->count() }}
                                  {{ $period->activiteiten->count() == 1 ? 'activiteit' : 'activiteiten' }} ingesteld
                              @elseif($period->activiteiten->count() > $config->recommended_max_activities)
                                  <i class="yellow exclamation icon"></i>
                                Er zijn voor deze periode meer activiteiten ingesteld dan er wordt aangeraden, namelijk <strong>{{ $period->activiteiten->count() }}</strong>
                              @else
                                  <i class="green checkmark icon"></i>
                                  Voor deze periode {{ $period->activiteiten->count() == 1 ? 'is' : 'zijn' }} er {{ $period->activiteiten->count() }} {{ $period->activiteiten->count() == 1 ? 'activiteit' : 'activiteiten' }} ingesteld
                              @endif
                          </div>
                      </div>

                      <div class="select-activities">

                          <div class="ui fluid multiple search special selection dropdown">
                              <input type="hidden" id="activities-{{$period->id}}" name="activities-{{$period->id}}" value="@foreach($period->activiteiten as $activiteit){{$activiteit->id}}@if(!$loop->last),@endif{{""}}@endforeach">

                              <i class="dropdown icon"></i>
                              @foreach($period->activiteiten as $activiteit)
                                  <a class="ui label transition visible
                            @if($activiteit->countPresentInPeriodConfiguration($config) > 1) doubled @endif"
                                     data-period="{{$period->id}}" data-value="{{$activiteit->id}}"
                                     data-name="{{$activiteit->name}}"
                                     style="display: inline-block !important;">
                                      {{$activiteit->name}}
                                      <i class="delete icon"></i>
                                  </a>
                              @endforeach

                              <div class="default text">Selecteer bewegingsactiviteiten</div>

                              <div class="menu">
                                  @forelse($bewegingsactiviteiten as $activiteit)

                                      <div class="item
                                         @if($activiteit->isPresentInPeriodConfiguration($config) == true)
                                          alreadyselected @endif"
                                           data-period="{{$period->id}}" data-value="{{$activiteit->id}}"
                                            data-name="{{$activiteit->name}}">
                                          {{$activiteit->name}}
                                          @if($activiteit->isPresentInPeriodConfiguration($config) == true)
                                            <div class="ui small horizontal red label">Al ingedeeld!</div>
                                          @endif
                                      </div>

                                  @empty
                                      Momenteel heeft deze periode nog geen bewegingsactiviteiten
                                  @endforelse
                              </div>

                          </div>

                      </div>

                  </div>

              @if(!$loop->last)
                  <hr>
                @endif
              @endforeach

          </div>

          <div class="field">
              <button class="ui fluid large blue button" type="submit">Opslaan</button>
          </div>

      </form>

  </div>


@endsection

@section('post-content')
    <script src="/js/period-activity.js"></script>
    <script> var usedActivities = [
        @if($config->activiteiten() != null)
                @foreach($config->activiteiten() as $activity)
                    {{$activity->id}} @if(!$loop->last),@endif
                @endforeach
            @endif
        ];

        var recommended_max_activities = {{ $config->recommended_max_activities }};</script>
    <script>$('.ui.dropdown')
            .dropdown()
        ;</script>
@endsection
