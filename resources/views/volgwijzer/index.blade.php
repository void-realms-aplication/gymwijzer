@extends('layouts.htmlStart', ['title' => 'Demo - GymWijzer!', 'pageid' => 'gymwijzer-demo'])

@section('head')
    <link rel="stylesheet" href="/css/gymwijzerpages.css">
@endsection

@section('content')
    @include('gymwijzer.partials.breadcrumbs', ['heading' => 'VolgWijzer ',
        'backurl' => route('start'),
        'crumbs' => [['name' => 'GymWijzer!', 'url' => route('start')], ['name' => 'VolgWijzer', 'url' => url()->current()]]
    ])

    <div class="wrapper">
        <div id="gymwijzer-grid-wrap">

            <a class="gymwijzer-button">
                groepen bewerken
            </a>
            <a class="gymwijzer-button">
                resultaten bewegingsactivitijd invoeren.
            </a>
            <a href="{{route('mijnAccount')}}" class="gymwijzer-button">
                gegevends over mijn school
            </a><a href="{{route('admin.dashboard')}}" class="gymwijzer-button">
                Beheer gropen en kinderen
            </a><a class="gymwijzer-button">
                blokje 5
            </a>


        </div>
    </div>

@endsection
