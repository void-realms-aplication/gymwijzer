@extends('layouts.htmlStart',['title' => 'Gegevends van mijn school - GymWijzer', 'pageid' => 'gymwijzer-demo'])

@section('head')
    <link rel="stylesheet" href="/css/gymwijzerpages.css">
@endsection

@section('content')
    @include('gymwijzer.partials.breadcrumbs', ['heading' => 'Wijzig mijn gegevens',
        'backurl' => route('mijnAccount'),
        'crumbs' => [['name' => 'GymWijzer!', 'url' => route('start')], ['name' => 'Mijn account', 'url' => route('mijnAccount')], ['name' => 'Wijzig mijn gegevens' , 'url' => url()->current()]]
    ])

    <div class="wrapper">

        <section class="my-school-section">
            <h1>Wijzig gegevens</h1>
            <form method="POST" action="{{route('mijnAccount.store')}}"
                  class="ui equal width form @if($errors->any())error @endif" style="font-weight: bold;">
                @csrf
                <div class="tree fields">
                    <div class="field">
                        <label>Voornaam</label>
                        <div class=" @error('firstname') error @enderror">
                            <input type="text" name="firstname" placeholder="Voornaam"
                                   value="{{ $firstName }}">
                            @error('firstname')
                            <x-field-error>{{ $message }}</x-field-error>
                            @enderror
                        </div>
                    </div>
                    <div class="field">
                        <label>Tussenvoegsel</label>
                        <div class=" @error('tussenvoegsel')error @enderror">
                            <input type="text" name="tussenvoegsel" placeholder="Tussenvoegsel"
                                   value="{{ $middleName }}">
                            @error('tussenvoegsel')
                            <x-field-error>{{ $message }}</x-field-error>
                            @enderror
                        </div>
                    </div>
                    <div class="field">
                        <label>Achternaam</label>
                        <div class=" @error('lastname') error @enderror">
                            <input type="text" name="lastname" placeholder="Achternaam"
                                   value="{{ $lastName }}">
                            @error('lastname')
                            <x-field-error>{{ $message }}</x-field-error>
                            @enderror
                        </div>
                    </div>
                </div>
                <button type="submit" class="action-button" style="max-width: 280px; width: 80%; margin-top: 10px">
                    Wijzig uw gegevens
                </button>
            </form>
        </section>
        <section class="my-school-section">
            <h1>Wijzig wachtwoord</h1>
                        <form method="POST" action="{{route('password.post')}}"
                  class="ui equal width form @if($errors->any())error @endif" style="font-weight: bold;">
                @csrf
                <div class="tree fields">
                    <div class="field">
                        <label>Uw oude wachtwoord</label>
                        <div class=" @error('oldPass') error @enderror">
                            <input type="password" name="oldPass" placeholder="Uw oude wachtwoord">
                            @error('oldPass')
                            <x-field-error>{{ $message }}</x-field-error>
                            @enderror
                        </div>
                    </div>
                    <div class="field">
                        <label>Uw nieuwe wachtwoord</label>
                        <div class=" @error('newPass')error @enderror">
                            <input type="password" name="newPass" placeholder="Uw nieuwe wachtwoord">
                            @error('newPass')
                            <x-field-error>{{ $message }}</x-field-error>
                            @enderror
                        </div>
                    </div>
                    <div class="field">
                        <label>Uw nieuwe wachtwoord nogmaals</label>
                        <div class=" @error('newPassRepeat') error @enderror">
                            <input type="password" name="newPassRepeat" placeholder="Uw nieuwe wachtwoord nogmaals">

                            @error('newPassRepeat')
                            <x-field-error>{{ $message }}</x-field-error>
                            @enderror
                        </div>
                    </div>
                </div>
                <button type="submit" class="action-button" style="max-width: 280px; width: 80%; margin-top: 10px">
                    Wijzig uw gegevens
                </button>
            </form>
        </section>


    </div>

@endsection
