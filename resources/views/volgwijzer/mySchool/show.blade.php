@extends('layouts.htmlStart',['title' => 'Mijn account - GymWijzer', 'pageid' => 'gymwijzer-demo'])

@section('head')
    <link rel="stylesheet" href="/css/gymwijzerpages.css">
@endsection

@section('content')
    @include('gymwijzer.partials.breadcrumbs', ['heading' => 'Mijn account',
        'backurl' => route('start'),
        'crumbs' => [['name' => 'GymWijzer!', 'url' => route('start')], ['name' => 'Mijn account', 'url' => url()->current()]]
    ])

    <div class="wrapper">


        <section class="my-school-section">
            <h1>mijn gegevens</h1>
            <form class="ui equal width form" style="font-weight: bold;">
                <div class="four fields">
                    <div class="field">
                        <label>Voornaam</label>
                        <div>
                            <input type="text" name="firstname"
                                   value="{{ $firstName }}" disabled>
                        </div>
                    </div>
                    <div class="field">
                        <label>Tussenvoegsel</label>
                        <div>
                            <input type="text" name="tussenvoegsel"
                                   value="{{ $middleName }}" disabled>
                        </div>
                    </div>
                    <div class="field">
                        <label>Achternaam</label>
                        <div>
                            <input type="text" name="lastname"
                                   value="{{ $lastName }}" disabled>
                        </div>
                    </div>
                </div>
            </form>

            <a href="{{Route('mijnAccount.edit')}}" class="action-button"
               style="max-width: 280px; width: 80%; margin-top:10px;">Wijzig mijn gegevens</a>
        </section>
        @if($school)
            <section class="my-school-section">
                <h1>gegevens van mijn school</h1>

                <h3><b>Contact</b></h3>
                <form class="ui equal width form" style="font-weight: bold;">
                    <div class="four fields">
                        <div class="field">
                            <label>Naam van de school</label>
                            <input type="text" name="school-name" value="{{$school->name}}" disabled>
                        </div>
                        <div class="field">
                            <label>Telefoonnummer</label>
                            <input type="text" name="phone-number" value="{{$school->phone_number}}" disabled>
                        </div>
                        <div class="field">
                            <label>E-mail contact persoon GymWijzer!</label>
                            <input type="text" name="admin-email" value="{{$school->getAdmin->email}}" disabled>
                        </div>
                        <div class="field">
                            <label>Naam contact persoon GymWijzer!</label>
                            <input type="text" name="admin-name" value="{{$nameAdmin}}" disabled>
                        </div>
                    </div>
                </form>
                <h3><b>Adres</b></h3>
                <form class="ui equal width form" style="font-weight: bold;">
                    <div class="four fields">
                        <div class="field">
                            <label>Straat</label>
                            <input type="text" name="street" value="{{$school->street}}" disabled>
                        </div>
                        <div class="field">
                            <label>Huisnummer</label>
                            <input type="text" name="phop" value="{{$adresNummer}}" disabled>
                        </div>
                        <div class="field">
                            <label>Stad</label>
                            <input type="text" name="city" value="{{$school->city}}" disabled>
                        </div>
                        <div class="field">
                            <label>Postcode</label>
                            <input type="text" name="postal_code" value="{{$school->postal_code}}" disabled>
                        </div>
                        <div class="field">
                            <label>Land</label>
                            <div class="ui labeled input">
                                @if($school->country == 'Nederland')
                                    <div class="ui label">
                                        <i class="nl flag"></i>
                                    </div>
                                    <input name="country" type="text" value="{{'Nederland'}}" disabled>
                                @else
                                    <div class="ui label">
                                        <i class="be flag"></i>
                                    </div>
                                    <input name="country" type="text" value="{{'België'}}" disabled>
                                @endif
                            </div>
                        </div>
                    </div>
                </form>
                <h3><b>Algemeen</b></h3>
                <form class="ui equal width form" style="font-weight: bold;">
                    <div class="tree fields">

                        <div class="field">
                            <label>Brin nummer of instellingsnummer</label>
                            <input type="text" name="brin_nummer" value="{{$school->brin_number}}" disabled>
                        </div>
                        <div class="field">
                            <label>Maximaal toegestane aantal kinderen</label>
                            <input type="text" name="max-students" value="{{$school->number_of_students}}" disabled>
                        </div>
                        <div class="field">
                            <label>Het aantal ingevoerde kinderen</label>
                            <input type="text" name="amount-students" value="{{$school->amountOfChildren()}}" disabled>
                        </div>
                    </div>
                </form>

                <a href="{{Route('school.edit')}}" class="action-button"
                   style="max-width: 280px; width: 80%; margin-top:10px;">Wijzig gegevens van mijn school</a>
            </section>
            <section class="my-school-section" style="width: 100%;">
                <h1>logo</h1>
                <div class="my-school-logo">
                    <div class="my-school-logo-item">
                        <p>Dit is het logo dat nu voor uw school ingesteld staat.</p>
                        @if($admin)
                            <a href="{{Route('logo')}}" class="action-button" style="max-width: 280px; width: 80%;">Verander
                                logo</a>
                        @else
                            Het logo kan allen worden veranderd door de contactpersoon van uw organizatie.
                        @endif
                    </div>
                    <div class="my-school-logo-item">
                        <img src="{{$logo}}" alt="logo kan niet worden weer gegeven" class="logo-img"
                             style="max-width: 330px; height: auto;">
                    </div>
                </div>
            </section>
            <section class="my-school-section">
                @if($admin)
                    <h1>Verlengen</h1>
                    <p>U heeft al een GymWijzer abonement sinds {{$validSinds}} en is nog geldig tot: {{$validTill}}</p>
                    <p>U kunt uw abonement op de GymWijzer via deze pagina verlengen, zodra wij de betaling hebben
                        ontvangen kun
                        u uw abonement weer een jaar gebruiken. Als u uw abonement verlengt voor de eind datum wordt en
                        een
                        extra jaar bij opgeteld, als uw abonement al is verlopen is het nieuwe abonement een jaar vanaf
                        vandaag
                        geldig.</p>
                    <a href="{{Route('mollie.renew')}}" class="action-button" style="max-width: 400px; width: 90%;">Verleng
                        uw abonement</a>
                @else
                    <h1>Geldig tot</h1>
                    <p>Uw organizatie heeft al een GymWijzer abonement sinds {{$validSinds}} en is nog geldig
                        tot: {{$validTill}}</p>
                    <p>Het abonement kan alleen verlengd worden door het contact persoon van uw organizatie.</p>
                @endif
            </section>
        @endif

    </div>

@endsection
