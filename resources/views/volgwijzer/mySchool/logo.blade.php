@extends('layouts.htmlStart',['title' => 'Gegevends van mijn school - GymWijzer', 'pageid' => 'gymwijzer-demo'])

@section('head')
    <link rel="stylesheet" href="/css/gymwijzerpages.css">
@endsection

@section('content')
    @include('gymwijzer.partials.breadcrumbs', ['heading' => 'Logo van mijn school',
        'backurl' => route('volgwijzer'),
        'crumbs' => [['name' => 'GymWijzer!', 'url' => route('start')], ['name' => 'VolgWijzer!', 'url' => route('volgwijzer')], ['name' => 'Logo van mijn school', 'url' => url()->current()]]
    ])


    <div class="wrapper">
        <h1>Verander uw logo</h1>
        <form action="{{ route('logo.store') }}" method="POST" role="form" enctype="multipart/form-data">
            @csrf
            <label for="logo">Logo</label>
            <input type="file" name="logo" required>
            @if ($logo)
                
            @endif

            <button type="submit">Update logo</button>

        </form>

    </div>
@endsection
