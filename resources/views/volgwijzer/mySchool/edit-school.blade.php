@extends('layouts.htmlStart',['title' => 'Wijzig gegevends van mijn school - GymWijzer', 'pageid' => 'gymwijzer-demo'])

@section('head')

@endsection

@section('content')
    @include('gymwijzer.partials.breadcrumbs', ['heading' => 'Wijzig gegevends van mijn school',
        'backurl' => route('mijnAccount'),
        'crumbs' => [['name' => 'GymWijzer!', 'url' => route('start')], ['name' => 'Mijn account', 'url' => route('mijnAccount')], ['name' => 'Wijzig gegevends van mijn school' , 'url' => url()->current()]]
    ])

    <div class="wrapper">
        @if($school)

            <h1>gegevens van mijn school</h1>

            <h3><b>Contact</b></h3>
            <form method="POST" action="{{route('school.store')}}"
                  class="ui equal width form @if($errors->any())error @endif" style="font-weight: bold;"
                  name="general-info">
                @csrf


                <div class="two fields">

                    <div class="field @error('school_name') error @enderror ">
                        <label>De naam van de school.</label>
                        <div>
                            <input name="school_name" type="text" value="{{ $school->name }}"
                                   placeholder="Naam van de school.">
                            @error('school_name')
                            <x-field-error>{{ $message }}</x-field-error>
                            @enderror
                        </div>
                    </div>
                    <div class="field @error('phone_number') error @enderror ">
                        <label>Telefoonnummer</label>
                        <div>
                            <input name="phone_number" type="text" value="{{$school->phone_number}}"
                                   placeholder="Telefoonnummer">
                            @error('phone_number')
                            <x-field-error>{{ $message }}</x-field-error>
                            @enderror
                        </div>
                    </div>
                </div>
                <h3><b>Adres</b></h3>
                <div class="tree fields">
                    <div class="field @error('street')error @enderror">
                        <label>Straat</label>
                        <div>
                            <input name="street" type="text" value="{{$school->street}}" placeholder="Straat">
                            @error('street')
                            <x-field-error>{{ $message }}</x-field-error>
                            @enderror
                        </div>
                    </div>
                    <div class="field @error('house_number')error @enderror ">
                        <label>Huisnummer</label>
                        <div>
                            <input name="house_number" type="text" value="{{$school->house_number}}">
                            @error('house_number')
                            <x-field-error>{{ $message }}</x-field-error>
                            @enderror
                        </div>
                    </div>
                    <div class="field @error('street')error @enderror">
                        <label for="additive">Toevoeging</label>
                        <div>
                            <input name="additive" type="text" value="{{$school->additive}}">
                            @error('additive')
                            <x-field-error>{{ $message }}</x-field-error>
                            @enderror
                        </div>
                    </div>
                </div>


                <div class="tree fields">
                    <div class="field @error('street')error @enderror ">
                        <label>Stad</label>
                        <div>
                            <input name="city" type="text" value="{{$school->city}}" placeholder="Stad">
                            @error('city')
                            <x-field-error>{{ $message }}</x-field-error>
                            @enderror
                        </div>
                    </div>
                    <div class="field @error('street')error @enderror ">
                        <label for="postal_code">Post code</label>
                        <div>
                            <input name="postal_code" type="text" value="{{$school->postal_code}}"
                                   placeholder="Post code">
                            @error('postal_code')
                            <x-field-error>{{ $message }}</x-field-error>
                            @enderror
                        </div>
                    </div>
                    <div class="field">
                        <label for="country">land</label>
                        <div class="ui labeled input">
                            @if($school->country == 'Nederland')
                                <div class="ui label">
                                    <i class="nl flag"></i>
                                </div>
                                <input name="country" type="text" value="{{'Nederland'}}" disabled>
                            @else
                                <div class="ui label">
                                    <i class="be flag"></i>
                                </div>
                                <input name="country" type="text" value="{{'België'}}" disabled>
                            @endif
                        </div>
                    </div>
                </div>


                <button type="submit" class="action-button" style="max-width: 280px; width: 80%; margin-top: 10px">
                    Update gegevens
                </button>
            </form>

        @endif

    </div>

@endsection
