@extends('emails.layout')

@section('title')
    Kom bij de GymWijzer.
@endsection

@section('body')
    <p>Hallo,</p>
    @if($invitedBy->tussenvoegsel)
        <p>U bent door meneer/mevrouw {{$invitedBy->tussenvoegsel}} {{$invitedBy->lastname}} uitgenodig om een GymWijzer account aan te maken.
    @else
        <p>U bent door meneer/mevrouw {{$invitedBy->lastname}} uitgenodig om een GymWijzer account aan te maken.
    @endif
        <a href="{{ $registerAccountUrl }}">Maak hier een account aan.</a></p>

@endsection
