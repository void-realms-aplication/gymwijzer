@extends('emails.layout')

@section('title')
    {{$title}}
@endsection


@section('body')
    <p>Hallo menner/mevrouw {{$user->lastname}},</p>
    <p>U heeft een nieuw wachtoord aangevraagd voor de GymWijzer.</p>
    <p ><a class="link"
                                    href="{{ $setPasswordUrl }}"
                                    style="font-weight: normal; text-decoration: underline; color: #0b9ce5;">
            Klik hier om uw wachtwoord in te stellen.</a>
    </p>
<p>Heeft u geen niew wachtoord aangevraagd, dan kun u deze mail negeren.</p>
@endsection

