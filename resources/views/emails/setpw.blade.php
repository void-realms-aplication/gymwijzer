@extends('emails.layout')

@section('title')
    {{$title}}
@endsection


@section('body')
    @if($user->tussenvoegsel)
        <p>Hallo menner/mevrouw {{$user->tussenvoegsel}} {{$user->lastname}},</p>
    @else
        <p>Hallo menner/mevrouw {{$user->lastname}},</p>
    @endif
    <p>U kunt nu een wachtoord instllen voor uw GymWijzer account.</p>
    <p ><a class="link"
                                    href="{{ $setPasswordUrl }}"
                                    style="font-weight: normal; text-decoration: underline; color: #0b9ce5;">
            Klik hier om uw wachtwoord in te stellen.</a>
    </p>

@endsection

