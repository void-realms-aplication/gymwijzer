@extends('emails.layout')

@section('title')
    {{$title}}
@endsection

@section('body')
    <p>Hallo {{$name}},</p>
    <p>U heeft zojuist een contact formulier ingevuld. Wij gaan zo snel mogelijk reageren</p>
    <p>Naam: <br>{{$name}}</p>
    <p>Email: <br>{{$email}}</p>
    <p>Onderwerp: <br>{{$subjectMail}}</p>
    <p>Bericht: <br>{{$messageMail}}</p>
@endsection
