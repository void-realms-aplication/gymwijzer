<!DOCTYPE html>
<html lang="nl-NL">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>GymWijzer!</title></head>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="padding: 0;">
<div id="wrapper" dir="ltr"
     style="background-color: #f7f7f7; margin: 0; padding: 70px 0; width: 100%; -webkit-text-size-adjust: none;">
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
        <tr>
            <td align="center" valign="top">
                <div id="template_header_image"></div>
                <table border="0" cellpadding="0" cellspacing="0" width="600" id="template_container"
                       style="background-color: #ffffff; border: 1px solid #dedede; box-shadow: 0 1px 4px rgba(0, 0, 0, 0.1); border-radius: 3px;">
                    <tr>
                        <td align="center" valign="top"> <!-- Header -->
                            <table border="0" cellpadding="0" cellspacing="0" width="600" id="template_header"
                                   style='background-color: #0b9ce5; color: #ffffff; border-bottom: 0; font-weight: bold; line-height: 100%; vertical-align: middle; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; border-radius: 3px 3px 0 0;'>
                                <tr>
                                    <td id="header_wrapper" style="padding: 36px 48px; display: block;"><h1
                                            style='font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 30px; font-weight: 300; line-height: 150%; margin: 0; text-align: left; text-shadow: 0 1px 0 #3cb0ea; color: #ffffff;'>
                                            Bedankt voor je bestelling</h1></td>
                                </tr>
                            </table> <!-- End Header --> </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top"> <!-- Body -->
                            <table border="0" cellpadding="0" cellspacing="0" width="600" id="template_body">
                                <tr>
                                    <td valign="top" id="body_content" style="background-color: #ffffff;">
                                        <!-- Content -->
                                        <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                            <tr>
                                                <td valign="top" style="padding: 48px 48px 32px;">
                                                    <div id="body_content_inner"
                                                         style='color: #636363; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>
                                                        <p style="margin: 0 0 16px;">Hallo Maud,</p>
                                                        <p style="margin: 0 0 16px;">Ter info — we hebben je bestelling
                                                            #8954 ontvangen en die wordt nu verwerkt.</p>
                                                        <h2 style='color: #0b9ce5; display: block; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 18px; font-weight: bold; line-height: 130%; margin: 0 0 18px; text-align: left;'>
                                                            [Bestelling #8954] (05/06/2020)</h2>
                                                        <div style="margin-bottom: 40px;">
                                                            <table class="td" cellspacing="0" cellpadding="6" border="1"
                                                                   style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
                                                                <thead>
                                                                <tr>
                                                                    <th class="td" scope="col"
                                                                        style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">
                                                                        Product
                                                                    </th>
                                                                    <th class="td" scope="col"
                                                                        style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">
                                                                        Aantal
                                                                    </th>
                                                                    <th class="td" scope="col"
                                                                        style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">
                                                                        Prijs
                                                                    </th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr class="order_item">
                                                                    <td class="td"
                                                                        style="color: #636363; border: 1px solid #e5e5e5; padding: 12px; text-align: left; vertical-align: middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; word-wrap: break-word;">
                                                                        Abonnement Student
                                                                    </td>
                                                                    <td class="td"
                                                                        style="color: #636363; border: 1px solid #e5e5e5; padding: 12px; text-align: left; vertical-align: middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
                                                                        1
                                                                    </td>
                                                                    <td class="td"
                                                                        style="color: #636363; border: 1px solid #e5e5e5; padding: 12px; text-align: left; vertical-align: middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
                                                                        <span
                                                                            class="woocommerce-Price-amount amount"><span
                                                                                class="woocommerce-Price-currencySymbol">€</span>15.00</span>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                                <tfoot>
                                                                <tr>
                                                                    <th class="td" scope="row" colspan="2"
                                                                        style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left; border-top-width: 4px;">
                                                                        Subtotaal:
                                                                    </th>
                                                                    <td class="td"
                                                                        style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left; border-top-width: 4px;">
                                                                        <span
                                                                            class="woocommerce-Price-amount amount"><span
                                                                                class="woocommerce-Price-currencySymbol">€</span>15.00</span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="td" scope="row" colspan="2"
                                                                        style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">
                                                                        Betaalmethode:
                                                                    </th>
                                                                    <td class="td"
                                                                        style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">
                                                                        iDEAL
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="td" scope="row" colspan="2"
                                                                        style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">
                                                                        Totaal:
                                                                    </th>
                                                                    <td class="td"
                                                                        style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">
                                                                        <span
                                                                            class="woocommerce-Price-amount amount"><span
                                                                                class="woocommerce-Price-currencySymbol">€</span>15.00</span>
                                                                    </td>
                                                                </tr>
                                                                </tfoot>
                                                            </table>
                                                        </div>
                                                        <h2 style='color: #0b9ce5; display: block; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 18px; font-weight: bold; line-height: 130%; margin: 0 0 18px; text-align: left;'>
                                                            Subscription information</h2>
                                                        <table class="td" cellspacing="0" cellpadding="6" border="1"
                                                               style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; margin-bottom: 40px;">
                                                            <thead>
                                                            <tr>
                                                                <th class="td" scope="col"
                                                                    style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">
                                                                    Abonnement ID
                                                                </th>
                                                                <th class="td" scope="col"
                                                                    style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">
                                                                    Start date
                                                                </th>
                                                                <th class="td" scope="col"
                                                                    style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">
                                                                    End date
                                                                </th>
                                                                <th class="td" scope="col"
                                                                    style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">
                                                                    Price
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td class="td" scope="row"
                                                                    style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">
                                                                    <a href="https://gymwijzer.nl/my-account/view-subscription/8955/"
                                                                       style="color: #0b9ce5; font-weight: normal; text-decoration: underline;">#8955</a>
                                                                </td>
                                                                <td class="td" scope="row"
                                                                    style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">
                                                                    05/06/2020
                                                                </td>
                                                                <td class="td" scope="row"
                                                                    style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">
                                                                    When cancelled
                                                                </td>
                                                                <td class="td" scope="row"
                                                                    style="color: #636363; border: 1px solid #e5e5e5; vertical-align: middle; padding: 12px; text-align: left;">
                                                                    <span class="woocommerce-Price-amount amount"><span
                                                                            class="woocommerce-Price-currencySymbol">€</span>15.00</span>
                                                                    / per jaar
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <br>
                                                        <p style="margin: 0 0 16px;">Thanks for purchasing a membership!
                                                            You can view more details about your membership from <a
                                                                href="https://gymwijzer.nl/my-account/members-area/5393/my-membership-content/"
                                                                style="color: #0b9ce5; font-weight: normal; text-decoration: underline;">your
                                                                account</a>.</p>
                                                        <p style="margin: 0 0 16px;"><strong>Aantal leerlingen op school
                                                                (van toepassing op meerdere abonnementen)):</strong> 656
                                                        </p>
                                                        <p style="margin: 0 0 16px;"><strong>Brin nummer:</strong> 25KB
                                                        </p>
                                                        <table id="addresses" cellspacing="0" cellpadding="0" border="0"
                                                               style="width: 100%; vertical-align: top; margin-bottom: 40px; padding: 0;">
                                                            <tr>
                                                                <td valign="top" width="50%"
                                                                    style="text-align: left; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; border: 0; padding: 0;">
                                                                    <h2 style='color: #0b9ce5; display: block; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 18px; font-weight: bold; line-height: 130%; margin: 0 0 18px; text-align: left;'>
                                                                        Factuuradres</h2>
                                                                    <address class="address"
                                                                             style="padding: 12px; color: #636363; border: 1px solid #e5e5e5;">
                                                                        Maud van Ingen<br>De Kleine Kampen 45<br>3911 PH
                                                                        Rhenen <br>0637308060 <br>m.vaningen4@student.han.nl
                                                                    </address>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <p style="margin: 0 0 16px;">Bedankt dat je gymwijzer.nl
                                                            gebruikt!</p></div>
                                                </td>
                                            </tr>
                                        </table> <!-- End Content --> </td>
                                </tr>
                            </table> <!-- End Body --> </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" valign="top"> <!-- Footer -->
                <table border="0" cellpadding="10" cellspacing="0" width="600" id="template_footer">
                    <tr>
                        <td valign="top" style="padding: 0; border-radius: 6px;">
                            <table border="0" cellpadding="10" cellspacing="0" width="100%">
                                <tr>
                                    <td colspan="2" valign="middle" id="credit"
                                        style='border-radius: 6px; border: 0; color: #8a8a8a; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 12px; line-height: 150%; text-align: center; padding: 24px 0;'></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table> <!-- End Footer --> </td>
        </tr>
    </table>
</div>
</body>
</html>
