<!DOCTYPE html>
<html lang="nl-NL" xmlns:https="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"
      style='background-color: #f7f7f7;  font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;'>
<div id="wrapper" dir="ltr"
     style="background-color: #f7f7f7; padding: 150px 150px; -webkit-text-size-adjust: none; ">

    <div class="container" style="position: relative; width: 750px; ">
        <section class="inner-body"
                 style="align-content: center; border-radius: 10px; border: 1px solid #dedede; box-shadow: 0 1px 4px rgba(0, 0, 0, 0.1);">

            <div id="template_header"
                 style='position: relative; background-color: #33ccff;  color: #ffffff; font-weight: bold; width: 100%; height: 100px; border-radius: 10px 10px 0 0;'>

                <h1 style='font-size: 30px; font-weight: 300; display: inline; position: absolute; top: 10px; left: 20px'>
                    @yield('title')
                </h1>
                <img id="template_header_image" src="https://gymwijzerdev.nl/img/gymwijzer-theme/gymwijzer.png"
                     style=" height: 70px; width: auto; position: absolute; left: 450px; top:15px" alt="GymWijzer!">
            </div>
            <div style="background: white">

                <div id="body_content_inner"
                     style='color: #636363; font-size: 16px; line-height: 150%; text-align: left; border-radius:  6px; padding: 40px;'>
                    @yield('body')
                    <p>Met vriendelijke groet,<br>
                    GymWijzer</p>
                </div>

            </div>
        </section>
        <p class="copyright"
           style="width: 100%; position: relative; font-size: 12px; color:#b3b3b3;  text-align: center"><i
                class="fa-copyright"></i>
            2020 GymWijzer. Alle rechten voorbehouden.</p>
    </div>
</div>
</body>
</html>

