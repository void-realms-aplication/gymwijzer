@extends('emails.layout')

@section('title')
    Bijna Verlopen
@endsection

@section('body')
    <p>Hallo {{$name}},</p>
    <p>Uw account voor de gymwijzer verloopt over {{$days}} dagen.</p>
    <p><a href="{{Route('renew')}}">U kunt uw abonement hier alvast verlengen.</a></p>
@endsection
