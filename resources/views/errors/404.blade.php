@extends('layouts.htmlStart',['title' => '404 - GymWijzer', 'pageid' => 'gymwijzer-demo'])

@section('head')
    <link rel="stylesheet" href="/css/gymwijzerpages.css">
    <link rel="stylesheet" href="/css/error.css">

@endsection

@section('content')
    @include('gymwijzer.partials.breadcrumbs', ['heading' => '404',
        'backurl' => route('volgwijzer'),
        'crumbs' => [['name' => 'GymWijzer!', 'url' => route('start')], ['name' => '404', 'url' => url()->current()]]
    ])


    <div class="wrapper" style="height: 100%">
        <section class="flex">
            <div class="d1">
                <h1>Helaas een 404</h1>
                <p>Het lijkt of deze pagina niet bestaat</p>
                <p>Gelukkig zijn er altijd meer manieren om aan de top te komen</p>
                <a href="{{Route('start')}}" class="action-button">Terug naar het beging</a>
            </div>
            <div class="d2">

            </div>
        </section>
    </div>

@endsection




