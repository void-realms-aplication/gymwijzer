@extends('layouts.htmlStart', [
    'title' => "Betaling | GymWijzer - Word lid",
    'pageid' => 'purchase-payment',
    'pageclasses' => 'purchase-page'
    ])

@section('content')
    @include('gymwijzer.partials.breadcrumbs', ['heading' => 'GymWijzer! - word lid',
        'backurl' => route('gymwijzer.index'),
        'crumbs' => [['name' => 'GymWijzer!', 'url' => route('start')], ['name' => 'GymWijzer! - Word lid', 'url' => url()->current()]
    ])

    <div class="wrapper">

        <h1>Betaling</h1>
        <p style="text-align: center">U wordt nu doorgestuurd naar de betaalpagina! <br />
            Althans, dat is de toekomstige bedoeling als dat zover is!</p>

    </div>

@endsection
