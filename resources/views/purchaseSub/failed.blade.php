@extends('layouts.htmlStart', [
    'title' => "Betaling mislukt | GymWijzer - Word lid",
    'pageid' => 'purchase-payment-fail',
    'pageclasses' => 'purchase-page'
    ])

@section('content')

    <div class="wrapper">
        @include('gymwijzer.partials.breadcrumbs')

        <h1>Betaling mislukt!</h1>
        <p style="text-align: center">
            Uw betaling is helaas niet geslaagd. <br />

        </p>

    </div>

@endsection
