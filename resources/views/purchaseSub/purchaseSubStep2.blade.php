@extends('layouts.htmlStart', [
    'title' => "GymWijzer - Word lid",
    'pageid' => 'purchase-cart',
    'pageclasses' => 'purchase-page'
    ])

@section('content')
    @include('gymwijzer.partials.breadcrumbs', ['heading' => 'GymWijzer! - word lid',
        'backurl' => route('purchase.start'),
        'crumbs' => [['name' => 'GymWijzer!', 'url' => route('start')], ['name' => 'GymWijzer! - Word lid', 'url' => url()->current()]]
    ])


    <div class="wrapper">

        <form method="POST" action="{{route('mollie')}}" class="ui equal width form @if($errors->any())error @endif">
            @csrf


            <div class="field">
                <label>Uw gegevens</label>
                <div class="three fields">
                    <div class="field @error('firstname')error @enderror">
                        <input type="text" name="firstname" placeholder="Voornaam" value="{{ old('firstname') }}">
                        @error('firstname')
                        <x-field-error>{{ $message }}</x-field-error>
                        @enderror
                    </div>

                    <div class="field" @error('tussenvoegsel')error @enderror>
                        <input type="text" name="tussenvoegsel" placeholder="Tussenvoegsel"
                               value="{{ old('tussenvoegsel') }}">
                        @error('tussenvoegsel')
                        <x-field-error>{{ $message }}</x-field-error>
                        @enderror

                    </div>
                    <div class="field @error('lastname')error @enderror">
                        <input type="text" name="lastname" placeholder="Achternaam" value="{{ old('lastname') }}">
                        @error('lastname')
                        <x-field-error>{{ $message }}</x-field-error>
                        @enderror
                    </div>
                </div>
            </div>

            <div id="company-name-input" class="field @error('company')error @enderror">
                <input type="text" name="company" placeholder="School of bedrijfsnaam" value="{{ old('company') }}">
                @error('company')
                <x-field-error>{{ $message }}</x-field-error>
                @enderror

            </div>

            <div class="field">
                <label>Adres</label>
                <div class="two fields">
                    <div class="field">
                        <div class="field @error('country')error @enderror">

                            <div class="ui fluid search selection dropdown selectCountry" id="select-subscription">
                                <input type="hidden" name="country" class="" value="{{ old('country') ?? 'nl' }}">
                                <i class="dropdown icon"></i>
                                <input class="search" autocomplete="off" tabindex="0">
                                <div class="text"><i class="nl flag"></i>Nederland</div>
                                <div class="menu" tabindex="-1">
                                    <div class="item active selected" data-value="nl"><i class="nl flag"></i>Nederland
                                    </div>
                                    <div class="item" data-value="be"><i class="be flag"></i>Belgie</div>
                                </div>

                            </div>

                            @error('country')
                            <x-field-error>{{ $message }}</x-field-error>
                            @enderror

                        </div>
                    </div>

                    <div class="field @error('city')error @enderror">
                        <input type="text" name="city" placeholder="Stad" value="{{ old('city') }}">

                        @error('city')
                        <x-field-error>{{ $message }}</x-field-error>
                        @enderror

                    </div>
                </div>

                <div class="fields">
                    <div class="eight wide field @error('street')error @enderror">
                        <input type="text" name="street" placeholder="Straat" value="{{ old('street') }}">

                        @error('street')
                        <x-field-error>{{ $message }}</x-field-error>
                        @enderror

                    </div>
                    <div class="three wide field @error('house_number')error @enderror">
                        <input type="text" name="house_number" placeholder="Huisnr" value="{{ old('house_number') }}">

                        @error('house_number')
                        <x-field-error>{{ $message }}</x-field-error>
                        @enderror

                    </div>
                    <div class="three wide field @error('additive')error @enderror">
                        <input type="text" name="additive" placeholder="Toevoeging" value="{{ old('additive') }}">
                        @error('additive')
                        <x-field-error>{{ $message }}</x-field-error>
                        @enderror

                    </div>
                    <div class="five wide field @error('postal_code')error @enderror">
                        <input type="text" name="postal_code" placeholder="Postcode" value="{{ old('postal_code') }}">

                        @error('postal_code')
                        <x-field-error>{{ $message }}</x-field-error>
                        @enderror

                    </div>

                </div>
            </div>
            <div class="fields">
                <div class="field @error('email')error @enderror">
                    <label>Email (van de school of opleiding)</label>
                    <input type="email" name="email" value="{{$email}}" placeholder="{{$email}}">

                    @error('email')
                    <x-field-error>{{ $message }}</x-field-error>
                    @enderror

                </div>
                <div class="field @error('phone_number')error @enderror">
                    <label>Telefoonnummer</label>
                    <input type="text" name="phone_number" placeholder="Telefoonnummer"
                           value="{{ old('phone_number') }}">

                    @error('phone_number')
                    <x-field-error>{{ $message }}</x-field-error>
                    @enderror

                </div>

                <div id="brin-input" class="field @error('brin_number')error @enderror ">
                    {{--                    @if( $selected->id == 1) style="display: none;" @endif--}}
                    <label id="brinNumber"><span id="brin-label">Brin nummer of instellingsnummer</span></label>

                    <input type="text" name="brin_number" placeholder="Brin nummer" value="{{ old('brin_number') }}">

                    @error('brin_number')
                    <x-field-error>{{ $message }}</x-field-error>
                    @enderror

                </div>
            </div>

            <div class="ui divider"></div>
            <div class="field" style="margin-bottom: 0;">
                <label>Abonnement keuze</label>
            </div>

            <div class="ui huge fluid selection dropdown selectSub @error('subscription_id')error @enderror"

                 id="select-subscription">
                <input type="hidden" name="subscription_id" @isset ($selected) value="{{$selected->id}}" @endisset>
                <i class="dropdown icon"></i>
                <div class="default text">@isset ($selected) {{$selected->name}} @else Selecteer alstublieft een
                    abonnement @endisset</div>
                <div class="menu">

                    @foreach($subscriptions as $subscription)
                        <div class="item" data-value="{{$subscription->id}}"
                             data-price="{{$subscription->price}}">{{$subscription->name}}</div>
                    @endforeach

                </div>

            </div>

            @error('subscription_id')
                <x-field-error>{{ $message }}</x-field-error>
            @enderror

            <div id="cart-summary">
                <div class="fields">
                    <div class="four wide field">
                        <div class="ui small statistic">
                            <div class="value">
                                €<span id="total-price">@if ($selected) {{$selected->price}} @endisset</span>
                            </div>
                            <div class="label">
                                Te betalen
                            </div>
                        </div>
                    </div>
                    <div class="twelve wide field">
                        <button class="ui fluid huge positive button" type="submit">Naar afrekenen</button>
                    </div>
                </div>
            </div>

        </form>

    </div>
@endsection

@section('post-content')

    @include('gymwijzer.partials.validation', ['validation' => $validation])

    <script>

        $('.ui.dropdown.selectSub').dropdown({
            onChange: function (value, text, $choice) {

                if ($choice.data('price')) {
                    $('#total-price').html($choice.data('price'))
                }
                if ($choice.data('value') === 1) {
                    $('#brin-input').css('display', 'none');
                    $('#company-name-input').css('display', 'none');
                    $('.ui.form').form('remove fields', ['brin_number', 'company']);
                } else if ($choice.data('value') !== 1) {
                    $('#brin-input').css('display', 'block');
                    $('#company-name-input').css('display', 'block');
                    $('.ui.form').form('add rule', 'brin_number', {
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'Een brin of instellingsnummer is verplicht, vul deze alstublieft in'
                            },
                            {
                                type: 'maxLength[6]',
                                prompt: 'Een brin of instellingsnummer mag maximaal 6 karakters lang zijn'
                            },
                            {
                                type: 'minLength[4]',
                                prompt: 'Een brin of instellingsnummer moet minimaal 4 karakters lang zijn'
                            }
                        ]
                    });
                    $('.ui.form').form('add rule', 'company', {
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'Een school of bedrijfsnaam is verplicht.'
                            }
                        ]
                    });
                }

                // if ($choice.data('value') == "be") {
                //     $('#brin-input').css('display', 'block');
                //     $('.ui.form').form('remove fields', ['brin_number']);
                //     $('.ui.form').form('add rule', 'instellingsnummer', {
                //         rules: [
                //             {
                //                 type: 'empty',
                //                 prompt: 'Een instellingsnummer is verplicht in België, vul deze alstublieft in'
                //             },
                //             {
                //                 type: 'maxLength[6]',
                //                 prompt: 'Een instellingsnummer mag maximaal 6 karakters langzijn'
                //             },
                //             {
                //                 type: 'minLength[4]',
                //                 prompt: 'Een instellingsnummer moet minimaal 4 karakters lang zijn'
                //             }
                //         ]
                //     });
                // }
                // if ($choice.data('value') == "nl") {
                //     $('#brin-input').css('display', 'block');
                //     $('.ui.form').form('add rule', 'brin_number', {
                //         rules: [
                //             {
                //                 type: 'empty',
                //                 prompt: 'Een brin nummer is verplicht in Nederland, vul deze alstublieft in'
                //             },
                //             {
                //                 type: 'maxLength[6]',
                //                 prompt: 'Een brin nummer mag maximaal 6 karakters lang zijn'
                //             },
                //             {
                //                 type: 'minLength[4]',
                //                 prompt: 'Een brin nummer moet minimaal 4 karakters lang zijn'
                //             }
                //         ]
                //     });
                // }

            }
        })
        // $('.ui.dropdown.selectCountry').dropdown({
        //     onChange: function (value, text, $choice) {
        //         if ($choice.data('value') == "nl") {
        //             $('#brin-label').html(brin_number)
        //         }
        //         if ($choice.data('value') == "be") {
        //             $('#brin-label').html(instellingsnummer)
        //         }
        //     }
        // })

        {{--        @i(old('country') != 'be')--}}
        {{--        $('.ui.form').form('add rule', 'brin_number', {--}}
        {{--            rules: [--}}
        {{--                {--}}
        {{--                    type: 'empty',--}}
        {{--                    prompt: 'Een brin nummer is verplicht in Nederland, vul deze alstublieft in'--}}
        {{--                },--}}
        {{--                {--}}
        {{--                    type: 'maxLength[6]',--}}
        {{--                    prompt: 'Een brin nummer mag maximaal 6 karakters zijn'--}}
        {{--                },--}}
        {{--                {--}}
        {{--                    type: 'minLength[4]',--}}
        {{--                    prompt: 'Een brin nummer moet minimaal 4 karakters zijn'--}}
        {{--                }--}}
        {{--            ]--}}
        {{--        });--}}
        {{--        @ndif--}}

        {{--            remove brin nummer when student sub is selected--}}
        @if(1 == $selected->id)
        $('#brin-input').css('display', 'none');
        $('#company-name-input').css('display', 'none');
        $('.ui.form').form('remove fields', ['brin_number', 'company']);

        @endif
    </script>

@endsection
