@extends('layouts.htmlStart', [
    'title' => "GymWijzer - Word lid",
    'pageid' => 'purchase-subscription',
    'pageclasses' => 'purchase-page'
 ])

@section('content')
    @include('gymwijzer.partials.breadcrumbs', ['heading' => 'GymWijzer! - word lid',
        'backurl' => route('gymwijzer.index'),
        'crumbs' => [['name' => 'GymWijzer!', 'url' => route('start')], ['name' => 'GymWijzer! - Word lid', 'url' => url()->current()]]
    ])

    <div class="wrapper">
        <h1>GymWijzer! aanschaffen</h1>

        <div id="purchase-info-block">
            <div id="info">
                <h4>Voor de aanschaf van GymWijzer! heeft u de volgende mogelijkheden:</h4>

                <table class="ui very basic celled table">
                    <tbody>
                        @foreach($subscriptions as $subscription)
                            <tr>
                                <td><strong>{{$subscription->name}}</strong></td>
                                <td>€{{$subscription->price}},- per jaar</td>
                            </tr>
{{--                            <strong>{{$subscription->name}}</strong>: €{{$subscription->price}},- per jaar <br />--}}
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div id="right-info">
                <h4>De volgende opties zijn bij elk abonnement inbegrepen:</h4>
                <ul class="subscription-features">
                    <li>50 bewegingsactiviteiten</li>
                    <li>1500 instructievideo’s</li>
                    <li>400 kijkwijzers</li>
                    <li>LVS VolgWijzer!</li>
                </ul>
            </div>
        </div>

        <div id="purchase-more-info">
            <div class="ui large info message">
                <ul class="list">
                    <li>U dient bij het inschrijven gebruik te maken van het e-mailadres van de school/organisatie waar u werkt of studeert. </li>
                    <li>De pijldatum voor het aantal leerlingen is 1 oktober, voor Belgische scholen 1 februari. </li>
                    <li>Het abonnement wordt  per school afgesloten.</li>
                    <li>Vul de gegevens naar waarheid in.</li>
                    <li>GymWijzer! is gemachtigd verdere stappen te ondernemen bij overtreding.</li>
                </ul>
            </div>
        </div>

        <div id="purchase-init-block">
            <div class="ui huge centered header">Selecteer je abonnement</div>
            <form method="POST" action="{{route('purchase.checkout')}}" class="ui huge form  @if($errors->any()) error @endif">
                @csrf
                <div class="equal width inline fields">
                    @foreach($subscriptions as $subscription)
                        <div class="field @error('subscription_id')error @enderror">
                            <div class="ui large radio checkbox">
                                <input type="radio" id="subscription-checkbox-{{$subscription->id}}"
                                       name="subscription_id" value="{{$subscription->id}}"
                                       tabindex="0" class="hidden"
                                       {{ (old('subscription_id') == $subscription->id) ? 'checked=""' : '' }}
                                       @if($selected == $subscription->id)
                                       checked=""
                                    @endif
                                >
                                <label for="subscription-checkbox-{{$subscription->id}}">{{$subscription->name}}</label>
                            </div>
                        </div>
                    @endforeach

                        @error('subscription_id')
                            <x-field-error>{{ $message }}</x-field-error>
                        @enderror

                </div>
                <div class="fields">
                    <div class="twelve wide field @error('email') error @enderror">
                        <input name="email" type="email" placeholder="Uw email">

                        @error('email')
                            <x-field-error>{{ $message }}</x-field-error>
                        @enderror

                    </div>
                    <div class="four wide field">
                        <button type="submit" class="ui huge positive fluid submit button">Aanmelden</button>
                    </div>
                </div>

            </form>

        </div>
    </div>
@endsection

@section('post-content')
    @include('gymwijzer.partials.validation', ['validation' => $validation])

    <script>
        $('.message .close')
            .on('click', function() {
                $(this)
                    .closest('.message')
                    .transition('fade')
                ;
            })
        ;
    </script>
@endsection
