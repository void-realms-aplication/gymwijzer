@extends('layouts.htmlStart', [
    'title' => "Betaling voltooid | GymWijzer - Word lid",
    'pageid' => 'purchase-payment-success',
    'pageclasses' => 'purchase-page'
    ])

@section('content')
    @include('gymwijzer.partials.breadcrumbs', ['heading' => 'GymWijzer! - word lid',
        'backurl' => route('purchase.start'),
        'crumbs' => [['name' => 'GymWijzer!', 'url' => route('start')], ['name' => 'GymWijzer! - Word lid', 'url' => '']]
    ])

    <div class="wrapper">

        <h1>Betaling voltooid!</h1>
        <p style="text-align: center">
            Uw betaling is voltooid! Welkom bij GymWijzer! <br />
            Voordat u begint met het instellen van uw school moet u eerst een wachtwoord instellen! <br/>
            Daarvoor is er zojuist een link naar uw email verstuurd. <br />

            Als u een wachtwoord heeft ingesteld kunt u <a href="{{route('login')}}">inloggen.</a>
        </p>

    </div>

@endsection
