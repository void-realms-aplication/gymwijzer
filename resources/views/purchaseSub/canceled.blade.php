@extends('layouts.htmlStart', [
    'title' => "Betaling geanuleerd | GymWijzer - Word lid",
    'pageid' => 'purchase-payment-canceled',
    'pageclasses' => 'purchase-page'
    ])

@section('content')

    <div class="wrapper">
        @include('gymwijzer.partials.breadcrumbs')

        <h1>Betaling geanuleerd</h1>
        <p style="text-align: center">
            Uw betaling is helaas geanuleerd.<br />

        </p>

    </div>

@endsection
