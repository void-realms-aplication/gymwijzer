@extends('layouts.htmlStart', [
    'title' => 'contact success',
    'pageid' => 'contact-success',
    ])

@section('content')
    @include('gymwijzer.partials.breadcrumbs', ['heading' => 'GymWijzer! - Contact',
        'backurl' => route('start'),
        'crumbs' => [['name' => 'GymWijzer!', 'url' => ''], ['name' => 'GymWijzer! - Contact', 'url' => route('contactUs')]]
    ])

    <div class="wrapper" style="text-align: center">

        <h1>Bericht ontvangen!</h1><br>
        <p>
            Uw bericht is ontvangen,<br />
            Wij gaan u zo snel mogelijk een reactie stuuren.
        </p>
<a class="action-button">terug naar de home page</a>
    </div>

@endsection
