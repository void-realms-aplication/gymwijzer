@extends('layouts.htmlStart')

@section('content')
    @include('gymwijzer.partials.breadcrumbs', ['heading' => 'GymWijzer! - Contact',
        'backurl' => route('start'),
        'crumbs' => [['name' => 'GymWijzer!', 'url' => '/'], ['name' => 'GymWijzer! - Contact', 'url' => '/contact-us']]
    ])

    <div class="wrapper">

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            @if (session('warning'))
                <div class="alert alert-warning">
                    {{ session('warning') }}
                </div>
            @endif

        <br>
            <form method="post" action="{{ route('contactUs.store') }}">
                {{ csrf_field() }}
                <h1>Neem contact op</h1>
                <p>Velden die gemarkeerd zijn met een <span style="color: red">*</span> zijn verplichte velden.</p>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                            Naam: <span style="color: red" >*</span><br>
                            <input type="text" name="name" class="form-control" required/>
                            @if ($errors->has('name'))
                                <span class="help-block">
											<strong>{{ $errors->first('name') }}</strong>
										</span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                            Uw emailadres: <span style="color: red" >*</span><br>
                            <input type="email" name="email" class="form-control" required/>
                            @if ($errors->has('email'))
                                <span class="help-block">
											<strong>{{ $errors->first('email') }}</strong>
										</span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('subject') ? ' has-error' : '' }}">
                            Het onderwerp van uw bericht: <span style="color: red" >*</span><br>
                            <input type="text" name="subject" class="form-control" />
                            @if ($errors->has('subject'))
                                <span class="help-block">
											<strong>{{ $errors->first('subject') }}</strong>
										</span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('message') ? ' has-error' : '' }}">
                            Uw bericht: <span style="color: red" >*</span><br>
                            <textarea name="message" class="form-control"
                                      style="width: 100%; height: 150px;" required></textarea>

                        <div class="form-group">

                            <input type="submit" name="btnSubmit" class="action-button"
                                   value="Stuur bericht"/>

                        </div>
                    </div>
                    <div class="col-md-6">

                            @if ($errors->has('message'))
                                <span class="help-block">
				<strong>{{ $errors->first('message') }}</strong>
				</span>
                            @endif
                        </div>
                    </div>
                </div>
            </form>
        </div>

@endsection
