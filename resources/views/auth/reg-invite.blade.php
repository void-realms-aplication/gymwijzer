@extends('layouts.htmlStart', [
    'title' => "Docent account aanmaken | GymWijzer!",
    'pageid' => "register-invite-page"
    ])

@section('content')

    @include('gymwijzer.partials.breadcrumbs', ['heading' => 'Registreren door uitnodiging',
         'backurl' => route('start'),
        'crumbs' => [['name' => 'GymWijzer!', 'url' => ''], ['name' => 'Registreren door uitnodiging', 'url' => url()->current()]
        ]
    ])

    <div class="wrapper">
        <form id="invite-teachers" method="POST" action="{{ route('user.saveNewUserWithInviteToken') }}" class="ui form">
            @csrf

            <input type="hidden" name="token" value="{{ $token ?? '' }}">

            <div class="field @error('firstname')error @enderror">
                <label>Voornaam</label>
                <input type="text" name="firstname" value="{{ old('firstname') }}" placeholder="{{ old('firstname') }}">
            </div>

            <div class="field @error('lastname')error @enderror">
                <label>Achternaam</label>
                <input type="text" name="lastname" value="{{ old('lastname') }}" placeholder="{{ old('lastname') }}">
            </div>

            <div class="field @error('email')error @enderror">
                <label>Email</label>
                <input type="text" name="email" style="color: #D4D4D4"
                       value="{{ $expectedEmail }}" placeholder="{{ $expectedEmail }}" readonly="" required>
            </div>

            <div class="field @error('password')error @enderror">
                <label>Wachtwoord</label>
                <input type="password" name="password" required>
            </div>
            <div class="field @error('password')error @enderror">
                <label>Wachtwoord bevestigen</label>
                <input type="password" name="password_confirmation" required>
            </div>

            <div class="field">
                <button class="ui fluid huge positive button" type="submit">Registreren</button>
            </div>

            @if($errors->any())
                <div class="ui error message">
                    <div class="header">Er is een fout opgetreden</div>
                    <ul class="list">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

        </form>

    </div>

@endsection

@section('post-content')
    @include('gymwijzer.partials.validation', ['validation' => $validation])
@endsection
