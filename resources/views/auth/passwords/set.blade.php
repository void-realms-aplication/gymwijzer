@extends('layouts.htmlStart', ['title' => 'Wachtwoord instellen - GymWijzer!', 'pageid' => 'password-confirm'])

@section('head')
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
@endsection

@section('content')
    @include('gymwijzer.partials.breadcrumbs', [
         'heading' => 'Wachtwoord instellen',
         'backurl' => route('start'),
         'crumbs' => [['name' => 'GymWijzer!', 'url' => route('start')], ['name' => 'Wachtwoord instellen', 'url' => url()->current()]
         ]
    ])

    <div class="wrapper">
        <form method="POST" action="{{ route('password.create') }}" class="ui form">
            @csrf

            <input type="hidden" name="token" value="{{ $token ?? '' }}">

            <div class="field @error('email')error @enderror">
                <label>Email</label>
                <input type="email" name="email" value="{{ old('email') }}" value="{{ $email ?? old('email') }}">

                @error('email')
                    <x-field-error>{{ $message }}</x-field-error>
                @enderror

            </div>

            <div class="field @error('password')error @enderror">
                <label>Nieuw wachtwoord</label>
                <input type="password" name="password">

                @error('password')
                    <x-field-error>{{ $message }}</x-field-error>
                @enderror

            </div>

            <div class="field @error('password')error @enderror">
                <label>Wachtwoord bevestigen</label>
                <input type="password" name="password_confirmation">
            </div>

            <div class="field">
                <button class="ui fluid large positive button" type="submit">Wachtwoord instellen</button>
            </div>
        </form>

        @error('token')
            @if(!$errors->has('email'))

                <div class="ui error message">
                    <ul class="list">
                            <li>{{ $message }}</li>
                    </ul>
                </div>
            @endif
        @enderror

    </div>

@endsection

@section('post-content')
    @include('gymwijzer.partials.validation', ['validation' => $validation])
@endsection
