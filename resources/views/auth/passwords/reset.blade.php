@extends('layouts.htmlStart', ['title' => 'Wachtwoord herstellen - GymWijzer!', 'pageid' => 'password-reset'])

@section('head')
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
@endsection

@section('content')
    @include('gymwijzer.partials.breadcrumbs', [
         'heading' => 'Wachtwoord herstellen',
         'backurl' => route('login'),
         'crumbs' => [['name' => 'GymWijzer!', 'url' => route('start')], ['name' => 'Wachtwoord herstellen', 'url' => url()->current()]
         ]
    ])

    <div class="wrapper">
        <form method="POST" action="{{ route('password.update') }}" class="ui form">
            @csrf

            <input type="hidden" name="token" value="{{ $token }}">

            <div class="field @error('email')error @enderror">
                <label>Email</label>
                <input type="email" name="email" value="{{ old('email') }}" placeholder="{{ old('email') }}">
            </div>

            <div class="field @error('password')error @enderror">
                <label>Nieuw wachtwoord</label>
                <input type="password" name="password" value="{{ old('password') }}" placeholder="{{ old('password') }}">
            </div>

            <div class="field @error('password')error @enderror">
                <label>Wachtwoord bevestigen</label>
                <input type="password" name="password_confirmation">
            </div>

            <div class="field">
                <button class="ui fluid large blue button" type="submit">Wachtwoord herstellen</button>
            </div>
        </form>

        @if($errors->any())
            <div class="ui error message">
                <div class="header">Er is een fout opgetreden</div>
                <ul class="list">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>



@endsection
