@extends('layouts.htmlStart', ['title' => 'Wachtwoord vergeten - GymWijzer!', 'pageid' => 'login-page'])

@section('head')
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
@endsection

@section('content')

    @include('gymwijzer.partials.breadcrumbs', [
         'heading' => 'Wachtwoord vergeten?',
         'backurl' => route('login'),
         'crumbs' => [['name' => 'GymWijzer!', 'url' => route('start')], ['name' => 'Wachtwoord vergeten', 'url' => url()->current()]
         ]
    ])

    <div class="wrapper">
        <form method="POST" action="{{ route('password.email') }}" class="ui form">
            @csrf

            <div class="field @error('email')error @enderror">
                <label>Email</label>
                <input type="email" name="email" value="{{ old('email') }}" placeholder="{{ old('email') }}">
            </div>

            <div class="field">
                <button class="ui fluid large blue button" type="submit">Wachtwoord herstel link versturen</button>
            </div>
        </form>

        @if($errors->any())
            <div class="ui error message">
                <div class="header">Er is een fout opgetreden</div>
                <ul class="list">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

@endsection
