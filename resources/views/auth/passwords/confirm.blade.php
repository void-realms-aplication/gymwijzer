@extends('layouts.htmlStart', ['title' => 'Wachtwoord bevestigen - GymWijzer!', 'pageid' => 'password-confirm'])

@section('head')
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
@endsection

@section('content')
    @include('gymwijzer.partials.breadcrumbs', [
         'heading' => 'Bevestig uw wachtwoord voordat u verder gaat',
         'crumbs' => [['name' => 'GymWijzer!', 'url' => route('start')], ['name' => 'Wachtwoord bevestigen', 'url' => url()->current()]
         ]
    ])

    <div class="wrapper">
        <form method="POST" action="{{ route('password.confirm') }}" class="ui form">
            @csrf

            <div class="field @error('password')error @enderror">
                <label>Uw wachtwoord</label>
                <input type="password" name="password">
            </div>

            <div class="field">
                <button class="ui fluid large blue button" type="submit">Wachtwoord bevestigen</button>
            </div>
        </form>


        @if($errors->any())
            <div class="ui error message">
                <div class="header">Er is een fout opgetreden</div>
                <ul class="list">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

@endsection
