@extends('layouts.htmlStart')

@section('content')

    @include('gymwijzer.partials.breadcrumbs', ['heading' => 'Verlopen',
        'backurl' => route('start'),
        'crumbs' => [['name' => 'Home', 'url' => '/'],['name' => 'verlopen', 'url' => '']]
    ])

    <div class="wrapper" >
        <h1>Uw account is verlopen</h1>
        @if($user->role_id == 3)
            <p>Het account voor Gymwijzer dat uw voor uw organisatie heeft aangaschaft is verlopen.</p>
            <a class="action-button" style="max-width: 400px; width: 90%;" href="{{route('renew')}}">Klik hier om uw abonement te verlengen</a>
        @else
            <p>Het account voor Gymwijzer dat uw organisatie heeft aangaschaft is verlopen.
                Binnen uw organisatie is
                meneer/mevrouw {{$lastNameAdmin}} de contact persoon voor GymWijzer, hij/zij kan het abonnement
                verlengen
            </p>
        @endif
        <h3>Wat gebeurt er met de gegevends die ik heb ingevult?</h3>
        <p>De gegevens van scholen, gebruikers en kinderen blijven standard twee jaar na het verlopen van het
            abonnement
            op onze server staan, zodra het abonnement is verlengd kunt u de Gym- en VolgWijzer gelijk weer gebruiken.
        </p>
    </div>

@endsection

