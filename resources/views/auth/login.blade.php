@extends('layouts.htmlStart', ['title' => 'Inloggen - GymWijzer!', 'pageid' => 'login-page'])

@section('head')
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('content')
    <div id="login-wrap" class="wrapper">
        <div class="login-box">
            <div class="img"></div>

            <div id="login-form">
                <div class="form-heading">
                    <h3>{{ __('Inloggen') }}</h3>
                </div>

                <form method="POST" action="{{ route('login') }}" class="ui form">
                    @csrf
                    <div class="form-group row">
                        <input id="email" type="email"
                               class="form-control field @error('email') is-invalid @enderror" name="email"
                               value="{{ old('email') }}" required autocomplete="email" autofocus>

                        <input id="password" type="password"
                               class="form-control field @error('password') is-invalid @enderror" name="password"
                               required autocomplete="current-password">

                        <div class="ui {{ old('remember') ? 'checked' : '' }} checkbox">
                            <input id="login-remember-me" type="checkbox" name="remember" {{ old('remember') ? 'checked=""' : '' }}>
                            <label for="login-remember-me">  {{ __('Aangemeld blijven') }}</label>
                        </div>

                        <button type="submit" class="action-button">
                            {{ __('Inloggen') }}
                        </button>

                    </div>

                </form>

                @if($errors->any())
                    <div class="ui error message">
                        <ul class="list">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div id="form-footer">
                    <div class="ui divider"></div>

                    <div class="form-footer-a">
                        @if (Route::has('password.request'))
                            <a href="{{ route('password.request') }}">{{__('Wachtwoord vergeten?')}}</a>
                        @endif
                    </div>
                    <div class="form-footer-b">
                        <a href="#">{{__('Terug naar Gymwijzer')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
