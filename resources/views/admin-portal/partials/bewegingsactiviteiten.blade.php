<h2 class="ui header" style="margin: 0;">Bewegingsactiviteiten</h2>

@if(count($activiteiten))

    <table class="ui selectable table">
        <thead>
        <tr>
            <th>Naam</th>
            <th>Videos</th>
            <th>Demo</th>
            <th colspan="2">Beheren</th>
        </tr>
        </thead>
        @foreach($activiteiten ?? '' as $activiteit)
            <tr>
                <td>
                    <a href="{{route('gymwijzer.bewegingsthema', [
                               $activiteit->parent->parent->path,
                               $activiteit->parent->path,
                               $activiteit->path])}}">
                        {{ $activiteit->name }}
                    </a>
                </td>
                <td>
                    {{ $activiteit->allVideos()->count() }}
                </td>

                <td {{ $activiteit->demo ? 'class=positive' : ""}}>{{ $activiteit->demo ? 'Ja' : 'Nee' }} </td>

                <td class="collapsing"><a class="Edit-button"
                                          href={{route('adminportal.editbewegingsactiviteit', $activiteit->path)}}>Bewerken</a></td>

                <td class="right aligned collapsing">
                            <span>
                                <a href="{{ route('adminportal.deletebewegingsactiviteit', $activiteit->path) }}"
                                   onclick="event.preventDefault();
                                       {{--document.getElementById('delete-bewegingsactiviteit-{{ $activiteit->path}}').submit()--}}
                                       deleteEntity('bewegingsactiviteit', '{{ $activiteit->path }}', '{{$activiteit->name}}');
                                       "
                                   class="ui  tiny red labled icon button">
                                <i class="delete icon"></i>
                                Verwijderen
                                </a>
                            </span>

                    <form id="delete-bewegingsactiviteit-{{ $activiteit->path }}" method="POST" style="display:none;"
                          action="{{route('adminportal.deletebewegingsactiviteit', $activiteit->path)}}">
                        {{ csrf_field() }}
                    </form>

                </td>

            </tr>
        @endforeach
    </table>

@else
    <p>Geen bewegingsactiviteiten gevonden</p>
@endif
