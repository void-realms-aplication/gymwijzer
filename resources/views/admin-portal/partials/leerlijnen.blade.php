<h2 class="ui header" style="margin: 0;">Leerlijnen</h2>
@if(count($leerlijnen))

    <table class="ui selectable table">
        <thead>
        <tr>
            <th>Naam</th>
            <th>Bewegingsthemas</th>
            <th>Bewegingsactiviteiten</th>
            <th>Demo</th>
            <th colspan="2">Beheren</th>
        </tr>
        </thead>
        @foreach($leerlijnen ?? '' as $leerlijn)
            <tr>
                <td><a style="font-size: 16px;" href="{{route('gymwijzer.leerlijn', $leerlijn->path)}}">
                        {{ $leerlijn->name }}</a></td>

                <td>
                    {{ $leerlijn->themas->count() }}
                </td>

                <td>
                    {{ $leerlijn->activiteiten->count() }}
                </td>

                <td {{ $leerlijn->demo ? 'class=positive' : ""}}>{{ $leerlijn->demo ? 'Ja' : 'Nee' }} </td>

                <td class="collapsing"><a class="Edit-button"
                       href={{route('adminportal.editleerlijn', $leerlijn->path)}}>Bewerken</a></td>

                <td class="right aligned collapsing">
                            <span class="@if($leerlijn->themas->count() >= 1) popupable @endif"
                                  data-content="Deze leerlijn bevat bewegingsthemas en kan niet worden verwijderd">
                                <a href="{{ route('adminportal.deleteleerlijn', $leerlijn->path) }}"
                                   onclick="event.preventDefault();
                                       deleteEntity('leerlijn', '{{ $leerlijn->path }}', '{{ $leerlijn->name }}')"
                                   class="ui  @if($leerlijn->themas->count() >= 1)disabled  @endif tiny red labled icon button">
                                <i class="delete icon"></i>
                                Verwijderen
                                </a>
                            </span>

                    <form id="delete-leerlijn-{{ $leerlijn->path }}" method="POST" style="display:none;"
                          action="{{route('adminportal.deleteleerlijn', $leerlijn->path)}}">
                        {{ csrf_field() }}
                    </form>

                </td>
            </tr>
        @endforeach
    </table>

@else
    <p>Geen leerlijnen gevonden</p>
@endif
