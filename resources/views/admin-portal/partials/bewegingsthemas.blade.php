<h2 class="ui header" style="margin: 0;">Bewegingsthemas</h2>

@if(count($themas))

    <table class="ui selectable table">
        <thead>
        <tr>
            <th>Naam</th>
            <th>Bewegingsactiviteiten</th>
            <th>Demo</th>
            <th colspan="2">Beheren</th>
        </tr>
        </thead>
        @foreach($themas ?? '' as $thema)
            <tr>
                <td><a style="font-size: 16px;" href="{{route('gymwijzer.bewegingsactiviteit', [$thema->parent->path, $thema->path])}}">
                        {{ $thema->name }}</a></td>

                <td>
                    {{ $thema->activiteiten->count() }}
                </td>

                <td {{ $thema->demo ? 'class=positive' : ""}}>{{ $thema->demo ? 'Ja' : 'Nee' }} </td>

                <td class="collapsing"><a class="Edit-button"
                       href={{route('adminportal.editbewegingsthema', $thema->path)}}>Bewerken</a></td>

                <td class="right aligned collapsing">
                            <span class="@if($thema->activiteiten->count() >= 1) popupable @endif"
                                  data-content="Deze thema bevat bewegingsactiviteiten en kan niet worden verwijderd">
                                <a href="{{ route('adminportal.deletebewegingsthema', $thema->path) }}"
                                   onclick="event.preventDefault()
                                       deleteEntity('bewegingsthema', '{{ $thema->path}}', '{{ $thema->name}}')"
                                   class="ui  @if($thema->activiteiten->count() >= 1)disabled  @endif tiny red labled icon button">
                                <i class="delete icon"></i>
                                Verwijderen
                                </a>
                            </span>

                    <form id="delete-bewegingsthema-{{ $thema->path }}" method="POST" style="display:none;"
                          action="{{route('adminportal.deletebewegingsthema', $thema->path)}}">
                        {{ csrf_field() }}
                    </form>

                </td>

            </tr>
        @endforeach
    </table>

@else
    <p>Geen bewegingsthemas gevonden</p>
@endif
