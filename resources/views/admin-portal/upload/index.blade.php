@extends('layouts.htmlStart')

@section('content')
    @include('gymwijzer.partials.breadcrumbs', ['heading' => 'Klas uploaden',
       'backurl' => route('gymwijzer.admin'),
       'crumbs' => [['name' => 'GymWijzer!', 'url' => '/'], ['name' => 'Dashboard', 'url' => route('gymwijzer.admin')], ['name'=>'Klas uploaden', 'url' => '']]
   ])

    <div class="wrapper">
        <h1>Klas uploaden</h1>
        <p>
            Op deze pagina heeft u de mogelijkheid om een edexml bestand te uploaden, alle klassen en leerlingen worden
            dan automatisch in GymWijzer gezet. Alle grote leerlingvolgsystemen bieden de mogelijkheid om een edexml
            bestand te exporteren. Een edexml bestand eindigt met .xml.
        </p>

        <form method="post" action="{{route('upload.store')}}" enctype="multipart/form-data">
            @csrf
            <input type="file" name="file" required>
            <input type="submit" value="Upload">
        </form>

    </div>

@endsection
