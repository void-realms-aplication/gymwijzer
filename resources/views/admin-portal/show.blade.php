@extends('layouts.htmlStart', ['title' => 'Admin Portal - GymWijzer!', 'pageid' => 'admin-portal-page'])

@section('head')

@endsection

@section('content')
    @if($leerlijn['leerlijnNummer'])
        @include('gymwijzer.partials.breadcrumbs', ['heading' => 'Leerlijn: '. $leerlijn['name'],
            'backurl' => route('gymwijzer.admin'),
            'crumbs' => [['name' => 'GymWijzer!', 'url' => ''], ['name' => 'Admin-portal / '. $leerlijn['id'], 'url' => route('adminportal.show', $leerlijn)]
            ]
        ])
    @elseif($leerlijn['bewegingsthemaNummer'])
        @include('gymwijzer.partials.breadcrumbs', ['heading' => 'Bewegingsthema: '.$leerlijn['name'],
            'backurl' => route('gymwijzer.admin'),
            'crumbs' => [['name' => 'GymWijzer!', 'url' => ''], ['name' => 'Admin-portal / '. $leerlijn['id'], 'url' => route('adminportal.show', $leerlijn)]
            ]
        ])>
    @else
        @include('gymwijzer.partials.breadcrumbs', ['heading' => 'Bewegingsactiviteit: ' . $leerlijn['name'],
            'backurl' => route('gymwijzer.admin'),
            'crumbs' => [['name' => 'GymWijzer!', 'url' => ''], ['name' => 'Admin-portal / '. $leerlijn['id'], 'url' => route('adminportal.show', $leerlijn)]
            ]
        ])
    @endif
    @if(auth()->user()->role_id == 1 || auth()->user()->role_id == 2)
    <div class="wrapper">

            <table>
                <tr>
                    <a class="dashboard-toevoeg-button" href="{{url('admin-portal/leerlijn-aanmaken')}}">Leerlijn aanmaken</a>
                </tr>
                <tr>
                    <a class="dashboard-toevoeg-button" href="{{url('admin-portal/bewegingsactiviteit-aanmaken')}}">Bewegingsthema aanmaken</a>
                </tr>
                <tr>
                    <a class="dashboard-toevoeg-button" href="{{url('admin-portal/bewegingsthema-aanmaken')}}">Bewegingsactiviteit aanmaken</a>
                </tr>
            </table>

    <div class="ui piled segment">
        @if($leerlijn['leerlijnNummer'])
            <h4 class="ui header">Leerlijn: {{$leerlijn['name']}}</h4>
        @elseif($leerlijn['bewegingsthemaNummer'])
            <h4 class="ui header">Bewegingsthema: {{$leerlijn['name']}}</h4>
        @else
            <h4 class="ui header">Bewegingsactiviteit: {{$leerlijn['name']}}</h4>
        @endif

            <table class="ui celled table">

                <tr>
                    <td>Naam</td>
                    <td>{{$leerlijn['name']}}</td>
                </tr>
                <tr>
                    <td>Path</td>
                    <td>{{$leerlijn['path']}}</td>
                </tr>

                @if($leerlijn['parent_path'])
                    <tr>
                        <td>Parent path</td>
                        <td>{{$leerlijn['parent_path']}}</td>
                    </tr>
                @endif

                @if($leerlijn['bewegingsactiviteitNummer'])
                    <tr>
                        <td>Has Video</td>
                        @if($leerlijn['has_video'] == 1)
                            <td>Ja</td>
                        @else
                            <td>Nee</td>
                        @endif
                    </tr>
                @endif

                <tr>
                    <td>Demo</td>
                    @if($leerlijn['demo'] == 1)
                        <td>Ja</td>
                    @else
                        <td>Nee</td>
                    @endif
                </tr>

            </table>
    </div>
    </div>
    @else
        <script>window.location = "/";</script>
    @endif
@endsection
