@extends('layouts.htmlStart', ['title' => 'GymWijzer! - Admin Portal', 'pageid' => 'admin-portal-page'])

@section('content')
    @include('gymwijzer.partials.breadcrumbs', ['heading' => 'Admin Portaal',
            'backurl' => route('start'),
            'crumbs' => [['name' => 'GymWijzer!', 'url' => ''], ['name' => 'Admin-portal', 'url' => route('gymwijzer.admin')]
            ]
        ])

    <div class="wrapper">
        <div class="ui three large buttons gw-admin-buttons" style="margin-bottom: 20px;">
            <a class="ui button" href="{{route('adminportal.createleerlijn')}}">Leerlijn aanmaken</a>
            <a class="ui button" href="{{route('adminportal.createbewegingsthema')}}">Bewegingsthema aanmaken</a>
            <a class="ui button" href="{{route('adminportal.createbewegingsactiviteit')}}">Bewegingsactiviteit
                aanmaken</a>
        </div>


        @include('admin-portal.partials.leerlijnen')

        @include('admin-portal.partials.bewegingsthemas')

        @include('admin-portal.partials.bewegingsactiviteiten')

    </div>

@endsection

@section('post-content')
    <script>
        $('.popupable')
            .popup()
        ;
    </script>

    <script>
        function deleteEntity($type, $path, $name) {
            $('body')
                .toast({
                    message: 'Weet u zeker dat u de ' + $type + ' <strong>' + $name + '</strong> wilt verwijderen?',
                    displayTime: 80000,
                    classActions: 'basic left',
                    title: 'Deze actie kan niet ongedaan worden',
                    actions: [
                        {
                        text: 'Verwijderen',
                        class: 'red',
                        click: function() {
                            document.getElementById('delete-' + $type +  '-' + $path).submit()
                        }
                    },
                        {
                            text: 'Annuleren',
                            class: 'blue',
                        }
                    ]
                })
            ;
        }
    </script>
@endsection
