@if(auth()->user()->role_id == 1 || auth()->user()->role_id == 2)
    @extends('layouts.htmlStart', ['title' => 'Admin Portal - GymWijzer!', 'pageid' => 'admin-portal-page'])

    @section('head')

    @endsection

    @section('content')
        @include('gymwijzer.partials.breadcrumbs', ['heading' => 'Bewegingsactiviteit Aanmaken',
            'backurl' => route('gymwijzer.admin'),
            'crumbs' => [['name' => 'GymWijzer!', 'url' => ''], ['name' => 'Bewegingsactiviteit-aanmaken', 'url' => route('adminportal.createbewegingsactiviteit')]
            ]
        ])
        <div class="wrapper">
            <table>
                <tr>
                    <a class="dashboard-toevoeg-button" href="{{url('admin-portal/leerlijn-aanmaken')}}">Leerlijn aanmaken</a>
                </tr>
                <tr>
                    <a class="dashboard-toevoeg-button" href="{{url('admin-portal/bewegingsactiviteit-aanmaken')}}">Bewegingsthema aanmaken</a>
                </tr>
                <tr>
                    <a class="dashboard-toevoeg-button" href="{{url('admin-portal/bewegingsthema-aanmaken')}}">Bewegingsactiviteit aanmaken</a>
                </tr>
            </table>



        <div class="ui piled segment">
            <h4 class="ui header">Bewegingsactiviteit aanmaken</h4>
            <form method="POST" action="/admin-portal">
                @csrf
                <table class="ui celled table">
                    <tr>
                        <td>Naam:</td>
                        <td><input class="ui input" type="text" id="nameInputText" name="name" required></td>
                    </tr>

                    <tr>
                        <td>Parent path:</td>
                        <td>
                            <select class="ui dropdown" name="parent_path" id="parentPathSelect" onchange="pathOnchange()" required>
                                <option selected></option>
                                @foreach($leerlijn as $row)
                                    @if($row['bewegingsthemaNummer'])
                                        <option value="{{$row['path']}}">{{$row['path']}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td>Path:</td>
                        <td><input class="ui input" type="text" name="path" id="pathInputText" required></td>
                    </tr>
                    <tr>
                        <td>Has video?</td>
                        <td>
                            <select class="ui dropdown" name="has_video" required>
                                <option selected></option>
                                <option value="1">Ja</option>
                                <option value="0">Nee</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Demo?</td>
                        <td>
                            <select class="ui dropdown" name="demo" required>
                                <option selected></option>
                                <option value="1">Ja</option>
                                <option value="0">Nee</option>
                            </select>
                        </td>
                    </tr>
                </table>
                <input type="hidden" name="bewegingsactiviteitNummer" value="{{\App\Leerlijn::max('bewegingsactiviteitNummer') + 1}}">
                <button class="Edit-button" type="submit" value="submit">Aanmaken</button>
            </form>
        </div>
        </div>
        <script>
            function pathOnchange(){
                var path = document.getElementById("nameInputText").value;
                var parentPath = document.getElementById("parentPathSelect").value;
                document.getElementById("pathInputText").value = parentPath + "/" + path;
            }
        </script>

    @endsection
@else
    <script>window.location = "/";</script>
@endif
