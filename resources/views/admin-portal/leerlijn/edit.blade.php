@extends('layouts.htmlStart', ['title' => $leerlijn->name . ' - Leerlijn bewerken - GymWijzer!'])

@section('content')
    @include('gymwijzer.partials.breadcrumbs', ['heading' =>  $leerlijn->name . ' - Leerlijn bewerken',
        'backurl' => route('gymwijzer.admin'),
        'crumbs' => [ ['name' => 'GymWijzer!', 'url' => ''],
         ['name' => 'GymWijzer beheren', 'url' => route('gymwijzer.admin')],
         ['name' => $leerlijn->name . ' - Leerlijn bewerken', 'url' => route('adminportal.editleerlijn', $leerlijn->path)]
        ]
    ])

    <div class="wrapper">
        <div class="ui three large buttons gw-admin-buttons" style="margin-bottom: 20px;">
            <a class="ui button" href="{{route('adminportal.createleerlijn')}}">Leerlijn aanmaken</a>
            <a class="ui button" href="{{route('adminportal.createbewegingsthema')}}">Bewegingsthema aanmaken</a>
            <a class="ui button" href="{{route('adminportal.createbewegingsactiviteit')}}">Bewegingsactiviteit
                aanmaken</a>
        </div>


        <form method="POST" action="{{route('adminportal.editleerlijn', $leerlijn->path)}}" class="ui large form">
            @csrf
            @method('PUT')

            <div class="field @error('name')error @enderror">
                <label>Naam</label>
                <input type="text" id="nameInputText" name="name" onchange="pathOnchange()"
                       value="{{ old('name') ?? $leerlijn->name }}">
                @error('name')
                    <x-field-error>{{ $message }}</x-field-error>
                @enderror
            </div>

                <div class="field @error('path')error @enderror">
                    <label>Pad</label>
                <input type="text" name="path" id="pathInputText" value="{{ old('path') ?? $leerlijn->path }}">
                @error('path')
                    <x-field-error>{{ $message }}</x-field-error>
                @enderror
            </div>

            <div class="field" @error('demo')error @enderror style="margin-bottom: 15px;">
                <div class="ui checkbox" id="demo-checkbox">
                    <input type="checkbox" tabindex="0" {{ $leerlijn->demo == 1 ? 'checked' : ''}}>
                    <input type="hidden" id="demo-input" name="demo" value="{{old('demo') ?? 0 }}">
                    <label>Demo</label>
                </div>
                @error('demo')
                <div class="ui basic red left pointing prompt label transition visible"
                     style="display: inline-block !important;">{{ $message }}</div>
                @enderror
            </div>

            <div class="field">
                <button class="ui large blue button" class="Edit-button" type="submit" value="submit">Opslaan
                </button>
            </div>

        </form>
    </div>
@endsection

@section('post-content')
    @include('gymwijzer.partials.validation', ['validation' => $validation])

    <script>
        function pathOnchange() {
            var path = document.getElementById("nameInputText").value;
            document.getElementById("pathInputText").value = path.toLowerCase().replace(' ', "-");
        }
    </script>

    <script>
        $('#demo-checkbox')
            .checkbox({
                onChecked: function() {
                    $('#demo-input').val(1);
                },
                onUnchecked: function() {
                    $('#demo-input').val(0);
                }
            })
        ;
    </script>
@endsection
