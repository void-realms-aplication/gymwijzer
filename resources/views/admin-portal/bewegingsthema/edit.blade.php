@extends('layouts.htmlStart', ['title' => $bewegingsthema->name . ' - Bewegingsthema bewerken - GymWijzer!'])

@section('content')
    @include('gymwijzer.partials.breadcrumbs', ['heading' =>  $bewegingsthema->name . ' - Bewegingsthema bewerken',
        'backurl' => route('gymwijzer.admin'),
        'crumbs' => [ ['name' => 'GymWijzer!', 'url' => ''],
         ['name' => 'GymWijzer beheren', 'url' => route('gymwijzer.admin')],
         ['name' => $bewegingsthema->name . ' - Bewegingsthema bewerken', 'url' => route('adminportal.editbewegingsthema', $bewegingsthema->path)]
        ]
    ])

    <div class="wrapper">
        <div class="ui three large buttons gw-admin-buttons" style="margin-bottom: 20px;">
            <a class="ui button" href="{{route('adminportal.createleerlijn')}}">Leerlijn aanmaken</a>
            <a class="ui button" href="{{route('adminportal.createbewegingsthema')}}">Bewegingsthema aanmaken</a>
            <a class="ui button" href="{{route('adminportal.createbewegingsactiviteit')}}">Bewegingsactiviteit
                aanmaken</a>
        </div>


        <form method="POST" action="{{route('adminportal.editbewegingsthema', $bewegingsthema->path)}}"
              class="ui large form @if($errors->any()) error @endif ">
        @csrf
            @method('PUT')

            <div class="field @error('name')error @enderror">
                <label>Naam</label>
                <input type="text" id="nameInputText" name="name" onchange="pathOnchange()"
                       value="{{ old('name') ?? $bewegingsthema->name }}">
                @error('name')
                <x-field-error>{{ $message }}</x-field-error>
                @enderror
            </div>

            <div class="field @error('path')error @enderror">
                <label>Pad</label>
                <input type="text" name="path" id="pathInputText" value="{{ old('path') ?? $bewegingsthema->path }}">
                @error('path')
                <x-field-error>{{ $message }}</x-field-error>
                @enderror
            </div>

            <div class="field @error('leerlijn')error @enderror">
                <label>Leerlijn</label>
                <div class="ui selection dropdown">

                    <input type="hidden" name="leerlijn" value="{{ old('leerlijn') ?? $bewegingsthema->parent->path }}">
                    <i class="dropdown icon"></i>
                    <div class="default text">Leerlijn</div>
                    <div class="menu">
                        @foreach($leerlijnen as $leerlijn)
                            <div class="item" data-value="{{$leerlijn->path}}">{{$leerlijn->name}}</div>
                        @endforeach
                    </div>
                </div>
                @error('leerlijn')
                    <x-field-error>{{ $message }}</x-field-error>
                @enderror

            </div>

            <div class="field" @error('demo')error @enderror style="margin-bottom: 15px;">
                <div class="ui checkbox" id="demo-checkbox">
                    <input type="checkbox" tabindex="0" {{ $bewegingsthema->demo == 1 ? 'checked' : ''}}>
                    <input type="hidden" id="demo-input" name="demo" value="{{old('demo') ?? 0 }}">
                    <label>Demo</label>
                </div>
                @error('demo')
                <div class="ui basic red left pointing prompt label transition visible"
                     style="display: inline-block !important;">{{ $message }}</div>
                @enderror
            </div>

            <div class="field">
                <button class="ui large blue button" class="Edit-button" type="submit" value="submit">Opslaan
                </button>
            </div>

        </form>
    </div>
@endsection

@section('post-content')
    @include('gymwijzer.partials.validation', ['validation' => $validation])

    <script>
        function pathOnchange() {
            var path = document.getElementById("nameInputText").value;
            document.getElementById("pathInputText").value = path.toLowerCase().replace(' ', "-");
        }
    </script>

    <script>
        $('#demo-checkbox')
            .checkbox({
                onChecked: function() {
                    $('#demo-input').val(1);
                },
                onUnchecked: function() {
                    $('#demo-input').val(0);
                }
            })
        ;
    </script>
@endsection
