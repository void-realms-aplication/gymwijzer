@extends('layouts.htmlStart', ['title' => $bewegingsactiviteit->name . ' - Bewegingsactivieit bewerken - GymWijzer!',
'pageclasses' => 'edit-activiteit-page'
])

@section('head')

@endsection

@section('content')
    @include('gymwijzer.partials.breadcrumbs', ['heading' =>  $bewegingsactiviteit->name . ' - Bewegingsactivieit bewerken',
        'backurl' => route('gymwijzer.admin'),
        'crumbs' => [ ['name' => 'GymWijzer!', 'url' => ''],
         ['name' => 'GymWijzer beheren', 'url' => route('gymwijzer.admin')],
         ['name' => $bewegingsactiviteit->name . ' - Bewegingsactivieit bewerken',
            'url' => route('adminportal.editbewegingsactiviteit', $bewegingsactiviteit->path)]
        ]
    ])

    <div class="wrapper">
        <div class="ui three large buttons gw-admin-buttons" style="margin-bottom: 20px;">
            <a class="ui button" href="{{route('adminportal.createleerlijn')}}">Leerlijn aanmaken</a>
            <a class="ui button" href="{{route('adminportal.createbewegingsthema')}}">Bewegingsthema aanmaken</a>
            <a class="ui button" href="{{route('adminportal.createbewegingsactiviteit')}}">Bewegingsactiviteit
                aanmaken</a>
        </div>


        <form method="POST" action="{{route('adminportal.editbewegingsactiviteit', $bewegingsactiviteit->path)}}"
              class="ui large form @if($errors->any()) error @endif
              @if ($bewegingsactiviteit->allVideos()->count() >= 1) warning @endif">
            @csrf
            @method('PUT')

                <div class="field @error('name')error @enderror attached">
                    <label>Naam</label>
                    <input type="text" id="nameInputText" name="name" onchange="pathOnchange()"
                           value="{{ old('name') ?? $bewegingsactiviteit->name }}">
                    @if ($bewegingsactiviteit->allVideos()->count() >= 1)
                        <div class="ui tiny bottom attached warning message">
                            <p><i class="icon warning"></i>Op dit moment zijn videos vastgekoppeld aan deze bewegingsactiviteit.
                                Op het moment dat u deze naam verandert verliest u de videos.</p>
                        </div>
                    @endif

                    @error('name')
                        <x-field-error>{{ $message }}</x-field-error>
                    @enderror

                </div>


                <div class="field @error('path')error @enderror">
                    <label>Pad</label>
                <input type="text" name="path" id="pathInputText" value="{{ old('path') ?? $bewegingsactiviteit->path }}">
                @error('path')
                    <x-field-error>{{ $message }}</x-field-error>
                @enderror
            </div>

            <div class="field @error('leerlijn')error @enderror">
                <label>Bewegingsthema</label>
                <div class="ui selection dropdown">

                    <input type="hidden" name="bewegingsthema" value="{{ old('bewegingsthema') ?? $bewegingsactiviteit->parent->path }}">
                    <i class="dropdown icon"></i>
                    <div class="default text">Leerlijn</div>
                    <div class="menu">
                        @foreach($bewegingsthemas as $bewegingsthema)
                            <div class="item" data-value="{{$bewegingsthema->path}}">{{$bewegingsthema->name}}</div>
                        @endforeach
                    </div>
                </div>
                @error('bewegingsthema')
                    <x-field-error>{{ $message }}</x-field-error>
                @enderror

            </div>

            <div class="field" @error('demo')error @enderror style="margin-bottom: 15px;">
                <div class="ui checkbox" id="demo-checkbox">
                    <input type="checkbox" tabindex="0" {{ $bewegingsactiviteit->demo == 1 ? 'checked' : ''}}>
                    <input type="hidden" id="demo-input" name="demo" value="{{old('demo') ?? 0 }}">
                    <label>Demo</label>
                </div>
                @error('demo')
                <div class="ui basic red left pointing prompt label transition visible"
                     style="display: inline-block !important;">{{ $message }}</div>
                @enderror
            </div>

            <div class="field">
                <button class="ui large blue button" class="Edit-button" type="submit" value="submit">Opslaan
                </button>
            </div>


        </form>
    </div>
@endsection

@section('post-content')
    @include('gymwijzer.partials.validation', ['validation' => $validation])

    <script>
        function pathOnchange() {
            var path = document.getElementById("nameInputText").value;
            document.getElementById("pathInputText").value = path.toLowerCase().replace(' ', "-");
        }
    </script>

    <script>
        $('#demo-checkbox')
            .checkbox({
                // check all children
                onChecked: function() {
                    console.log('hello');
                    $('#demo-input').val(1);
                },
                // uncheck all children
                onUnchecked: function() {
                    console.log('hello');

                    $('#demo-input').val(0);
                }
            })
        ;
    </script>
@endsection
