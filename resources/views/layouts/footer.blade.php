<footer id="footer">
    <div id="footer-container">
        <div id="footer-inner" class="wrapper">

            <div id="footer-products" class="footer-group">
                <h3 class="footer-header">Producten</h3>

                <a href="/demo">Demo</a>
                <a href="/gymwijzer">GymWijzer!</a>
                <a href="/volgwijzer">VolgWijzer!</a>
                <div class="divider"></div>
                <a href="https://gymlessenbasisonderwijs.nl/web/de-lesbrieven/">De lesbrieven</a>
                <a href="https://gymlessenbasisonderwijs.nl/web/gymmenindegrotegymzaal/">Gymmen in de Grote Gymzaal</a>
            </div>

            <div id="footer-indepth" class="footer-group">
                <h3 class="footer-header">Verdieping</h3>

                <a href="#">Achtergronden GymWijzer!</a>
                <a href="#">Achtergronden VolgWijzer!</a>
                <div class="divider"></div>
                <a href="{{Route('contactUs')}}">Contact</a>
                <a href="">Algemene voorwaaren</a>

            </div>

            <div id="footer-partners" class="footer-group">
                <h3 class="footer-header">Partners</h3>

                <a href="https://www.deloapp.nl/">De LOapp</a>
                <a href="http://www.reisgidsdigitaalleermateriaal.org/">Reisgids Digitaal Leermateriaal</a>
            </div>
        </div>
    </div>
</footer>
