<section id="mobile-menu-block" style="display:none; transform: translate(-251px)">
    <div id="gymwijzer-logo"></div>
    <nav id="mobile-menu">
        <a class="active" href="{{route('start')}}">Home</a>
        <a href="#">Producten</a>
        <a href="#">Workshops</a>
        <div class="divider"></div>
        @if(Auth::check())
            <a href="/gymwijzer">Start Gymwijzer!</a>
            <a href="{{route('volgwijzer')}}">Start Volgwijzer</a>
            <a href="{{ route('mijnAccount') }}">Mijn account</a>
            <a href="{{ route('logout') }}"
               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Uitloggen</a>
        @else
            <a href="{{route('demo')}}">Demo</a>
            <a href="{{route('login')}}">Inloggen</a>
        @endif

    </nav>
</section>

<header id="header">
    <div id="header-inner" class="wrapper">
        <div id="header-left">
            <a href="{{route('start')}}" id="gymwijzer-logo"></a>

            <nav id="header-left-menu" class="header-menu">
                <a class="active" href="{{route('start')}}">Home</a>
                <a href="#">Producten</a>
                <a href="#">Workshops</a>
            </nav>
        </div>

        <div id="header-right">
            @if(Auth::check())
                <nav id="header-right-menu" class="header-menu">
                    <a href="/gymwijzer">Start Gymwijzer!</a>
                    <a href="{{route('volgwijzer')}}">Start Volgwijzer</a>

                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        Uitloggen
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                    <a href="{{route('mijnAccount')}}">mijn account</a>


                </nav>
            @else
                <nav id="header-right-menu" class="header-menu">
                    <a href="{{route('demo')}}">Demo</a>
                    <a href="{{route('login')}}">Inloggen</a>
                </nav>
            @endif

            <a id="mobile-menu-icon">
                <svg fill="#666" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"
                     ratio="1">
                    <rect y="9" width="20" height="2"></rect>
                    <rect y="3" width="20" height="2"></rect>
                    <rect y="15" width="20" height="2"></rect>
                </svg>
            </a>
        </div>
    </div>

</header>


