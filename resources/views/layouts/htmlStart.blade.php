<!doctype html>
<html lang="nl-NL" prefix="og: http://ogp.me/ns#" @isset ($pageid) id="{{$pageid}}" @endisset
@isset($pageclasses) class="{{$pageclasses}}" @endisset
>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> {{ $title ?? 'GymWijzer!' }}</title>

    <link rel="stylesheet" href="/css/font-awesome.css">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/normalize.css">

    <script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
    <script src="/semantic/dist/semantic.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/semantic/dist/semantic.min.css">
    <link rel="stylesheet" href="/css/gymwijzer/v5.css">

    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Poppins%3A600%2C300%2C%7COpen%20Sans%3A400%2C300%2C400i%2C%7CRoboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto%20Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&subset="/>
    <script src="/js/gymwijzer-layout.js"></script>

    @yield('head')
</head>
<body>
<div id="page-content">
    @include('cookieConsent::index')

    @include('layouts.header')



    @yield('pre-content')

    @yield('content')

</div>

@include('layouts.footer')

{{--//#slava: sometimes you need to inject certain content here (most likely JS)--}}
@yield('post-content')

@include('gymwijzer.partials.notification')
</body>
</html>
