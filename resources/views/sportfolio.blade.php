<!DOCTYPE HTML>
<style>
    table, td, tr{
        border: 1px solid black;
        text-align: center;
        border-collapse: collapse;
        text-overflow: ellipsis;
        word-wrap: break-word;
        white-space: normal;
    }
    table.one{
        max-width: 523px;
        width: 523px;
        position: absolute;
        left: 65px;
    }
    table.two{
        max-width: 523px;
        width: 523px;
        position: absolute;
        left: 65px;
        top: 205px;
    }
    table.child{
        position: absolute;
        left: 65px;
    }
    div.opmerking{
        table-layout: fixed;
        height: 43px;
        overflow: hidden;
        max-width: 131px;
        width: 131px;
        font-size: 8px;
        max-height: 43px;
        padding: 5px;
    }
    div.square{
        table-layout: fixed;
        height: 43px;
        min-height: 43px;
        min-width: 43px;
        width: 43px;
        max-height: 43px;
        max-width: 43px;
    }
    div.one{
        overflow: hidden;
        font-size: 10px;
        max-height: 46px;
        height: 46px;
        width: 86px;
        max-width: 100px;
    }
    div.two{
        vertical-align: middle;
        text-align: center;
        overflow: hidden;
        font-size: 20px;
        max-height: 46px;
        height: 46px;
        width: 37px;
        max-width: 37px;
    }
    div.three{
        vertical-align: middle;
        text-align: center;
        overflow: hidden;
        font-size: 20px;
        max-height: 46px;
        height: 46px;
        width: 51px;
        max-width: 51px;
    }
    div.four{
        vertical-align: middle;
        text-align: center;
        overflow: hidden;
        font-size: 10px;
        max-height: 46px;
        height: 46px;
        width: 74px;
        max-width: 74px;
    }
    div.comment{
        table-layout: fixed;
        height: 49px;
        overflow: hidden;
        max-width: 131px;
        width: 131px;
        font-size: 8px;
        max-height: 49px;
    }
    .page-break{
        page-break-after: always;
    }
</style>
<table class="one">
    <tr style="max-width: 523px;">
        <td style="width: 177px; height: 52px; max-width: 196px; background-color: #d9d9d9"  rowspan="2" colspan="2"><b>Sportfolio</b></td>
        <td style="width: 196px; height: 26px; background-color: #d9d9d9" colspan="2">Sportvereniging</td>
        <td style="width: 65px; height: 26px; background-color: #d9d9d9" colspan="2">Zwemles</td>
        <td style="width: 65px; height: 26px; background-color: #d9d9d9" colspan="2">Zwemdiploma</td>
    </tr>
    <tr>
        <td class="t" style="max-width: 43px; width: 43px;"><div class="one"></div></td>
        <td style="max-width: 43px; width: 43px;"><div class="one"></div></td>
        <td style="max-width: 34px; width: 34px;"><div class="two"></div></td>
        <td style="max-width: 34px; width: 34px;"><div class="two">Ja</div></td>
        <td style="max-width: 48px; width: 48px;"><div class="three"></div></td>
        <td style="max-width: 48px; width: 48px;"><div class="three"></div></td>
    </tr>
    <tr>
        <td style="width: 74px"><div class="four"> </div></td>
        <td style="width: 74px"><div class="four"> </div></td>
        <td style="max-width: 43px; width: 43px;"><div class="one"></div></td>
        <td style="max-width: 43px; width: 43px;"><div class="one"></div></td>
        <td style="max-width: 34px; width: 34px;"><div class="two"></div></td>
        <td style="max-width: 34px; width: 34px;"><div class="two">Nee</div></td>
        <td style="max-width: 48px; width: 48px;"><div class="three"></div></td>
        <td style="max-width: 48px; width: 48px;"><div class="three"></div></td>
    </tr>
    <tr>
        <td style="width: 74px"><div class="four"> </div></td>
        <td style="width: 74px"><div class="four"> </div></td>
        <td style="max-width: 43px; width: 43px"><div class="one" style="font-size: 20px">Naam</div></td>
        <td style="max-width: 43px; width: 43px"><div class="one" style="font-size: 15px"></div></td>
        <td style="max-width: 43px; width: 34px;" colspan="2"><div style="">Groep 8</div></td>
        <td style="max-width: 48px; width: 48px;"><div class="three"></div></td>
        <td style="max-width: 48px; width: 48px;"><div class="three"></div></td>
    </tr>
</table>
<table class="two">
    <tr>
        <td style="background-color:yellow; width: 523px; height: 43px" colspan="7"></td>
    </tr>
    <tr>
        <td style="max-width: 98px; max-height: 26px; width: 98px; height: 26px; table-layout: fixed; background-color: #d9d9d9 " colspan="2">Activiteit</td>
        <td style="max-width:131px; width: 131px; background-color: #d9d9d9" height="26"> </td>
        <td style="width: 65px; height: 26px" rowspan="7"> </td>
        <td style="width: 98px; height: 26px; background-color: #d9d9d9" colspan="2">Activiteit</td>
        <td style="width: 131px; height: 26px; background-color: #d9d9d9"> </td>
    </tr>
    <tr>
        <td style="background-color:#ff00ff;"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div></td>
        <td style="background-color:#ff00ff;"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div></td>
    </tr>
    <tr>
        <td style="background-color: #00ff00"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
        <td style="background-color: #00ff00"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
    </tr>
    <tr>
        <td style="background-color: #0000ff"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
        <td style="background-color: #0000ff"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
    </tr>
    <tr>
        <td style="background-color: #ff6600"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
        <td style="background-color: #ff6600"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
    </tr>
    <tr>
        <td style="background-color: #ff3300"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
        <td style="background-color: #ff3300"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
    </tr>
    <tr>
        <td style="background-color: #e60000"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
        <td style="background-color: #e60000"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
    </tr>

    <tr>
        <td style="width: 523px; height: 43px; background-color: white" colspan="7"></td>
    </tr>
    <tr>
        <td style="max-width: 98px; max-height: 26px; width: 98px; height: 26px; table-layout: fixed; background-color: #d9d9d9 " colspan="2">Activiteit</td>
        <td style="max-width:131px; width: 131px; background-color: #d9d9d9" height="26"> </td>
        <td style="width: 65px; height: 26px" rowspan="7"> </td>
        <td style="width: 98px; height: 26px; background-color: #d9d9d9" colspan="2">Activiteit</td>
        <td style="width: 131px; height: 26px; background-color: #d9d9d9"> </td>
    </tr>
    <tr>
        <td style="background-color:#ff00ff;"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div></td>
        <td style="background-color:#ff00ff;"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div></td>
    </tr>
    <tr>
        <td style="background-color: #00ff00"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
        <td style="background-color: #00ff00"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
    </tr>
    <tr>
        <td style="background-color: #0000ff"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
        <td style="background-color: #0000ff"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
    </tr>
    <tr>
        <td style="background-color: #ff6600"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
        <td style="background-color: #ff6600"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
    </tr>
    <tr>
        <td style="background-color: #ff3300"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
        <td style="background-color: #ff3300"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
    </tr>
    <tr>
        <td style="background-color: #e60000"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
        <td style="background-color: #e60000"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
    </tr>
</table>
<div class="page-break"></div>
<table class="two">
    <tr>
        <td style="background-color:yellow; width: 523px; height: 43px" colspan="7"></td>
    </tr>
    <tr>
        <td style="max-width: 98px; max-height: 26px; width: 98px; height: 26px; table-layout: fixed; background-color: #d9d9d9 " colspan="2">Activiteit</td>
        <td style="max-width:131px; width: 131px; background-color: #d9d9d9" height="26"> </td>
        <td style="width: 65px; height: 26px" rowspan="7"> </td>
        <td style="width: 98px; height: 26px; background-color: #d9d9d9" colspan="2">Activiteit</td>
        <td style="width: 131px; height: 26px; background-color: #d9d9d9"> </td>
    </tr>
    <tr>
        <td style="background-color:#ff00ff;"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div></td>
        <td style="background-color:#ff00ff;"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div></td>
    </tr>
    <tr>
        <td style="background-color: #00ff00"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
        <td style="background-color: #00ff00"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
    </tr>
    <tr>
        <td style="background-color: #0000ff"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
        <td style="background-color: #0000ff"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
    </tr>
    <tr>
        <td style="background-color: #ff6600"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
        <td style="background-color: #ff6600"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
    </tr>
    <tr>
        <td style="background-color: #ff3300"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
        <td style="background-color: #ff3300"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
    </tr>
    <tr>
        <td style="background-color: #e60000"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
        <td style="background-color: #e60000"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
    </tr>

    <tr>
        <td style="width: 523px; height: 43px; background-color: white" colspan="7"></td>
    </tr>
    <tr>
        <td style="max-width: 98px; max-height: 26px; width: 98px; height: 26px; table-layout: fixed; background-color: #d9d9d9 " colspan="2">Activiteit</td>
        <td style="max-width:131px; width: 131px; background-color: #d9d9d9" height="26"> </td>
        <td style="width: 65px; height: 26px" rowspan="7"> </td>
        <td style="width: 98px; height: 26px; background-color: #d9d9d9" colspan="2">Activiteit</td>
        <td style="width: 131px; height: 26px; background-color: #d9d9d9"> </td>
    </tr>
    <tr>
        <td style="background-color:#ff00ff;"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div></td>
        <td style="background-color:#ff00ff;"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div></td>
    </tr>
    <tr>
        <td style="background-color: #00ff00"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
        <td style="background-color: #00ff00"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
    </tr>
    <tr>
        <td style="background-color: #0000ff"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
        <td style="background-color: #0000ff"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
    </tr>
    <tr>
        <td style="background-color: #ff6600"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
        <td style="background-color: #ff6600"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
    </tr>
    <tr>
        <td style="background-color: #ff3300"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
        <td style="background-color: #ff3300"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
    </tr>
    <tr>
        <td style="background-color: #e60000"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
        <td style="background-color: #e60000"> <div class="square"> </div></td>
        <td style="border: 2px solid black"> <div class="square"> </div></td>
        <td><div class="opmerking"> </div> </td>
    </tr>
</table>
<div class="page-break"></div>
<table class="child" style="max-width: 522px; width: 523px;">
    <tr>
        <td style="width: 355px; height: 56px;" colspan="7">Hoe zie ik mijzelf in de gymles</td>
        <td style="width: 145px; height: 56px;">Actie gymles</td>
    </tr>
    <tr>
        <td style="width: 100px; height: 46px;"> </td>
        <td style="border: 2px solid lime" ></td>
        <td style="background-color: lime; border: 2px solid lime">Ja</td>
        <td style="border: 2px solid yellow"></td>
        <td style="background-color: yellow; border: 2px solid yellow"> </td>
        <td style="border: 2px solid red"></td>
        <td style="border: 2px solid red; background-color: red"> </td>
        <td style="width: 131px; height: 46px; max-width: 131px"><div class="comment"> </div></td>
    </tr>
    <tr>
        <td style="width: 100px; height: 46px;"> </td>
        <td style="border: 2px solid lime" ></td>
        <td style="background-color: lime; border: 2px solid lime">Ja</td>
        <td style="border: 2px solid yellow"></td>
        <td style="background-color: yellow; border: 2px solid yellow"> </td>
        <td style="border: 2px solid red"></td>
        <td style="border: 2px solid red; background-color: red"> </td>
        <td><div class="comment"> </div></td>
    </tr>
    <tr>
        <td style="width: 100px; height: 46px;"> </td>
        <td style="border: 2px solid lime" ></td>
        <td style="background-color: lime; border: 2px solid lime">Ja</td>
        <td style="border: 2px solid yellow"></td>
        <td style="background-color: yellow; border: 2px solid yellow"> </td>
        <td style="border: 2px solid red"></td>
        <td style="border: 2px solid red; background-color: red"> </td>
        <td><div class="comment"> </div></td>
    </tr>
    <tr>
        <td style="width: 100px; height: 46px;"> </td>
        <td style="border: 2px solid lime" ></td>
        <td style="background-color: lime; border: 2px solid lime">Ja</td>
        <td style="border: 2px solid yellow"></td>
        <td style="background-color: yellow; border: 2px solid yellow"> </td>
        <td style="border: 2px solid red"></td>
        <td style="border: 2px solid red; background-color: red"> </td>
        <td><div class="comment"> </div></td>
    </tr>
    <tr>
        <td style="width: 370px; height: 56px;" colspan="7">Hoe ziet de gym meester/juf mij in de gymles</td>
        <td style="width: 140px; height: 56px;">Opmerkingen</td>
    </tr>
    <tr>
        <td style="width: 100px; height: 46px;"> </td>
        <td style="border: 2px solid lime" ></td>
        <td style="background-color: lime; border: 2px solid lime">Ja</td>
        <td style="border: 2px solid yellow"></td>
        <td style="background-color: yellow; border: 2px solid yellow"> </td>
        <td style="border: 2px solid red"></td>
        <td style="border: 2px solid red; background-color: red"> </td>
        <td><div class="comment"> </div></td>
    </tr>
    <tr>
        <td style="width: 100px; height: 46px;"> </td>
        <td style="border: 2px solid lime" ></td>
        <td style="background-color: lime; border: 2px solid lime">Ja</td>
        <td style="border: 2px solid yellow"></td>
        <td style="background-color: yellow; border: 2px solid yellow"> </td>
        <td style="border: 2px solid red"></td>
        <td style="border: 2px solid red; background-color: red"> </td>
        <td><div class="comment"> </div></td>
    </tr>
    <tr>
        <td style="width: 100px; height: 46px;"> </td>
        <td style="border: 2px solid lime" ></td>
        <td style="background-color: lime; border: 2px solid lime">Ja</td>
        <td style="border: 2px solid yellow"></td>
        <td style="background-color: yellow; border: 2px solid yellow"> </td>
        <td style="border: 2px solid red"></td>
        <td style="border: 2px solid red; background-color: red"> </td>
        <td><div class="comment"> </div></td>
    </tr>
    <tr>
        <td style="width: 100px; height: 46px;"> </td>
        <td style="border: 2px solid lime" ></td>
        <td style="background-color: lime; border: 2px solid lime">Ja</td>
        <td style="border: 2px solid yellow"></td>
        <td style="background-color: yellow; border: 2px solid yellow"> </td>
        <td style="border: 2px solid red"></td>
        <td style="border: 2px solid red; background-color: red"> </td>
        <td><div class="comment"> </div></td>
    </tr>
</table>
