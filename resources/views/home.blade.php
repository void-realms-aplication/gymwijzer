@extends('gymwijzer.htmlStart')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">

                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                    </div>
                    <div>
                        <li>
                            <a href="/mijn-account/mijn-gegevens">Mijn Gegevens</a>
                        </li>
                    </div>
                        <div>
                        @can('bekijk_klas')
                            <li>
                                <a href="/mijn-account/mijn-klassen">Mijn Klassen</a>
                            </li>
                        @endcan
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection
