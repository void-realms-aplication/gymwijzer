@extends('layouts.htmlStart')

@section('content')
    @include('gymwijzer.partials.breadcrumbs', ['heading' => 'GymWijzer! - Contact',
       'backurl' => route('start'),
       'crumbs' => [['name' => 'GymWijzer!', 'url' => ''], ['name' => 'GymWijzer! - Cookies', 'url' => '/cookies']]
   ])

    <div class="wrapper">
        <h1>Wat is een cookie?</h1>
        <p>
            Wij maken op deze website gebruik van cookies. Een cookie is een klein eenvoudig bestandje dat tijdens uw
            bezoek naar u verzonden wordt. De cookies worden door uw browser opgaslagen op de harde schijf van uw
            computer. De cookies worden iedere keer dat u een nieuwe pagina opent terug gestuurd naar onze server, dit
            is nodig om de website goed te laten functioneren.
        </p>
        <p>
            Sommige websites gebruiken cookies om te volgen waar u op het internet bent geweest, vaak is dit om later
            gerichte reclame naar u toe te sturen. Wij doen dit echter niet, wij gebruiken cookies alleen voor een goede
            werking van onze website. Zonder cookies zou u bijvoorbeeld niet kunnen inloggen of een abonnement
            afsluiten. Omwille van de veiligheid en privacy van onze gebruikers vertellen we aan uw browser dat onze
            cookies maar 2 uur geldig zijn, daarna worden ze automatisch verwijderd. De cookie die aangeeft dat u op
            onze melding heeft geklikt blijft een stukje langer geldig, namelijk 30 dagen, zo krijgt u niet iedere keer
            onze melding.
        </p>
        <p>
            Volgends de Nederlandse en Europese wet is het verplicht een melding te laten zien als er cookies geplaats
            worden. Voor cookies die niet perse noodzakelijk zijn voor de werking van de website is het zelfs verplicht
            van te voren toestemming te vragen. Zoals eerder genoemd staat gebruiken wij allen noodzakelijke cookies,
            daarom hebben we stiekem al een cookie geplaatst voordat u op onze melding klikte.
        </p>
        <p>
            Wilt u ondanks deze informatie toch nog meer weten, dan kunt u kijken op de
            <a href="https://nl.wikipedia.org/wiki/Cookie_(internet)"> Wikipedia pagina over cookies.</a>
            Heeft u een persoonlijke vraag dan kunt u ons altijd mailen op <a href="mailto: info@gymwijzer.nl">info@gymwijzer.nl</a>
        </p>
    </div>

@endsection
