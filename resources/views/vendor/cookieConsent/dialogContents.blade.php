<div class="js-cookie-consent cookie-consent">

    <span class="cookie-consent__message">
        Deze site gebruikt cookies. &nbsp;
    <button class="js-cookie-consent-agree cookie-consent__agree cookie-button">
        Prima
    </button>
    <a href="/cookies">
        <button class="cookie-button">
            Meer info
        </button>
    </a>
</span>

</div>
