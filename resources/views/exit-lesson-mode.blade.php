@extends('layouts.htmlStart', ['title' => 'Bevestig uw identiteit alstufblieft'])

@section('content')

    @include('gymwijzer.partials.breadcrumbs', ['heading' => 'Identiteit bevestigen',
        'crumbs' => [['name' => 'GymWijzer!', 'url' => route('start')],
     ['name' => 'Identiteit bevestigen', 'url' => url()->current()]
        ]
    ])

    <div class="wrapper" style="max-width: 786px;">
        <h3>Vul alstublieft uw wachtwoord om door te gaan!</h3>
        <form method="POST" action="{{ route('exitLessonMode') }}" class="ui form">
            @csrf

            <div class="field @error('password')error @enderror">
                <label>Uw wachtwoord</label>
                <input type="password" name="password">

                @error('password')
                    <x-field-error>{{ $message }}</x-field-error>
                @enderror

            </div>

            <div class="field">
                <button class="ui fluid large positive button" type="submit">Doorgaan</button>
            </div>


        </form>
    </div>

@endsection

@section('post-content')
    <script>
        $('.ui.form')
            .form({
                inline: true,
                fields : {
                    password: {
                        rules: [
                            {
                                type: 'empty',
                                prompt: 'Vul alstublieft een wachtwoord in.'
                            }                                                                 ]

                    }
                }
            });
    </script>
@endsection
