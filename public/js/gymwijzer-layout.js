window.addEventListener("load", function() {
    let mobileMenuIcon = document.getElementById("mobile-menu-icon");
    let mobileMenu = document.getElementById("mobile-menu-block")
    mobileMenuIcon.addEventListener("click", function () {
        toggleMenu(mobileMenu);
    })

    window.addEventListener("resize", function() {
        let browserWidth = document.documentElement.clientWidth;
        if(browserWidth > 954 && mobileMenu.style.display === "block") {
            hideMenu(mobileMenu)
        };
    });
});

function showMenu(element) {
    element.style.display = "block";
    setTimeout(function() {
        element.style.transform = "translate(0)";
    }, 1)
}

function hideMenu(element) {
    element.style.transform = "translate(-251px)";
    setTimeout(function() {
        element.style.display = "none";
    }, 400)
}

function toggleMenu(element) {
    if (element.style.display === "none") {
       showMenu(element)
    } else {
        hideMenu(element)
    }
}

