// $('.item[data-config=1]').click(function () {
//     console.log("hey");
// });

$(document).ready(function () {
    $('.menu .item').click(function () {
        var configuration = $(this).attr('data-config');
        hideConfigs();
        $(this).addClass('active');
        $('#configurtaions-info .content[data-config-info="' + configuration + '"]').addClass('active');
        $('[name="config_id"]').val(configuration);
    });
});


function hideConfigs() {
    $('#configurtaions-info .content').each(function (i, item) {
        $(item).removeClass('active');
    })

    $('.menu .item').each(function (i, item) {
        $(item).removeClass('active')
    })
}

function selectPeriod() {
    console.log("hello");

    $('body')
        .toast({
            message: 'Weet u zeker dat u de aantal rapport afnames voor dit schooljaar wilt opslaan? <strong>Deze actie kan alleen ongedaan worden door de administrator!</strong>',
            displayTime: 80000,
            classActions: 'warning left',
            title: 'Bevestig uw keuze!',
            actions: [
                {
                    text: 'Bevestigen',
                    class: 'green',
                    click: function () {
                        document.getElementById('periods-submit').submit()
                    }
                },
                {
                    text: 'Annuleren',
                    class: 'red',
                }
            ]
        })
    ;

}
