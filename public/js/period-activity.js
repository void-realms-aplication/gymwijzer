/*
    Attempts to periods front-end fanciness v0.1
    TODO:
    -   Go through the code again and figure out wtf have I done
    -   cleanup
 */

var label = '<div class="ui small horizontal red label">Al ingedeeld!</div>';
var constructIcon = '<i class="tiny white wrench icon"></i>';
var construction = {};
$(document).ready(function () {

    $('.menu .item').click(function () {
        var period = $(this).attr('data-period');
        var activity = $(this).attr('data-value')
        // console.log(`Activity ${activity} has just been clicked for period ${period}`)
        setTimeout(setListnersDelete, 30);
        var listitems = $(`.select-activities div.item[data-value="${activity}"]`)
        if(usedActivities.includes(parseInt(activity))) {
            $('body')
                .toast({
                    message: `U heeft zojuist de bewegingsactiviteit <strong>${$(this).attr('data-name')}</strong> over meerdere periodes ingesteld!`,
                    class: 'warning',
                    displayTime: '5000',
                    showIcon: 'exclamation'
                })
            ;
            setTimeout(function() {
                $(`a.ui.label[data-value="${activity}"]`).addClass('doubled');
            },150)
        } else {
            listitems.addClass('alreadyselected');
            listitems.append(label);
        }

        if($(`.item.period[data-period="${period}"] a.label.visible`).length == recommended_max_activities) {
            $('body')
                .toast({
                    message: `U heeft zojuist meer dan ${recommended_max_activities} activiteiten ingesteld voor periode <strong>${period}</strong>, dit wordt niet aangeraden!`,
                    class: 'warning',
                    displayTime: '5000',
                    showIcon: 'exclamation'
                })
            ;
        }
        usedActivities.push(parseInt(activity));

        setConstruction(period);
    });

    setListnersDelete();
    // console.log(usedActivities);


});

function setListnersDelete() {
    $('.delete.icon').off();
    $('.delete.icon').click(function () {
        var activity = $(this).parent().attr('data-value');
        var period = $(this).parent().attr('data-period');
        var name = $(`.select-activities .item[data-value="${activity}"]`).first().attr('data-name');
        const index = usedActivities.indexOf(parseInt(activity));
        if (index > -1) {
            usedActivities.splice(index, 1);
        }
        if(!usedActivities.includes(parseInt(activity))) {
            setTimeout(function() {
                $(`.select-activities .item[data-value="${activity}"]`).removeClass('alreadyselected');
                $(`.select-activities .item[data-value="${activity}"]`).html(name);
            }, 150);

        }

        if(getOccurrence(usedActivities, activity) < 2) {
            $(`a.ui.label[data-value="${activity}"]`).removeClass('doubled');

        }

        setConstruction(period);
    })
}

function getOccurrence(array, value) {
    return array.filter(x => x == value).length;
}

//When a period is being edited display certain feedback about that
function setConstruction(period) {
    var editNotification = '<span class="edit-notification">Deze periode wordt nu aangepast</span>'

    if(!construction[period]) {
        $(`.period-number[data-period="${period}"]`).prepend(constructIcon);
        $(`.item.period[data-period=${period}] .select-activities`).append(editNotification)
        construction[period] = true;
    }

}
