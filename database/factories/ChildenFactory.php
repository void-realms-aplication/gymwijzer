<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Child;
use App\Model;
use Faker\Generator as Faker;

$factory->define(Child::class, function (Faker $faker) {

    return [
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'date_of_birth' => $faker->date(),
        'password' => $faker->password,
        'schoolclass_id' => random_int(1,10),
        'school_id' => 1
    ];
});
