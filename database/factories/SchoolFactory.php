<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\School;
use Faker\Generator as Faker;

$factory->define(School::class, function (Faker $faker) {
    return [
        'name' => $faker->city . ' - School',
        'email' => $faker->email,
        'phone_number' => $faker->phoneNumber,
        'brin_number' => random_int(100,999),
        'country' => 'Nederland',
        'city' => $faker->city,
        'street' => $faker->streetName,
        'house_number' => random_int(1, 200),
        'postal_code' => $faker->postcode,
        'number_of_students' => random_int(0, 600),
    ];
});
