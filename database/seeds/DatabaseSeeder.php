<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $this->call(LeerLijnSeeder::class);
        $this->call(BewegingsthemaSeeder::class);
        $this->call(BewegingsactiviteitSeeder::class);
        $this->call(SubscriptionsSeeder::class);
        $this->call(AbilitySeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(SchoolSeeder::class);
        $this->call(SchoolClassSeeder::class);
        $this->call(ChildSeeder::class);
        $this->call(KijkwijzerSeeder::class);
        $this->call(TestSchoolSeeder::class);
        $this->call(PeriodSeerder::class);



    }


}

