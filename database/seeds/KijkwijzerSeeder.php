<?php

use Illuminate\Database\Seeder;

class KijkwijzerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kijkwijzers')->insert([[
            'name' => 'Over de kop gaan kijkwijzer',
            'description' => 'Koprolletjes, en je nek niet breken!',
            'path' => 'https://gymwijzer.nl/wp-content/uploads/securepdfs/Kijkwijzer-glijden-klaar-voor-nieuwe-website-20181101.pdf',
            'bewegingsactiviteit_id' => 4
        ],[
            'name' => 'Jongeleren kijkwijzer',
            'description' => 'Jongereleren met balletjes',
            'path' => 'https://gymwijzer.nl/wp-content/uploads/securepdfs/Kijkwijzer-zwaaien-in-de-ringen-klaar-voor-de-nieuwe-website-20181101.pdf',
            'bewegingsactiviteit_id' => 8
        ],[
            'name' => 'Starten kijkwijzer',
            'description' => 'Wild hardlopen',
            'path' => 'https://gymwijzer.nl/wp-content/uploads/securepdfs/Kijkwijzer-bergbeklimmen-klaar-voor-nieuwe-website-20181101.pdf',
            'bewegingsactiviteit_id' => 6
        ]]);
    }
}
 //https://gymwijzer.nl/wp-content/uploads/Kijkje_in_de_gymzaal_hvdm.pdf
