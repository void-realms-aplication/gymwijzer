<?php

use Illuminate\Database\Seeder;
use App\Leerlijn;

class LeerLijnSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
            $pages = [
                [1, "index", NULL, "" , NULL, NULL, NULL, 0, 1, NULL, NULL],
                [2, "Balanceren","", "Balanceren", 1, NULL, NULL, 0, 1, NULL, NULL],
                [3, "Klimmen","", "Klimmen", 2, NULL, NULL, 0, 0,NULL, NULL],
                [4, "Zwaaien","", "Zwaaien", 3, NULL, NULL, 0, 0,NULL, NULL],
                [5, "Over de kop gaan","", "Over-de-kop-gaan", 4, NULL, NULL, 0, 0, NULL, NULL],
                [6, "Springen","", "Springen", 5, NULL, NULL, 0, 0, NULL, NULL],
                [7, "Hardlopen","", "Hardlopen", 6, NULL, NULL, 0, 0, NULL, NULL],
                [8, "Mikken","", "Mikken", 7, NULL, NULL, 0, 0, NULL, NULL],
                [9, "Jongleren","", "Jongleren", 8, NULL, NULL, 0, 0, NULL, NULL],
                [10, "Doelspelen","", "Doelspelen", 9, NULL, NULL, 0, 0, NULL, NULL],
                [11, "Tikspelen","", "Tikspelen", 10, NULL, NULL, 0, 0, NULL, NULL],
                [12, "Stoeispelen","", "Stoeispelen", 11, NULL, NULL, 0, 0, NULL, NULL],
                [13, "Bewegen op Muziek","","Bewegen-op-Muziek", 12, NULL, NULL, 0, 0, NULL, NULL],
                [14, "Acrobatiek", "Balanceren", "Balanceren/Acrobatiek", NULL, 1, NULL, 0, 1, NULL, NULL],
                [15, "Balanceren", "Balanceren", "Balanceren/Balanceren", NULL, 2, NULL, 0, 1, NULL, NULL],
                [16, "Glijden", "Balanceren", "Balanceren/glijden", NULL, 3, NULL, 0, 0, NULL, NULL],
                [17, "Klimmen", "Klimmen", "Klimmen/Klimmen", NULL, 4, NULL, 0, 0, NULL, NULL],
                [18, "Klauteren", "Klimmen", "Klimmen/Klauteren", NULL, 5, NULL, 0, 0, NULL, NULL],
                [19, "Hangendzwaaien", "Zwaaien", "Zwaaien/Hangendzwaaien", NULL, 6, NULL, 0, 0, NULL, NULL],
                [20, "Steunzwaaien", "Zwaaien", "Zwaaien/Steunzwaaien", NULL, 7, NULL, 0, 0, NULL, NULL],
                [21, "Schommelen", "Zwaaien", "Zwaaien/Schommelen", NULL, 8, NULL, 0, 0, NULL, NULL],
                [22, "Over de kop gaan", "Over-de-kop-gaan", "Over-de-kop-gaan/Over-de-kop-gaan", NULL, 9, NULL, 0, 0, NULL, NULL],
                [23, "Duikelen", "Over-de-kop-gaan", "Over-de-kop-gaan/Duikelen", NULL, 10, NULL, 0, 0, NULL, NULL],
                [24, "Freerunnen", "Springen", "Springen/Freerunnen", NULL, 11, NULL, 0, 0, NULL, NULL],
                [25, "Hoogspringen", "Springen", "Springen/Hoogspringen", NULL, 12, NULL, 0, 0, NULL, NULL],
                [26, "Verspringen", "Springen", "Springen/Verspringen", NULL, 13, NULL, 0, 0, NULL, NULL],
                [27, "Steunspringen", "Springen", "Springen/Steunspringen", NULL, 14, NULL, 0, 0, NULL, NULL],
                [28, "Trampolinespringen", "Springen", "Springen/Trampolinespringen", NULL, 15, NULL, 0, 0, NULL, NULL],
                [29, "Touwtjespringen", "Springen", "Springen/Touwtjespringen", NULL, 16, NULL, 0, 0, NULL, NULL],
                [30, "Estafette", "Hardlopen", "Hardlopen/Estafette", NULL, 17, NULL, 0, 0, NULL, NULL],
                [31, "Starten", "Hardlopen", "Hardlopen/Starten", NULL, 18, NULL, 0, 0, NULL, NULL],
                [32, "Shuttlerun test", "Hardlopen", "Hardlopen/Shuttlerun-test", NULL, 19, NULL, 0, 0, NULL, NULL],
                [33, "Wegspelen", "Mikken", "Mikken/Wegspelen", NULL, 20, NULL, 0, 0, NULL, NULL],
                [34, "Mikken", "Mikken", "Mikken/Mikken", NULL, 21, NULL, 0, 0, NULL, NULL],
                [35, "Retourneren", "Jongleren", "Jongleren/Retourneren",   NULL, 22, NULL, 0, 0, NULL, NULL],
                [36, "Soleren", "Jongleren", "Jongleren/Soleren", NULL, 23, NULL, 0, 0, NULL, NULL],
                [37, "Werpen en vangen", "Jongleren", "Jongleren/Werpen-en-vangen", NULL, 24, NULL, 0, 0, NULL, NULL],
                [38, "Keepersspelen", "Doelspelen", "Doelspelen/Keepersspelen", NULL, 25, NULL, 0, 0, NULL, NULL],
                [39, "Lummelspelen", "Doelspelen", "Doelspelen/Lummelspelen", NULL, 26, NULL, 0, 0, NULL, NULL],
                [40, "Wegspelen", "Doelspelen", "Doelspelen/Wegspelen", NULL, 27, NULL, 0, 0, NULL, NULL],
                [41, "Honkloopspelen", "Tikspelen", "Tikspelen/Honkloopspelen", NULL, 28, NULL, 0, 0, NULL, NULL],
                [42, "Tikspelen", "Tikspelen", "Tikspelen/Tikspelen", NULL, 29, NULL, 0, 0, NULL, NULL],
                [43, "Stoeispelen", "Stoeispelen", "Stoeispelen/Stoeispelen", NULL, 30, NULL, 0, 0, NULL, NULL],
                [47, "Acrobatiek", "Balanceren/Acrobatiek", "Balanceren/Acrobatiek/Acrobatiek", NULL, NULL, 1, 1, 1, NULL, NULL],
                [48, "Handstand", "Balanceren/Acrobatiek", "Balanceren/Acrobatiek/Handstand", NULL, NULL, 2, 1, 1, NULL, NULL],
                [49, "Lopen over smal vlak", "Balanceren/Balanceren", "Balanceren/Balanceren/Lopen-over-smal-vlak", NULL, NULL, 3, 1, 1, NULL, NULL],
                [50, "Glijden", "Balanceren/Glijden", "Balanceren/Glijden/Glijden", NULL, NULL, 4, 1, 0, NULL, NULL],
                [51, "Verplaatsen in touwen", "Klimmen/Klimmen", "Klimmen/Klimmen/Verplaatsen-in-touwen", NULL, NULL, 5, 1, 0, NULL, NULL],
                [52, "Bergbeklimmen", "Klimmen/Klauteren", "Klimmen/Klauteren/Bergbeklimmen", NULL, NULL, 6, 1, 0, NULL, NULL],
                [53, "Touwzwaaien", "Zwaaien/Hangendzwaaien", "Zwaaien/Hangendzwaaien/Touwzwaaien", NULL, NULL, 7, 1, 0, NULL, NULL],
                [54, "Ringzwaaien", "Zwaaien/Hangendzwaaien", "Zwaaien/Hangendzwaaien/Ringzwaaien", NULL, NULL, 8, 1, 0, NULL, NULL],
                [55, "Trapeze steunzwaaien", "Zwaaien/Steunzwaaien", "Zwaaien/Steunzwaaien/Trapeze-steunzwaaien", NULL, NULL, 9, 1, 0, NULL, NULL],
                [56, "Klokkenluiden", "Zwaaien/Schommelen", "Zwaaien/Schommelen/Klokkenluiden", NULL, NULL, 10, 1, 0, NULL, NULL],
                [57, "Schommelen in de ringen", "Zwaaien/Schommelen", "Zwaaien/Schommelen/Schommelen-in-de-ringen", NULL, NULL, 11, 1, 0, NULL, NULL],
                [58, "Koprol op recht vlak", "Over-de-kop-gaan/Over-de-kop-gaan", "Over-de-kop-gaan/Over-de-kop-gaan/Koprol-op-recht-vlak", NULL, NULL, 12, 1, 0, NULL, NULL],
                [59, "Koprol op schuin vlak", "Over-de-kop-gaan/Over-de-kop-gaan", "Over-de-kop-gaan/Over-de-kop-gaan/Koprol-op-schuin-vlak", NULL, NULL, 13, 1, 0, NULL, NULL],
                [60, "Duikelen aan brug en rekstok", "Over-de-kop-gaan/Duikelen", "Over-de-kop-gaan/Duikelen/Duikelen-aan-brug-en-rekstok", NULL, NULL, 14, 1, 0, NULL, NULL],
                [61, "Bayo flip", "Springen/Freerunnen", "Springen/Freerunnen/Bayoflip", NULL, NULL, 15, 1, 0, NULL, NULL],
                [62, "Cat jump", "Springen/Freerunnen", "Springen/Freerunnen/Cat jump", NULL, NULL, 16, 1, 0, NULL, NULL],
                [63, "Roof to roof", "Springen/Freerunnen", "Springen/Freerunnen/Roof-to-roof", NULL, NULL, 17, 1, 0, NULL, NULL],
                [64, "Running up", "Springen/Freerunnen", "Springen/Freerunnen/Running-up", NULL, NULL, 18, 1, 0, NULL, NULL],
                [65, "Speed vault", "Springen/Freerunnen", "Springen/Freerunnen/Speed-vault", NULL, NULL, 19, 1, 0, NULL, NULL],
                [66, "Wall steps", "Springen/Freerunnen", "pringen/Freerunnen/Wall-step", NULL, NULL, 20, 1, 0, NULL, NULL],
                [67, "Hoogspringen", "Springen/Hoogspringen", "Springen/Hoogspringen/Hoogspringen", NULL, NULL, 21, 1, 0, NULL, NULL],
                [68, "Verspringen", "Springen/Verspringen", "Springen/Verspringen/Verspringen", NULL, NULL, 22, 1, 0, NULL, NULL],
                [69, "Boksprong", "Springen/Steunspringen", "Springen/Steunspringen/Boksprong", NULL, NULL, 23, 1, 0, NULL, NULL],
                [70, "Hurksprong", "Springen/Steunspringen", "Springen/Steunspringen/Hurksprong", NULL, NULL, 24, 1, 0, NULL, NULL],
                [71, "hurkwendsprong", "Springen/Steunspringen", "Springen/Steunspringen/hurkwendsprong", NULL, NULL, 25, 1, 0, NULL, NULL],
                [72, "Trampoline springen", "Springen/Trampolinespringen", "Springen/Trampolinespringen/Trampoline springen", NULL, NULL, 26, 1, 0, NULL, NULL],
                [73, "Kort springtouw", "Springen/Touwtjespringen", "Springen/Touwtjespringen/Korte-springtouw", NULL, NULL, 27, 1, 0, NULL, NULL],
                [74, "Lang springtouw", "Springen/Touwtjespringen", "Springen/Touwtjespringen/Lange-springtouw", NULL, NULL, 28, 1, 0, NULL, NULL],
                [75, "Rijenestafette", "Hardlopen/Estafette", "Hardlopen/Estafette/Rijenestafette ", NULL, NULL, 29, 1, 0, NULL, NULL],
                [76, "starten", "Hardlopen/Starten", "Hardlopen/Starten/starten", NULL, NULL, 30, 1, 0, NULL, NULL],
                [77, "Shuttlerun test", "Hardlopen/Shuttlerun-test", "Hardlopen/Shuttlerun-test/Shuttlerun-test", NULL, NULL, 31, 1, 0, NULL, NULL],
                [78, "Slaan van tennisbal", "Mikken/Wegspelen", "Mikken/Wegspelen/Slaan-van-tennisbal", NULL, NULL, 32, 1, 0, NULL, NULL],
                [79, "Handbal- en voedbalmikken", "Mikken/Mikken", "Mikken/Mikken/Handbal-voedbalmikken", NULL, NULL, 33, 1, 0, NULL, NULL],
                [80, "Basketbal: lay-up en Set-shot", "Mikken/Mikken", "Mikken/Mikken/Basketbal:-lay-up-en-Set-shot", NULL, NULL, 34, 1, 0, NULL, NULL],
                [81, "Badminton", "Jongleren/Retourneren", "Jongleren/Retourneren/Badminton", NULL, NULL, 35, 1, 0, NULL, NULL],
                [82, "tafeltennis", "Jongleren/Retourneren", "Jongleren/Retourneren/tafeltennis", NULL, NULL, 36, 1, 0, NULL, NULL],
                [83, "Jongleren met ballen en doekjes", "Jongleren/Soleren", "Jongleren/Soleren/Jongleren-met-ballen-en-doekjes", NULL, NULL, 37, 1, 0, NULL, NULL],
                [84, "Dribbelen met basketbal", "Jongleren/Soleren", "Jongleren/Soleren/Dribbelen-met-basketbal", NULL, NULL, 38, 1, 0, NULL, NULL],
                [85, "Dribbelen met voetbal", "Jongleren/Soleren", "Jongleren/Soleren/Dribbelen-met-voetbal", NULL, NULL, 39, 1, 0, NULL, NULL],
                [86, "Dribbelen met hockey", "Jongleren/Soleren", "Jongleren/Soleren/Dribbelen-met-hockey", NULL, NULL, 40, 1, 0, NULL, NULL],
                [87, "Kaatsenballen", "Jongleren/Werpen-en-vangen", "Jongleren/Werpen-en-vangen/Kaatsenballen", NULL, NULL, 41, 1, 0, NULL, NULL],
                [88, "Lijnbal", "Doelspelen/Keepersspelen", "Doelspelen/Keepersspelen/Lijnbal", NULL, NULL, 42, 1, 0, NULL, NULL],
                [89, "Lummelen", "Doelspelen/Lummelspelen", "Doelspelen/Lummelspelen/Lummelen", NULL, NULL, 43, 1, 0, NULL, NULL],
                [90, "Hockey", "Doelspelen/Wegspelen", "Doelspelen/Wegspelen/Hockey", NULL, NULL, 44, 1, 0, NULL, NULL],
                [91, "Slagbal en Beeball", "Tikspelen/Honkloopspelen", "Tikspelen/Honkloopspelen/Slagbal-en-Beeball", NULL, NULL, 45, 1, 0, NULL, NULL],
                [92, "De Chinese muur", "Tikspelen/Tikspelen", "Tikspelen/Tikspelen/De-Chinese-muur", NULL, NULL, 46, 1, 0, NULL, NULL],
                [93, "Kantelen", "Stoeispelen/stoeispelen", "Stoeispelen/Kantelen", NULL, NULL, 47, 1, 0, NULL, NULL],
                [94, "In de maat", "Bewegen-op-Muziek/", "Bewegen-op-Muziek/In-de-maat", NULL, NULL, 48, 1, 0, NULL, NULL],
                [96, "Streetdance", "Bewegen-op-Muziek/", "Bewegen-op-Muziek/streetdance", NULL, NULL, 50, 1, 0, NULL, NULL],
                [97, "Aerobics", "Bewegen-op-Muziek/", "Bewegen-op-Muziek/Aerobics", NULL, NULL, 51, 1, 0, NULL, NULL]
            ];

//            foreach ($pages as $page) {
//                DB::table('leerlijnen')->insert([
//                    'id' => $page[0],
//                    'name' => $page[1],
//                    'parent_path' => $page[2],
//                    'path' => $page[3],
//                    'leerlijnNummer'=> $page[4],
//                    'bewegingsthemaNummer' => $page[5],
//                    'bewegingsactiviteitNummer' => $page[6],
//                    'has_video' => $page[7],
//                    'demo' => $page[8]
//                ]);
//            }

            $new = [
                ['Balanceren', 'balanceren', 0],
                ['Klimmen', 'klimmen', 0],
                ['Doelspelen', 'doelspelen', 0],
                ['Mikken', 'mikken', 0]

            ];

//            foreach ($new as $leerlijn) {
//                DB::table('leerlijnen')->insert([
//                    'name' => $leerlijn[0],
//                    'path' => $leerlijn[1],
//                    'demo' => $leerlijn[2]
//                ]);
//            }

         Leerlijn::create( [
             'name'=>'Balanceren',
             'path'=>'balanceren',
             'demo'=>1
         ] );



         Leerlijn::create( [
             'name'=>'Klimmen',
             'path'=>'klimmen',
             'demo'=>0
         ] );



         Leerlijn::create( [
             'name'=>'Zwaaien',
             'path'=>'zwaaien',
             'demo'=>0
         ] );



         Leerlijn::create( [
             'name'=>'Over de kop gaan',
             'path'=>'over-de-kop-gaan',
             'demo'=>0
         ] );



         Leerlijn::create( [
             'name'=>'Springen',
             'path'=>'springen',
             'demo'=>0
         ] );



         Leerlijn::create( [
             'name'=>'Hardlopen',
             'path'=>'hardlopen',
             'demo'=>0
         ] );



         Leerlijn::create( [
             'name'=>'Mikken',
             'path'=>'mikken',
             'demo'=>0
         ] );



         Leerlijn::create( [
             'name'=>'Jongleren',
             'path'=>'jongleren',
             'demo'=>0
         ] );



         Leerlijn::create( [
             'name'=>'Doelspelen',
             'path'=>'doelspelen',
             'demo'=>0
         ] );



         Leerlijn::create( [
             'name'=>'Tikspelen',
             'path'=>'tikspelen',
             'demo'=>0
         ] );



         Leerlijn::create( [
             'name'=>'Stoeispelen',
             'path'=>'stoeispelen',
             'demo'=>0
         ] );



         Leerlijn::create( [
             'name'=>'Bewegen op Muziek',
             'path'=>'bewegen-op-Muziek',
             'demo'=>0
         ] );

     }
}

