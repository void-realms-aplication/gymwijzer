<?php

use Illuminate\Database\Seeder;
use App\Bewegingsthema;
use App\Bewegingsactiviteit;


class BewegingsactiviteitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $new = [
            ['Acrobatiek', 'acrobatiek', 'acrobatiek', 0],
            ['Handstand', 'handstand', 'acrobatiek', 0],
            ['Handbal- en voedbalmikken', 'handenvoetbal', 'mikken', 0],
            ['Basketbal: lay-up en Set-shot', 'basketbal', 'mikken', 0]
        ];

//        foreach ($new as $bewegingsthema) {
//            DB::table('bewegingsactiviteiten')->insert([
//                'name' => $bewegingsthema[0],
//                'path' => $bewegingsthema[1],
//                'bewesdfsgingsthema_id' => Bewegingsthema::where('path', $bewegingsthema[2])->first()->id,
//                'demo' => $bewegingsthema[3]
//            ]);
//        }

        Bewegingsactiviteit::create( [
            'name'=>'Acrobatiek',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'acrobatiek')->first()->id,
            'path'=>'acrobatiek',
            'demo'=>1
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Handstand',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'acrobatiek')->first()->id,
            'path'=>'handstand',
            'demo'=>1
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Lopen over smal vlak',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'balanceren')->first()->id,
            'path'=>'lopen-over-smal-vlak',
            'demo'=>1
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Glijden',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'glijden')->first()->id,
            'path'=>'glijden',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Verplaatsen in touwen',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'klimmen')->first()->id,
            'path'=>'verplaatsen-in-touwen',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Bergbeklimmen',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'klauteren')->first()->id,
            'path'=>'bergbeklimmen',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Touwzwaaien',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'hangendzwaaien')->first()->id,
            'path'=>'touwzwaaien',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Ringzwaaien',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'hangendzwaaien')->first()->id,
            'path'=>'ringzwaaien',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Trapeze steunzwaaien',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'steunzwaaien')->first()->id,
            'path'=>'trapeze-steunzwaaien',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Klokkenluiden',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'schommelen')->first()->id,
            'path'=>'klokkenluiden',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Schommelen in de ringen',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'schommelen')->first()->id,
            'path'=>'schommelen-in-de-ringen',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Koprol op recht vlak',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'over-de-kop-gaan')->first()->id,
            'path'=>'koprol-op-recht-vlak',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Koprol op schuin vlak',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'over-de-kop-gaan')->first()->id,
            'path'=>'koprol-op-schuin-vlak',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Duikelen aan brug en rekstok',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'duikelen')->first()->id,
            'path'=>'duikelen-aan-brug-en-rekstok',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Bayo flip',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'freerunnen')->first()->id,
            'path'=>'bayoflip',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Cat jump',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'freerunnen')->first()->id,
            'path'=>'cat jump',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Roof to roof',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'freerunnen')->first()->id,
            'path'=>'roof-to-roof',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Running up',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'freerunnen')->first()->id,
            'path'=>'running-up',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Speed vault',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'freerunnen')->first()->id,
            'path'=>'speed-vault',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Wall steps',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'freerunnen')->first()->id,
            'path'=>'wall-step',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Hoogspringen',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'hoogspringen')->first()->id,
            'path'=>'hoogspringen',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Verspringen',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'verspringen')->first()->id,
            'path'=>'verspringen',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Boksprong',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'steunspringen')->first()->id,
            'path'=>'boksprong',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Hurksprong',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'steunspringen')->first()->id,
            'path'=>'hurksprong',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Hurkwendsprong',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'steunspringen')->first()->id,
            'path'=>'hurkwendsprong',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Trampoline springen',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'trampolinespringen')->first()->id,
            'path'=>'trampoline springen',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Kort springtouw',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'touwtjespringen')->first()->id,
            'path'=>'korte-springtouw',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Lang springtouw',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'touwtjespringen')->first()->id,
            'path'=>'lange-springtouw',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Rijenestafette',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'estafette')->first()->id,
            'path'=>'rijenestafette ',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Starten',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'starten')->first()->id,
            'path'=>'starten',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Shuttlerun test',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'shuttlerun-test')->first()->id,
            'path'=>'shuttlerun-test',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Slaan van tennisbal',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'wegspelen-van-tennisbal')->first()->id,
            'path'=>'slaan-van-tennisbal',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Handbal- en voedbalmikken',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'mikken')->first()->id,
            'path'=>'handbal-voedbalmikken',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Basketbal: lay-up en Set-shot',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'mikken')->first()->id,
            'path'=>'basketbal-set-shot',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Badminton',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'retourneren')->first()->id,
            'path'=>'badminton',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Tafeltennis',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'retourneren')->first()->id,
            'path'=>'tafeltennis',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Jongleren met ballen en doekjes',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'soleren')->first()->id,
            'path'=>'jongleren-met-ballen-en-doekjes',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Dribbelen met basketbal',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'soleren')->first()->id,
            'path'=>'dribbelen-met-basketbal',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Dribbelen met voetbal',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'soleren')->first()->id,
            'path'=>'dribbelen-met-voetbal',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Dribbelen met hockey',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'soleren')->first()->id,
            'path'=>'dribbelen-met-hockey',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Kaatsenballen',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'werpen-en-vangen')->first()->id,
            'path'=>'kaatsenballen',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Lijnbal',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'keepersspelen')->first()->id,
            'path'=>'kijnbal',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Lummelen',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'lummelspelen')->first()->id,
            'path'=>'lummelen',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Hockey',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'wegspelen')->first()->id,
            'path'=>'hockey',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Slagbal en Beeball',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'honkloopspelen')->first()->id,
            'path'=>'slagbal-en-beeball',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'De Chinese muur',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'tikspelen')->first()->id,
            'path'=>'de-chinese-muur',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Kantelen',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'stoeispelen')->first()->id,
            'path'=>'kantelen',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'In de maat',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'bewegen-op-muziek')->first()->id,
            'path'=>'in-de-maat',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Streetdance',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'bewegen-op-muziek')->first()->id,
            'path'=>'streetdance',
            'demo'=>0
        ] );



        Bewegingsactiviteit::create( [
            'name'=>'Aerobics',
            'bewegingsthema_id'=> Bewegingsthema::where('path', 'bewegen-op-muziek')->first()->id,
            'path'=>'aerobics',
            'demo'=>0
        ] );


    }
}
