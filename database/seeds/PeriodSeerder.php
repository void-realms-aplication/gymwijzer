<?php

use Illuminate\Database\Seeder;
use App\Period\PeriodTemplate;
use App\Period\PeriodConfigurationTemplate;
use App\Period\PeriodConfiguration;
use Carbon\Carbon;

class PeriodSeerder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PeriodConfigurationtemplate::create([
            'year_start' => Carbon::createFromDate(2020, 9, 1),
            'year_end' => Carbon::createFromDate(2021, 6, 31),
            'max_activities' => 12,
            'recommended_min_activities' => 4,
            'recommended_max_activities' => 8
        ]);

        PeriodConfigurationtemplate::create([
            'year_start' => Carbon::createFromDate(2020, 9, 1),
            'year_end' => Carbon::createFromDate(2021, 6, 31),
            'max_activities' => 10,
            'recommended_min_activities' => 3,
            'recommended_max_activities' => 6
        ]);

        PeriodConfigurationtemplate::create([
            'year_start' => Carbon::createFromDate(2020, 9, 1),
            'year_end' => Carbon::createFromDate(2021, 6, 31),
            'max_activities' => 8,
            'recommended_min_activities' => 2,
            'recommended_max_activities' => 4
        ]);

        PeriodConfigurationtemplate::create([
            'year_start' => Carbon::createFromDate(2020, 9, 1),
            'year_end' => Carbon::createFromDate(2021, 6, 31),
            'max_activities' => 6,
            'recommended_min_activities' => 1,
            'recommended_max_activities' => 2
        ]);

        /* Optie 1: 2 periodes */
        PeriodTemplate::create([
            'config_id' => 1,
            'start' => Carbon::createFromDate(2020, 9, 1),
            'end' => Carbon::createFromDate(2020, 12, 31),
        ]);

        PeriodTemplate::create([
            'config_id' => 1,
            'start' => Carbon::createFromDate(2021, 1, 1),
            'end' => Carbon::createFromDate(2021, 6, 30 ),
        ]);

        /* Optie 2: 3 periodes */
        PeriodTemplate::create([
            'config_id' => 2,
            'start' => Carbon::createFromDate(2020, 9, 1),
            'end' => Carbon::createFromDate(2020, 12, 31),
        ]);

        PeriodTemplate::create([
            'config_id' => 2,
            'start' => Carbon::createFromDate(2020, 1, 1),
            'end' => Carbon::createFromDate(2021, 2, 28),
        ]);

        PeriodTemplate::create([
            'config_id' => 2,
            'start' => Carbon::createFromDate(2021, 3, 1),
            'end' => Carbon::createFromDate(2021, 6, 30),
        ]);

        /* Optie 3: 5 periodes */
        PeriodTemplate::create([
            'config_id' => 3,
            'start' => Carbon::createFromDate(2020, 9, 1),
            'end' => Carbon::createFromDate(2020, 10, 31),
        ]);

        PeriodTemplate::create([
            'config_id' => 3,
            'start' => Carbon::createFromDate(2020, 11, 1),
            'end' => Carbon::createFromDate(2020, 12, 31),
        ]);

        PeriodTemplate::create([
            'config_id' => 3,
            'start' => Carbon::createFromDate(2021, 1, 1),
            'end' => Carbon::createFromDate(2021, 2, 28),
        ]);

        PeriodTemplate::create([
            'config_id' => 3,
            'start' => Carbon::createFromDate(2021, 3, 1),
            'end' => Carbon::createFromDate(2021, 5, 31),
        ]);

        PeriodTemplate::create([
            'config_id' => 3,
            'start' => Carbon::createFromDate(2021, 6, 1),
            'end' => Carbon::createFromDate(2021, 6, 30),
        ]);

        /* Optie 4: 5 periodes */
        PeriodTemplate::create([
            'config_id' => 4,
            'start' => Carbon::createFromDate(2020, 9, 1),
            'end' => Carbon::createFromDate(2020, 9, 30),
        ]);

        PeriodTemplate::create([
            'config_id' => 4,
            'start' => Carbon::createFromDate(2020, 10, 1),
            'end' => Carbon::createFromDate(2020, 10, 31),
        ]);

        PeriodTemplate::create([
            'config_id' => 4,
            'start' => Carbon::createFromDate(2020, 11, 1),
            'end' => Carbon::createFromDate(2020, 11, 30),
        ]);

        PeriodTemplate::create([
            'config_id' => 4,
            'start' => Carbon::createFromDate(2020, 12, 1),
            'end' => Carbon::createFromDate(2020, 11, 31),
        ]);

        PeriodTemplate::create([
            'config_id' => 4,
            'start' => Carbon::createFromDate(2021, 1, 1),
            'end' => Carbon::createFromDate(2021, 11, 31),
        ]);

        PeriodTemplate::create([
            'config_id' => 4,
            'start' => Carbon::createFromDate(2021, 2, 1),
            'end' => Carbon::createFromDate(2021, 2, 31),
        ]);

        PeriodTemplate::create([
            'config_id' => 4,
            'start' => Carbon::createFromDate(2021, 3, 1),
            'end' => Carbon::createFromDate(2021, 3, 31),
        ]);

        PeriodTemplate::create([
            'config_id' => 4,
            'start' => Carbon::createFromDate(2021, 4, 1),
            'end' => Carbon::createFromDate(2021, 4, 31),
        ]);

        PeriodTemplate::create([
            'config_id' => 4,
            'start' => Carbon::createFromDate(2021, 5, 1),
            'end' => Carbon::createFromDate(2021, 5, 31),
        ]);

        PeriodTemplate::create([
            'config_id' => 4,
            'start' => Carbon::createFromDate(2021, 6, 1),
            'end' => Carbon::createFromDate(2021, 6, 30),
        ]);

        $cfg = new PeriodConfiguration([
            'school_id' => 4,
            'max_activities' => 8,
            'recommended_min_activities' => 2,
            'recommended_max_activities' => 4
        ]);

        $cfg->setStartEndDates(3);
        $cfg->save();
        $cfg->initPeriods(3);

    }
}
