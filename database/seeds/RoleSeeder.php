<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Ability;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            ["dev", "The account type for the developer group"],
            ["master", "The account type for the product owner who can manage the whole system"],
            ["dean", "The school master who manages one particular school"],
            ["teacher", "A gym teacher who teaches in one particular school"],

        ];
        foreach ($roles as $roleData) {
             $role = Role::create([
                'name' => $roleData[0],
                'description' => $roleData[1]
            ]);

            if($roleData[0] == 'dean' || $roleData[0] == 'master') {
                $ability = Ability::where('name', 'invite_teachers')->get();
                $role->allowTo($ability);
            }
            /* toevoegen van alle abilities van alle dashbord-content
             * voor alle rollen die hier boven zijn gemaakt
             */
            if($roleData[0] == 'dev') {
                $ability = Ability::where('name', 'dev_dashboard')->get();
                $role->allowTo($ability);
                $ability = Ability::where('name', 'view_gymwijzer_admin_page')->get();
                $role->allowTo($ability);
                $ability = Ability::where('name', 'manage_gymwijzer_content')->get();
                $role->allowTo($ability);
            }
            if($roleData[0] == 'master') {
                $ability = Ability::where('name', 'master_dashboard')->get();
                $role->allowTo($ability);
                $ability = Ability::where('name', 'manage_all_schools')->get();
                $role->allowTo($ability);
                $ability = Ability::where('name', 'view_gymwijzer_admin_page')->get();
                $role->allowTo($ability);
                $ability = Ability::where('name', 'manage_gymwijzer_content')->get();
                $role->allowTo($ability);
                $ability = Ability::where('name', 'manage_school_periods')->get();
                $role->allowTo($ability);
                $ability = Ability::where('name', 'dean_dashboard')->get();
                $role->allowTo($ability);
            }
            if($roleData[0] == 'dean') {
                $ability = Ability::where('name', 'dean_dashboard')->get();
                $role->allowTo($ability);
                $ability = Ability::where('name', 'manage_own_school')->get();
                $role->allowTo($ability);
                $ability = Ability::where('name', 'renew_subscription')->get();
                $role->allowTo($ability);
                $ability = Ability::where('name', 'manage_children')->get();
                $role->allowTo($ability);
                $ability = Ability::where('name', 'manage_class')->get();
                $role->allowTo($ability);
                $ability = Ability::where('name', 'manage_school_periods')->get();
                $role->allowTo($ability);
                $ability = Ability::where('name', 'manage_logo')->get();
                $role->allowTo($ability);
            }
            if($roleData[0] == 'teacher') {
                $ability = Ability::where('name', 'teacher_dashboard')->get();
                $role->allowTo($ability);
            }


        }

    }
}
