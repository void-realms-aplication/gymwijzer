<?php

use Illuminate\Database\Seeder;
use App\Leerlijn;
use App\Bewegingsthema;


class BewegingsthemaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $new = [
//            ['Acrobatiek', 'acrobatiek', 'balanceren', 0],
//            ['Glijden', 'glijden', 'balanceren', 0],
//            ['Balanceren', 'balanceren', 'balanceren', 0],
//            ['Mikken', 'mikken', 'mikken', 0]
//
//        ];
//
//        foreach ($new as $bewegingsactiviteit) {
//            DB::table('bewegingsthemas')->insert([
//                'name' => $bewegingsactiviteit[0],
//                'path' => $bewegingsactiviteit[1],
////                'leerlijn' => $bewegingsactiviteit[2],
//                'leerxxlijn_id' => Leerlijn::where('path', $bewegingsactiviteit[2])->first()->id,
//                'demo' => $bewegingsactiviteit[3],
//            ]);
//        }

        Bewegingsthema::create( [
            'name'=>'Acrobatiek',
            'leerlijn_id'=> Leerlijn::where('path', 'balanceren')->first()->id,
            'path'=>'acrobatiek',
            'demo'=>1
        ] );



        Bewegingsthema::create( [
            'name'=>'Balanceren',
            'leerlijn_id'=> Leerlijn::where('path', 'balanceren')->first()->id,
            'path'=>'balanceren',
            'demo'=>1
        ] );



        Bewegingsthema::create( [
            'name'=>'Glijden',
            'leerlijn_id'=> Leerlijn::where('path', 'balanceren')->first()->id,
            'path'=>'glijden',
            'demo'=>0
        ] );



        Bewegingsthema::create( [
            'name'=>'Klimmen',
            'leerlijn_id'=> Leerlijn::where('path', 'klimmen')->first()->id,
            'path'=>'klimmen',
            'demo'=>0
        ] );



        Bewegingsthema::create( [
            'name'=>'Klauteren',
            'leerlijn_id'=> Leerlijn::where('path', 'klimmen')->first()->id,
            'path'=>'klauteren',
            'demo'=>0
        ] );



        Bewegingsthema::create( [
            'name'=>'Hangendzwaaien',
            'leerlijn_id'=> Leerlijn::where('path', 'zwaaien')->first()->id,
            'path'=>'hangendzwaaien',
            'demo'=>0
        ] );



        Bewegingsthema::create( [
            'name'=>'Steunzwaaien',
            'leerlijn_id'=> Leerlijn::where('path', 'zwaaien')->first()->id,
            'path'=>'steunzwaaien',
            'demo'=>0
        ] );



        Bewegingsthema::create( [
            'name'=>'Schommelen',
            'leerlijn_id'=> Leerlijn::where('path', 'zwaaien')->first()->id,
            'path'=>'schommelen',
            'demo'=>0
        ] );



        Bewegingsthema::create( [
            'name'=>'Over de kop gaan',
            'leerlijn_id'=> Leerlijn::where('path', 'over-de-kop-gaan')->first()->id,
            'path'=>'over-de-kop-gaan',
            'demo'=>0
        ] );



        Bewegingsthema::create( [
            'name'=>'Duikelen',
            'leerlijn_id'=> Leerlijn::where('path', 'over-de-kop-gaan')->first()->id,
            'path'=>'duikelen',
            'demo'=>0
        ] );



        Bewegingsthema::create( [
            'name'=>'Freerunnen',
            'leerlijn_id'=> Leerlijn::where('path', 'springen')->first()->id,
            'path'=>'freerunnen',
            'demo'=>0
        ] );



        Bewegingsthema::create( [
            'name'=>'Hoogspringen',
            'leerlijn_id'=> Leerlijn::where('path', 'springen')->first()->id,
            'path'=>'hoogspringen',
            'demo'=>0
        ] );



        Bewegingsthema::create( [
            'name'=>'Verspringen',
            'leerlijn_id'=> Leerlijn::where('path', 'springen')->first()->id,
            'path'=>'verspringen',
            'demo'=>0
        ] );



        Bewegingsthema::create( [
            'name'=>'Steunspringen',
            'leerlijn_id'=> Leerlijn::where('path', 'springen')->first()->id,
            'path'=>'steunspringen',
            'demo'=>0
        ] );



        Bewegingsthema::create( [
            'name'=>'Trampolinespringen',
            'leerlijn_id'=> Leerlijn::where('path', 'springen')->first()->id,
            'path'=>'trampolinespringen',
            'demo'=>0
        ] );



        Bewegingsthema::create( [
            'name'=>'Touwtjespringen',
            'leerlijn_id'=> Leerlijn::where('path', 'springen')->first()->id,
            'path'=>'touwtjespringen',
            'demo'=>0
        ] );



        Bewegingsthema::create( [
            'name'=>'Estafette',
            'leerlijn_id'=> Leerlijn::where('path', 'hardlopen')->first()->id,
            'path'=>'estafette',
            'demo'=>0
        ] );



        Bewegingsthema::create( [
            'name'=>'Starten',
            'leerlijn_id'=> Leerlijn::where('path', 'hardlopen')->first()->id,
            'path'=>'starten',
            'demo'=>0
        ] );



        Bewegingsthema::create( [
            'name'=>'Shuttlerun test',
            'leerlijn_id'=> Leerlijn::where('path', 'hardlopen')->first()->id,
            'path'=>'shuttlerun-test',
            'demo'=>0
        ] );



        Bewegingsthema::create( [
            'name'=>'Wegspelen van tennisbal',
            'leerlijn_id'=> Leerlijn::where('path', 'mikken')->first()->id,
            'path'=>'wegspelen-van-tennisbal',
            'demo'=>0
        ] );



        Bewegingsthema::create( [
            'name'=>'Mikken',
            'leerlijn_id'=> Leerlijn::where('path', 'mikken')->first()->id,
            'path'=>'mikken',
            'demo'=>0
        ] );



        Bewegingsthema::create( [
            'name'=>'Retourneren',
            'leerlijn_id'=> Leerlijn::where('path', 'jongleren')->first()->id,
            'path'=>'retourneren',
            'demo'=>0
        ] );



        Bewegingsthema::create( [
            'name'=>'Soleren',
            'leerlijn_id'=> Leerlijn::where('path', 'jongleren')->first()->id,
            'path'=>'soleren',
            'demo'=>0
        ] );



        Bewegingsthema::create( [
            'name'=>'Werpen en vangen',
            'leerlijn_id'=> Leerlijn::where('path', 'Jongleren')->first()->id,
            'path'=>'werpen-en-vangen',
            'demo'=>0
        ] );



        Bewegingsthema::create( [
            'name'=>'Keepersspelen',
            'leerlijn_id'=> Leerlijn::where('path', 'doelspelen')->first()->id,
            'path'=>'keepersspelen',
            'demo'=>0
        ] );



        Bewegingsthema::create( [
            'name'=>'Lummelspelen',
            'leerlijn_id'=> Leerlijn::where('path', 'doelspelen')->first()->id,
            'path'=>'lummelspelen',
            'demo'=>0
        ] );



        Bewegingsthema::create( [
            'name'=>'Wegspelen',
            'leerlijn_id'=> Leerlijn::where('path', 'doelspelen')->first()->id,
            'path'=>'wegspelen',
            'demo'=>0
        ] );



        Bewegingsthema::create( [
            'name'=>'Honkloopspelen',
            'leerlijn_id'=> Leerlijn::where('path', 'tikspelen')->first()->id,
            'path'=>'honkloopspelen',
            'demo'=>0
        ] );



        Bewegingsthema::create( [
            'name'=>'Tikspelen',
            'leerlijn_id'=> Leerlijn::where('path', 'tikspelen')->first()->id,
            'path'=>'tikspelen',
            'demo'=>0
        ] );



        Bewegingsthema::create( [
            'name'=>'Stoeispelen',
            'leerlijn_id'=> Leerlijn::where('path', 'stoeispelen')->first()->id,
            'path'=>'stoeispelen',
            'demo'=>0
        ] );

        Bewegingsthema::create( [
            'name'=>'Bewegen op muziek',
            'leerlijn_id'=> Leerlijn::where('path', 'bewegen-op-muziek')->first()->id,
            'path'=>'bewegen-op-muziek',
            'demo'=>0
        ] );
    }
}
