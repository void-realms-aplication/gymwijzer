<?php

use Illuminate\Database\Seeder;
use App\Ability;


class AbilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $abilities = [
            ["admin_pagina", "Kan de admin pagina bekjijken ofzo"],
            ["manage_gymwijzer_content", "Kan de GymWijzer! leerlijn/video content beheren"],
            ["invite_teachers", "De user mag leraren nieuwe uitnodigen voor zijn school"],
            ["invite_teachers_always", "De user mag nieuwe leraren uitnodigen voor alles scholen"],
            ['dev_dashboard','kan de dev dashboard zien'],
            ['master_dashboard','kan de master dashboard zien'],
            ['dean_dashboard','kan de dean dashboard zien'],
            ['teacher_dashboard','kan de teacher dashboard zien'],
            ['manage_all_schools', 'Mag de groepen van (zijn eigen) school beheren'],
            ['manage_own_school', 'Mag de groepen van alle scholen beheren'],
            ['renew_subscription', 'Mag het school abonement verlengen'],
            ['manage_children', 'Mag kinderen toevoegen en verwijderen'],
            ['manage_class', 'Mag kinderen toevoegen en verwijderen'],
            ['manage_school_periods', 'Mag de hoeveel periodes per jaar voor een school beheren'],
            ['manage_logo', 'Mar het logo van de school weizigen'],
        ];

        foreach ($abilities as $abilityData) {
            $ability = Ability::create([
                'name' => $abilityData[0],
                'description' => $abilityData[1]
            ]);
        }

    }
}
