<?php
use Illuminate\Database\Seeder;
use App\School;
use App\User;
use App\Schoolclass;

class TestSchoolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $School = School::create([
            'name' => 'de niet bestaande test school',
            'email' => 'info@testschool.nl',
            'phone_number' => '0000000000',
            'brin_number' => '12345',
            'country' => 'Nederland',
            'city' => 'Gouda',
            'street' => 'Coniferensingel',
            'house_number' => '69',
            'postal_code' => '2157JH',
            'number_of_students' => '500',
            'subscription_id' => 5,
        ]);
        $ArrayOfUsersWithClasses = (
        [
            [
                ['firstname' => 'test', 'lastname' => 'school', 'email' => 'test@school.com', 'password' => Hash::make('test'), 'role_id' => 3, 'school_id' => $School->id],
                false
            ],
            [
                ['firstname' => 'test', 'lastname' => 'teacher', 'email' => 'test@teacher.com', 'password' => Hash::make('test'), 'role_id' => 4, 'school_id' => $School->id],
                true,
                [
                    ['name' => 'groep 1A', 'grade' => '1', 'email' => 'docent@voor.klas1en2A','school_id' => $School->id],
                    ['name' => 'groep 2A', 'grade' => '2', 'email' => 'docent@voor.klas1en2A','school_id' => $School->id],
                    ['name' => 'groep 1B', 'grade' => '1', 'email' => 'docent@voor.klas1en2B','school_id' => $School->id],
                    ['name' => 'groep 2B', 'grade' => '2', 'email' => 'docent@voor.klas1en2B','school_id' => $School->id],
                    ['name' => 'groep 1C', 'grade' => '1', 'email' => 'docent@voor.klas1en2C','school_id' => $School->id],
                    ['name' => 'groep 2C', 'grade' => '2', 'email' => 'docent@voor.klas1en2C','school_id' => $School->id],
                    ['name' => 'groep 3', 'grade' => '3', 'email' => 'docent@voor.klas3','school_id' => $School->id],
                    ['name' => 'groep 4', 'grade' => '4', 'email' => 'docent@voor.klas4','school_id' => $School->id],
                    ['name' => 'groep 5', 'grade' => '5', 'email' => 'docent@voor.klas5','school_id' => $School->id],
                    ['name' => 'groep 6', 'grade' => '6', 'email' => 'docent@voor.klas6','school_id' => $School->id],
                    ['name' => 'groep 7', 'grade' => '7', 'email' => 'docent@voor.klas7','school_id' => $School->id],
                    ['name' => 'groep 8', 'grade' => '8', 'email' => 'docent@voor.klas8','school_id' => $School->id]
                ]
            ]
        ]);

        for ($i = 0; $i < sizeof($ArrayOfUsersWithClasses); $i++)
        {
            $User = User::create($ArrayOfUsersWithClasses[$i][0]);
            if ($ArrayOfUsersWithClasses[$i][1] == true)
            {
                for ($j =0; $j < sizeof($ArrayOfUsersWithClasses[$i][2]); $j++)
                {
                    $class = Schoolclass::create($ArrayOfUsersWithClasses[$i][2][$j]);
                    DB::table('schoolclass_gymteacher')->insert(['user_id' => $User->id, 'schoolclass_id' => $class->id]);
                    factory(App\Child::class, 10)->create()->each(function ($child) use ($class, $School) {
                        $child->setSchoolClass($class->id);
                        $child->setSchool_id($School->id);
                    });

                }
            }
        }
    }
}
