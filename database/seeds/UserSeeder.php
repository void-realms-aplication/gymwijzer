<?php

use Illuminate\Database\Seeder;
use App\User;
use App\School;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $school = factory(App\School::class)->create();

        DB::table('users')->insert([
            ['firstname' => 'Slava', 'lastname' => 'Syurin', 'email' => 'slava@syurin.com', 'password' => Hash::make('tieten'), 'role_id' => 1 ],
            ['firstname' => 'zelle', 'lastname' => 'berg', 'email' => 'zelle@berg.com', 'password' => Hash::make('1234'), 'role_id' => 1],
            ['firstname' => 'Harry', 'lastname' => 'vd Meer', 'email' => 'info@gymwijzer.nl', 'password' => Hash::make('harry'), 'role_id' => 2],
            ['firstname' => 'Boas', 'lastname' => 'Kalma', 'email' => 'boas.kalma@planet.nl', 'password' => Hash::make('2hI1%cWjPyZo%4MO'), 'role_id' => 1],
            ['firstname' => 'Erol', 'lastname' => 'Meeuws', 'email' => 'a@a.a', 'password' => Hash::make('aaaa'), 'role_id' => 1],
            ['firstname' => 'Erol', 'lastname' => 'Meeuws', 'email' => 'boas.kalma@gmail.com', 'password' => Hash::make('12345678'), 'role_id' => 3],
        ]);

    }
}
