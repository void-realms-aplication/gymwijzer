<?php

use Illuminate\Database\Seeder;

class SchoolClassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('schoolclasses')->insert([[
            'name' => '3A',
            'grade' => '3',
            'email' => 'waarom@watkanjewel.nl',
            'school_id' => 2
        ],[
            'name' => '3B',
            'grade' => '3',
            'email' => 'waarom@watkanjewel.nl',
            'school_id' => 2
        ],[
            'name' => '3C',
            'grade' => '3',
            'email' => 'waarom@watkanjewel.nl',
            'school_id' => 2
        ],[
            'name' => '3D',
            'grade' => '3',
            'email' => 'waarom@watkanjewel.nl',
            'school_id' => 2
        ],[
            'name' => '4A',
            'grade' => '4',
            'email' => 'waarom@watkanjewel.nl',
            'school_id' => 2
        ],[
            'name' => '4B',
            'grade' => '4',
            'email' => 'waarom@watkanjewel.nl',
            'school_id' => 2
        ],[
            'name' => 'Groep 5',
            'grade' => '5',
            'email' => 'waarom@watkanjewel.nl',
            'school_id' => 2
        ],[
            'name' => 'Groep 6',
            'grade' => '6',
            'email' => 'waarom@watkanjewel.nl',
            'school_id' => 2
        ],[
            'name' => 'Groep 7',
            'grade' => '7',
            'email' => 'waarom@watkanjewel.nl',
            'school_id' => 2
        ],[
            'name' => 'Groep 8',
            'grade' => '8',
            'email' => 'waarom@watkanjewel.nl',
            'school_id' => 2
        ]]);
    }
}
