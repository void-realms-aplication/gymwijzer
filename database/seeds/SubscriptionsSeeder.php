<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class SubscriptionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subscriptions')->insert([
            ['name' => 'Student',         'description' => 'voor één gebruiker',                       'price' => '15',  'max_students' => '40'],
            ['name' => '<200 Leerlingen', 'description' => 'voor scholen tot 200 leerlingen',          'price' => '40',  'max_students' => '200'],
            ['name' => '<400 Leerlingen', 'description' => 'voor scholen tot 200 leerlingen',          'price' => '75',  'max_students' => '400'],
            ['name' => '<600 Leerlingen', 'description' => 'voor scholen tot 200 leerlingen',          'price' => '100', 'max_students' => '600'],
            ['name' => '>600 Leerlingen', 'description' => 'voor scholen met meer dan 600 leerlingen', 'price' => '175', 'max_students' => null]
        ]);
    }
}
