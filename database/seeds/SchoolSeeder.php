<?php

use Illuminate\Database\Seeder;

class SchoolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('schools')->insert([[
            'name' => 'De Triangel',
            'email' => 'info@triangelgouda.nl',
            'phone_number' => '0182530225',
            'brin_number' => '12345',
            'country' => 'Nederland',
            'city' => 'Gouda',
            'street' => 'Coniferensingel',
            'house_number' => '20',
            'postal_code' => '2803JH',
            'subscription_id' => 2,
            'number_of_students' => '400',
            'subscription_id' => 3,
        ], [
            'name' => 'De Vuurvogel',
            'email' => 'info@vuurvogelgouda.nl',
            'phone_number' => '0182524424',
            'brin_number' => '12345',
            'country' => 'Nederland',
            'city' => 'Gouda',
            'street' => 'Scharroosingel',
            'house_number' => '54',
            'postal_code' => '2807 BX',
            'subscription_id' => 3,
            'number_of_students' => '200',
            'subscription_id' => 2,
        ], [
            'name' => 'De Cirkel',
            'email' => 'info@cirkelgouda.nl',
            'phone_number' => '0182539877',
            'brin_number' => '12345',
            'country' => 'Nederland',
            'city' => 'Gouda',
            'street' => 'Rijnlust 42',
            'house_number' => '54',
            'postal_code' => '2804 LC',
            'subscription_id' => 2,
            'number_of_students' => '200',
            'subscription_id' => 2,
        ]]);


    }
}
