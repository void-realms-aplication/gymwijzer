<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolclassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schoolclasses', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->id()->unsigned();

            $table->string('name'); //#slava - this was; 'class_name'. naming conventions
            $table->string('grade');
            $table->string('email')->nullable();

            //#slava replace the teacher first and last last name by their teacher id
            //THIS SHOULD REFERENCE THE USERS TABLE WITH A PROPER TEACHER?
            //but what if one class has multiple teachers!?
            //so now we don't reference the teacher here but we do a table inbetween!
            $table->foreignId('school_id')->constrained()->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schoolclasses');
    }
}
