<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeriodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('period_configurations', function(Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('max_activities');
            $table->integer('recommended_min_activities');
            $table->integer('recommended_max_activities');
            $table->date('year_start');
            $table->date('year_end');
            $table->foreignId('school_id')->references('id')->on('schools')->onDelete('cascade');
        });

        Schema::create('periods', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('config_id')->references('id')->on('period_configurations')->onDelete('cascade');
            $table->date('start');
            $table->date('end');
        });

        Schema::create('activity_period', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('period_id')->references('id')->on('periods')->onDelete('cascade');
            $table->foreignId('activity_id')->references('id')->on('bewegingsactiviteiten')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
