<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeriodconfigurationtemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('period_configuration_templates', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('max_activities');
            $table->integer('recommended_min_activities');
            $table->integer('recommended_max_activities');
            $table->date('year_start');
            $table->date('year_end');
        });

        Schema::create('period_templates', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('config_id')->references('id')->on('period_configuration_templates');
            $table->date('start');
            $table->date('end');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('period_configuration_templates');

        Schema::dropIfExists('period_templates');
    }
}
