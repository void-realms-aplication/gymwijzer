<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserInvitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_invites', function (Blueprint $table) {
            $table->string('email')->index();
            $table->timestamps();
            $table->string('token');
            $table->string('invitedBy'); //the user id of the admin user that sent the invitation
            $table->foreignId('school_id')->nullable()->constrained()->onDelete('cascade')->onUpdate('cascade');
            $table->timestamp('expires')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_invites');
    }
}
