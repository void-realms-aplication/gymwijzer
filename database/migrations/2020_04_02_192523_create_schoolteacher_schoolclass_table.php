<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolTeacherSchoolclassTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schoolteacher_schoolclass', function (Blueprint $table) {
            $table->id();

            $table->foreignId('schoolteacher_id')->constrained()->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('schoolclass_id')->constrained()->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teacher_schoolclass');
    }
}
