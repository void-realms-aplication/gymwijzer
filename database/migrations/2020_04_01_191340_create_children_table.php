<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChildrenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('children', function (Blueprint $table) {
            /*$table->engine = "InnoDB";*/
            $table->id();
            $table->string('firstname', 30);
            $table->string('tussenVoegsel', 30)->nullable();
            $table->string('lastname', 30);
            $table->date('date_of_birth');
            $table->string('password');

            $table->foreignId('schoolclass_id')->constrained()->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('school_id')->constrained()->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });

        DB::statement('
        alter table children modify schoolclass_id bigint unsigned null;
        ');
        DB::statement('
        alter table children drop foreign key children_schoolclass_id_foreign;
        ');
        DB::statement('
        alter table children
            add constraint children_schoolclass_id_foreign
                foreign key (schoolclass_id) references schoolclasses (id)
                    on update set null on delete set null;
        ');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('children');
    }
}
