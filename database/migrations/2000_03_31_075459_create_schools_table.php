<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->id();
            $table->string('name', 50)->nullable(); //#slava - this was; 'school_name'. naming conventions
            //#sprint5: now nullable as school is initiated w/o name;
            $table->string('email', 50); //#slava - this was; 'school_email'. naming conventions
            //#slava - do we actually need a school email?
            $table->string('phone_number', 15); //#slava - this was; 'school_phone_number'. naming conventions
            //#slava: this was an integer, but if you try to insert a phone number starting with 0 - well wont work
            $table->string('brin_number', 20);
            $table->string('country', 20);
            $table->string('city', 20);
            $table->string('street', 50);
            $table->string('house_number', 10);
            $table->string('additive', 10)->nullable();
            $table->string('postal_code', 10);
            $table->integer('number_of_students')->nullable(); //#slava I suppose we mean MAX number of students?
            $table->foreignId('subscription_id')->constrained()->onDelete('cascade')->onUpdate('cascade');
            //boas, fiels nassesary for experation
            $table->timestamp('expires_at')->nullable();
            $table->boolean('expired')->default(false);
            $table->string('logo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
    }
}
