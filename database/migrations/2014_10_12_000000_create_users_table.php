<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('lastname', 60);
            $table->string('tussenvoegsel', 10)->nullable();
            $table->string('firstname', 60);
            $table->string('email', 50)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable(); //#slava. we want to be able to create users w/o a password
            $table->boolean('lesson_mode')->default(false);
            $table->rememberToken();
            //#slava: school id - > nullable: so that master accounts don't have to be connected to the school
            $table->foreignId('school_id')->nullable()->constrained()->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('role_id')->constrained()->onDelete('cascade')->onUpdate('cascade');
            //#slava: dev / master accounts also don't have an invoice, so nullable
            $table->foreignId('invoice_id')->nullable()->constrained()->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
