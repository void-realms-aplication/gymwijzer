<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKijkwijzersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kijkwijzers', function (Blueprint $table) {
            $table->id();
            $table->string('path');
            $table->string('name');
            $table->string('description');
            $table->foreignId('bewegingsactiviteit_id')->references('id')->on('bewegingsactiviteiten')->onUpdate('cascade')->onDelete('cascade'); //->onUpdate('cascade')->onDelete('cascade')->
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kijkwijzers');
    }
}


