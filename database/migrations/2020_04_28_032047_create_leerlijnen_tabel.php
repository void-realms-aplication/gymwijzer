<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeerlijnenTabel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leerlijnen', function (Blueprint $table) {
            $table->id();
            $table->text('name');
//            $table->text('parent_path')->nullable();
            $table->text('path');
//            $table->integer('leerlijnNummer')->nullable();
//            $table->integer('bewegingsthemaNummer')->nullable();
//            $table->integer('bewegingsactiviteitNummer')->nullable();
//            $table->boolean('has_video');
            $table->boolean('demo');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leerlijnen');
    }
}
