<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->id();
            $table->string('bewegingsactiviteit');
            $table->integer('niveau'); // 0 = starter, 1 = niveau 1,  2 = niveau 2, 3 = niveau 3,  4 = niveau 4, 5 = gevorderd, 6 = niveau 1&2, 7 = niveau 3&4, 9 = alle neveaus
            $table->string('titel');
            $table->string('link');
            $table->integer('volgorde');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
