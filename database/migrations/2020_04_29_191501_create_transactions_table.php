<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //#slava: Transactions schema. right now im implementing it for buying the subscription
        //later the same table/model probably could be used for renewing payments

        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->string('email');
            $table->string('status')->default('Open'); //paid? //expired? //cancelled? //pending
//            $table->timestamp('expires'); //we need some kind of a deadline to finish the payment

            //Basic payer payment details, mostly taken from: gymwijzer.nl/checkout
            $table->string('firstname');
            $table->string('tussenvoegsel')->nullable();
            $table->string('lastname');
            $table->string('company')->nullable();
            $table->string('brin_number', 20)->nullable();
            $table->string('country', 20);
            $table->string('city', 20);
            $table->string('street', 50);
            $table->string('house_number', 10);
            $table->string('additive', 10)->nullable();
            $table->string('postal_code', 10);
            $table->string('phone_number', 15);
            $table->string('mollieToken', 30);
            $table->string('serverToken', 30);
            $table->boolean('renew')->default(false);
            //which subscription are we buying?
            $table->foreignId('subscription_id')->constrained()->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();

            //TODO: Mollie tokens and stuff should be added here later on
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
