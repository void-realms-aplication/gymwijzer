<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBewegingsthemasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bewegingsthemas', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->text('name');
            $table->text('path');
//            $table->foreignId('leerlijn_id')->constrained()->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('leerlijn_id');
            $table->foreign('leerlijn_id')->references('id')->on('leerlijnen')->onDelete('cascade')->onUpdate('cascade');
            $table->boolean('demo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bevegingsthemas');
    }
}
