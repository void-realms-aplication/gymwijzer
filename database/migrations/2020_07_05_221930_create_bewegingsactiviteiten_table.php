<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBewegingsactiviteitenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bewegingsactiviteiten', function (Blueprint $table) {
                $table->id();
                $table->timestamps();
                $table->text('name');
                $table->text('path');
                $table->boolean('demo');
//                $table->foreignId('bewegingsthema_id')->constrained()->onDelete('cascade')->onUpdate('cascade');
                $table->unsignedBigInteger('bewegingsthema_id');
                $table->foreign('bewegingsthema_id')->references('id')->on('bewegingsthemas')->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bewegingsactiviteiten');
    }
}
