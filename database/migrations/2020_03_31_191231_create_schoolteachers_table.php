<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schoolteachers', function (Blueprint $table) {
//          Table for the school teachers THAT DO NOT TEACH GYM
//          Those are here for statistic info purposes - they cannot log into the system!

            $table->id();

            $table->string('firstname', 30);
            $table->string('tussenvoegsel', 10)->nullable();
            $table->string('lastname', 30);
            $table->string('email');
//          No password or role id for the teachers!
            $table->foreignId('school_id')->constrained()->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teachers');
    }
}
