<?php

use Illuminate\Support\Facades\Route;

//Slava 11-06-2020 Custom Route file for test routes
//Purpose: cleanup the web.php file!
Route::get('/test', 'PurchaseController@test');
Route::get('/clear', 'PurchaseController@clear');
Route::get('/more', 'PurchaseController@moreTest');

Route::get('/next', 'PurchaseController@nextTest');

//FRONT END FOOTER TEST
Route::get('foot', function () {
    return view('test.testfooter');
});
