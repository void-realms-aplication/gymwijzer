<?php

use Illuminate\Support\Facades\Route;

/**
 * Route file for the following user features:
 *      - Invite User Via Mail & Register by Invitation
 *      - Setup password for new user
 */

// Invite User via mail feature (for new Gym Teachers)
Route::prefix('user')->group(function () {
    //Show the user invite form
    Route::get('/invite/', 'Auth\TeacherInviteController@create')->name('teacher.invite')->middleware('hasActiveSubscription');

    //Submit the request for inviting a teacher
    Route::post('/invite/', 'Auth\TeacherInviteController@store')->middleware('hasActiveSubscription');

    //Show the register form
    Route::get('/register/{token}', 'Auth\RegisterByInviteController@create')->name('user.registerWithInviteToken');

    //Submit the request for registering with a token
    Route::post('/register/', 'Auth\RegisterByInviteController@store')->name('user.saveNewUserWithInviteToken');
});

//The setting up password feature (after a User is registered)
Route::prefix('password')->group(function () {
    //Show the form for set up a password
    Route::get('/set/{token}', 'Auth\SetPasswordController@create')->name('password.set');

    //Persist the password in the database
    Route::post('/set/', 'Auth\SetPasswordController@store')->name('password.create');
});
