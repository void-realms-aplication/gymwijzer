<?php

use Illuminate\Support\Facades\Route;

//zelle 07-05-2020 Custom Route file for teacher routes
//Purpose: cleanup the web.php file!

//Add/edit class to school
Route::prefix('teacher')->group(function () {
    //Display the page for vieuing the selected schoolgroup AND adding/removing children from it
    Route::get('/group/{schoolclass}/viewgroup', 'SchoolclassController@show')->name('teacher.schoolclass.viewgroup')->middleware('hasActiveSubscription');

    //Display the page for vieuing the selected Child
    Route::get('/children/{child}/viewchild', 'ChildController@show')->name('teacher.child.viewchild');
});
