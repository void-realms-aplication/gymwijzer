<?php

use Illuminate\Support\Facades\Route;

//Slava 11-06-2020 Custom Route file for purchasing the subscription
//Purpose: cleanup the web.php file!5

//Purchase a subscription
Route::prefix('purchasesubscription')->group(function () {
    //Start the purchase from the mainpage with pre-selected subscription (Step 1)
    Route::post('/', 'PurchaseController@start')->name('purchase.start');

    //Start the purchase from the url (Step 1)
    Route::get('/', 'PurchaseController@start');

    //Display the next step of entering the need user data for purchase (Step 2)
    //Only accessible via POST request from previous step, unless..
    Route::post('/checkout', 'PurchaseController@checkout')->name('purchase.checkout');
    //Unless we are already here and have validation errors!
    Route::get('/checkout', 'PurchaseController@checkout')->middleware('validation_errors');

    //Submit the request with the filled user data to proceed to payment
    Route::post('/payment', 'PurchaseController@pay')->name('purchase.pay');
    //If the user lost the payment page - this route will redirect him to the payment page
    Route::get('/payment', 'PurchaseController@showPayPage')->name('purchase.view');

    //Display the success page after the payment is succesfull
    Route::get('/success/{id}', 'PurchaseController@success')->name('purchase.success');
});

//Prepare Mollie payment
Route::match(array('GET', 'POST'), 'mollie', 'MollieController@preparePayment')->name('mollie');

//Webhook for Mollie to use
Route::post('/webhook/paymentdata', 'MollieController@handlePayment')->name('purchase.webhook');

Route::get('/verlengen','RenewController@index')->name('renew')->middleware('auth');
Route::get('/verlengen/mollie', 'MollieController@renewPayment')->name('mollie.renew')->middleware('auth');
Route::get('/verlengen/success/{id}', 'RenewController@success')->name('renew.success')->middleware('auth');

