<?php

use Illuminate\Support\Facades\Route;

//Display the view for setting the amount of periods per schoolyear
Route::get('/set-periods/{schoolyear}', 'PeriodController@create')->name('setPeriods');

//Handle POST request for setting the amount of periods per schoolyear
Route::post('/set-periods/{schoolyear}', 'PeriodController@store');
Route::get('/edit-periods/{config}', 'PeriodController@edit')->name('editPeriods');
Route::post('/edit-periods/{config}', 'PeriodController@update');
