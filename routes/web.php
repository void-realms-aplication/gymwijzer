<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomepageController@index')->name('start');
Route::get('/demo', 'HomepageController@demo')->name('demo');

Auth::routes();

Route::get('/sportfolio', 'pdfController@download')->middleware('auth');

Route::get('/exit-lesson-mode', 'LessonModeController@update')->name('exitLessonMode');
Route::post('/exit-lesson-mode', 'LessonModeController@destroy')->name('exitLessonModePOST');


Route::get('/cookies', 'CookieControler@show')->name('cookies');

//Dashboard!
Route::get('/dashboard', 'DashboardController@index')->name('admin.dashboard')->middleware('hasActiveSubscription');
Route::get('/{selectedSchool}/dashboard', 'DashboardController@school')->middleware('hasActiveSubscription');

Route::get('/mijn-account', 'UserController@show')->name('mijnAccount');
Route::get('/mijn-account/edit', 'UserController@edit')->name('mijnAccount.edit');
Route::post('/mijn-account/edit', 'UserController@update')->name('mijnAccount.store');
Route::post('/mijn-account/edit/password', 'UserController@password')->name('password.post');
Route::get('/school/edit', 'SchoolController@edit')->name('school.edit')->middleware('auth');
Route::post('/school/edit', 'SchoolController@update')->name('school.store')->middleware('auth');

Route::get('/test/test', 'HomepageController@test');

Route::get('contact-us', 'ContactUsController@index')->name('contactUs');
Route::post('contact-us', 'ContactUsController@store')->name('contactUs.store');

/*
 * Now every time we need to force users to load new css we can specify different style url in the template
 * This code will make it return the very same css file!
 */

Route::get('/css/{file}/{token}', function ($file, $token) {
    if (File::exists(public_path() . '/css/' . $file . '.css')) {
        return response()->file(public_path() . '/css/' . $file . '.css', ['Content-Type' => 'text/css']);
    } else abort(404);
})->name('static-css');


Route::prefix('volgwijzer')->group(function () {
    Route::get('/', 'VolgWijzerController@index')->name('volgwijzer')->middleware('auth');
    Route::get('/demo', 'VolgWijzerController@demo')->name('volgwijzer.demo');
    Route::get('/logo', 'LogoController@index')->name('logo')->middleware('auth');
    Route::post('/logo', 'LogoController@store')->name('logo.store')->middleware('auth');


});

Route::get('/gymwijzer/overzicht', 'gymwijzer\LeerlijnController@overview')->name('leerlijnen.overview');
