<?php

use Illuminate\Support\Facades\Route;

Route::prefix('gymwijzer')->group(function () {
    Route::get('/', 'GymWijzer\LeerlijnController@index')->name('gymwijzer.index')->middleware('hasActiveSubscription');

    Route::get('/{leerlijn}', 'GymWijzer\LeerlijnController@show')->name('gymwijzer.leerlijn')->middleware('hasActiveSubscription');

    Route::get('/{leerlijn}/{bewegingsactiviteit}', 'GymWijzer\BewegingsthemaController@show')
        ->name('gymwijzer.bewegingsactiviteit')->middleware('hasActiveSubscription');

    Route::get('/{leerlijn}/{bewegingsthema}/{bewegingsactiviteit}', 'GymWijzer\BewegingsactiviteitController@show')
        ->name('gymwijzer.bewegingsthema')->middleware('hasActiveSubscription');


});
