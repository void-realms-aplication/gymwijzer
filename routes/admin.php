<?php

use Illuminate\Support\Facades\Route;

//Slava 11-06-2020 Custom Route file for admin routes
//Purpose: cleanup the web.php file!

//GymWijzer!
Route::prefix('admin-portal')->group(function () {
    Route::get('/', 'AdminPortalController@index')->name('gymwijzer.admin');

    //Resource routes: leerlijn
    Route::get('/add-leerlijn', 'GymWijzer\LeerlijnController@create')->name('adminportal.createleerlijn');
    Route::post('/add-leerlijn', 'GymWijzer\LeerlijnController@store');
    Route::get('/edit-leerlijn/{leerlijn}', 'GymWijzer\LeerlijnController@edit')->name('adminportal.editleerlijn');
    Route::put('/edit-leerlijn/{leerlijn}', 'GymWijzer\LeerlijnController@update');
    Route::post('/delete-leerlijn/{leerlijn}', 'GymWijzer\LeerlijnController@destroy')->name('adminportal.deleteleerlijn');

    //Resource routes: bewegingsthema
    Route::get('/add-bewegingsthema', 'GymWijzer\BewegingsthemaController@create')->name('adminportal.createbewegingsthema');
    Route::post('/add-bewegingsthema', 'GymWijzer\BewegingsthemaController@store');
    Route::get('/edit-bewegingsthema/{bewegingsthema}', 'GymWijzer\BewegingsthemaController@edit')->name('adminportal.editbewegingsthema');
    Route::put('/edit-bewegingsthema/{bewegingsthema}', 'GymWijzer\BewegingsthemaController@update');
    Route::post('/delete-bewegingsthema/{bewegingsthema}', 'GymWijzer\BewegingsthemaController@destroy')->name('adminportal.deletebewegingsthema');

    //Resource routes: bewegingsactiviteit
    Route::get('/add-bewegingsactiviteit', 'GymWijzer\BewegingsactiviteitController@create')->name('adminportal.createbewegingsactiviteit');
    Route::post('/add-bewegingsactiviteit', 'GymWijzer\BewegingsactiviteitController@store');
    Route::get('/edit-bewegingsactiviteit/{bewegingsactiviteit}', 'GymWijzer\BewegingsactiviteitController@edit')->name('adminportal.editbewegingsactiviteit');
    Route::put('/edit-bewegingsactiviteit/{bewegingsactiviteit}', 'GymWijzer\BewegingsactiviteitController@update');
    Route::post('/delete-bewegingsactiviteit/{bewegingsactiviteit}', 'GymWijzer\BewegingsactiviteitController@destroy')->name('adminportal.deletebewegingsactiviteit');
});

//Add/edit class to school
Route::prefix('admin')->group(function () {
    //Display the page for adding a group to the school
    Route::get('/upload', 'ClassUploadController@index')->name('upload.index');
    Route::post('/upload', 'ClassUploadController@store')->name('upload.store');

    Route::get('/school/{schoolId}/addgroup', 'SchoolclassController@create')->name('schoolclass.create');

    //Process the request for storing the new group (schoolclass) in the system
    Route::post('/school/{schoolId}/addgroup', 'SchoolclassController@store');

    //Display the page for editing the selected schoolgroup AND adding/removing teachers from it
    Route::get('/group/{schoolclass}/edit', 'SchoolclassController@edit')->name('schoolclass.edit');

    //Display the page for vieuing the selected schoolgroup AND adding/removing children from it
    Route::get('/group/{schoolclass}/viewgroup', 'SchoolclassController@show')->name('admin.schoolclass.viewgroup');

    //Process the edit request for the schoolgroup
    Route::post('/group/{schoolclass}/edit', 'SchoolclassController@update');

    //Process the request for attaching and detaching teachers to the school
    Route::post('/group/{schoolclass}/teachers', 'SchoolclassController@updateTeachers')->name('schoolclass.editTeacher');

    //Display the page for vieuing the selected Child
    Route::get('/children/{child}/viewChild', 'ChildController@show')->name('admin.child.viewchild');

    //Display the page for vieuing the selected Child
    Route::get('/children/{child}/viewChildWithoutGroup', 'ChildController@showChildWithoutGroup')->name('admin.child.viewchild_Withoutgroup');

    //adds Child
    Route::post('/addChild', 'ChildController@add')->name('admin.addChild');

    //eddits selected Child
    Route::post('/children/edditGroup', 'ChildController@edditGroup')->name('admin.child.edditGroup');

    //eddits selected Child
    Route::post('/children/store', 'ChildController@edit')->name('admin.child.store');

    //eddits selected Child paswoord
    Route::post('/children/storepass', 'ChildController@editPass')->name('admin.child.passReset');

    //Display the page for vieuing the children without a group
    Route::get('/children/without/group', 'SchoolclassController@showNoGroup')->name('admin.children.without.class');
});
